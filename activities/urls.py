from django.urls import include, path

from activities.views import (
    ActivityDetailsView,
    ActivityEnrollmentSuccessView,
    ActivityUnrollmentSuccessView,
    IndexView,
    enroll,
    unroll,
)

app_name = "activities"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("<int:pk>", ActivityDetailsView.as_view(), name="detail"),
    path("inschrijvingen/<int:admittanceID>/inschrijven", enroll, name="enroll"),
    path(
        "inschrijvingen/<int:pk>/inschrijving_gelukt",
        ActivityEnrollmentSuccessView.as_view(),
        name="enroll_success",
    ),
    path("inschrijvingen/<int:admittanceID>/uitschrijven", unroll, name="unroll"),
    path(
        "inschrijvingen/<int:pk>/uitschrijving_gelukt",
        ActivityUnrollmentSuccessView.as_view(),
        name="unroll_success",
    ),
]
