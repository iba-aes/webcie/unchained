from django.test import TestCase
from django.utils import timezone

from authentication.models import User
from groups.models import Group
from members.models import Person

from .models import Activity, ActivityCategory, Admittance, Event
from .views import can_enroll, can_unroll, is_user_registered

# Create your tests here.


class ActivitiesEnrollmentTestCase(TestCase):
    # Setup the test environment
    @classmethod
    def setUpTestData(cls):
        cls.activity = Activity.objects.create(
            title="TestActivity",
            description="TestDescription",
        )
        cls.activity.organisers.set(
            [
                Group.objects.create(
                    name="TestGroup", kind=Group.COMMITTEE, since=timezone.now()
                )
            ]
        )
        cls.activity.categories.set(
            [ActivityCategory.objects.create(title="TestCategory")]
        )

        cls.event = Event.objects.create(
            activity=cls.activity,
            subtitle="TestEvent",
            description="TestDescription",
            location="TestLocation",
            time_start=timezone.now(),
            time_end=timezone.now(),
        )

        cls.admittance = Admittance.objects.create(
            event=cls.event,
            price=0,
            registration_deadline=timezone.now() + timezone.timedelta(days=1),
            deregistration_deadline=timezone.now() + timezone.timedelta(days=1),
            max_tickets=10,
        )

        (cls.person, cls.contact) = Person.create_with_contact(
            legal_name="hoi",
            full_name="Mao Zedong",
            given_name="Zedong",
            email="great-leader@example.com",
            birth_date="2000-1-1",
        )

        cls.user = User.objects.create_user("username", cls.person.pk, "password")

    # Login the user
    def setUp(self):
        self.client.force_login(self.user)

    # Test a successful enrollment
    def test_success_enroll(self):
        self.assertTrue(can_enroll(self.admittance, self.contact))
        response = self.client.get(
            f"/activiteiten/inschrijvingen/{self.admittance.pk}/inschrijven"
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            f"/activiteiten/inschrijvingen/{self.admittance.pk}/inschrijving_gelukt",
        )
        self.assertTrue(is_user_registered(self.admittance, self.contact))
        self.assertFalse(can_enroll(self.admittance, self.contact))
        self.assertTrue(can_unroll(self.admittance, self.contact))

    # Test a failed enrollment
    def test_fail_enroll(self):
        self.admittance.registration_deadline = timezone.now() - timezone.timedelta(
            days=1
        )
        self.admittance.save()
        self.assertFalse(can_enroll(self.admittance, self.contact))
        response = self.client.get(
            f"/activiteiten/inschrijvingen/{self.admittance.pk}/inschrijven"
        )
        self.assertEqual(response.status_code, 404)

    # Test a succesfull unrollment
    def test_success_unroll(self):
        self.client.get(
            f"/activiteiten/inschrijvingen/{self.admittance.pk}/inschrijven"
        )
        self.assertTrue(can_unroll(self.admittance, self.contact))
        response = self.client.get(
            f"/activiteiten/inschrijvingen/{self.admittance.pk}/uitschrijven"
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            f"/activiteiten/inschrijvingen/{self.admittance.pk}/uitschrijving_gelukt",
        )
        self.assertFalse(is_user_registered(self.admittance, self.contact))
        self.assertTrue(can_enroll(self.admittance, self.contact))
        self.assertFalse(can_unroll(self.admittance, self.contact))

    # Test a failed unrollment
    def test_fail_unroll(self):
        self.assertFalse(can_unroll(self.admittance, self.contact))
        response = self.client.get(
            f"/activiteiten/inschrijvingen/{self.admittance.pk}/uitschrijven"
        )
        self.assertEqual(response.status_code, 404)
