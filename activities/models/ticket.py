import datetime
from typing import Dict, Optional

from django.db import models

from ideal.models import Transaction
from members.models import Contact
from unchained.junk_drawer import enum_field

from .admittance import Admittance
from .event import Event


class Ticket(models.Model):
    """The registration of a person to an Event.

    Configured by Admittance.
    """

    admittance: Admittance = models.ForeignKey(Admittance, on_delete=models.CASCADE)
    participant: Optional[Contact] = models.ForeignKey(
        "members.Contact", blank=True, null=True, on_delete=models.SET_NULL
    )
    purchase: Transaction = models.ForeignKey(
        "ideal.Transaction", on_delete=models.PROTECT
    )
    moment_registration: datetime = models.DateTimeField()

    def __str__(self) -> str:
        return f"Ticket '{self.admittance.event}' for {self.participant}"


class Question(models.Model):
    """A question to ask participants of an event."""

    FREE: str = "free"
    NUMBER: str = "number"
    MULTIPLE_CHOICE: str = "multiple-choice"
    FILTER: Dict[str, str] = {
        FREE: "free entry",
        NUMBER: "number",
        MULTIPLE_CHOICE: "multiple choice",
    }

    admittance: Admittance = models.ForeignKey(Admittance, on_delete=models.CASCADE)
    question_text: str = models.CharField(max_length=1024)
    format: str = enum_field(FILTER)
    format_options: str = models.TextField()


class Answer(models.Model):
    """An answer to a question for a participant."""

    question: Question = models.ForeignKey(Question, on_delete=models.CASCADE)
    ticket: Ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)

    answer_text: str = models.CharField(max_length=1024)
