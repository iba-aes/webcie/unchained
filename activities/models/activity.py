from django.db import models

from groups.models import Group
from unchained.junk_drawer import enum_field
from unchained.types.queryset import QueryType


class ActivityCategory(models.Model):
    """A group of activities, for easier searchability."""

    title: str = models.CharField(max_length=256)

    def __str__(self) -> str:
        return self.title


class Activity(models.Model):
    """Activities group events, which members can attend.

    An activity has one description and one set of organisers.
    It has also some events, which indicate a time span and location.
    """

    title: str = models.CharField(max_length=1024)
    description: str = models.TextField()
    activity_poster = models.ImageField(
        upload_to="posters/activities/", null=True, blank=True
    )
    organisers: QueryType[Group] = models.ManyToManyField("groups.Group")
    categories: QueryType[ActivityCategory] = models.ManyToManyField("ActivityCategory")

    def __str__(self) -> str:
        return self.title
