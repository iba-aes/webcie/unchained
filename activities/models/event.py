import datetime

from django.db import models

from .activity import Activity


class Event(models.Model):
    """Space-time coordinates associated to an Activity.

    An Activity can have multiple Events associated to it.
    This feature allows for repeating activities,
    such as, famously, the Working Party for the WebCie,
    number 153 of which this comment was written as.
    """

    # Why this structure of Activity 1 <- n Event?
    # In WhosWho, there were a few attempts at the correct representation.

    # There is one class Activiteit,
    # functioning both as Activity and Event.
    # An activity can repeat either daily, weekly or monthly,
    # and all these events are stored in one Activity object.
    # The issue with this is that finding the events on a given moment
    # is going to be very complicated.

    # Another attempt:
    # There is one class Activiteit,
    # functioning both as Activity and Event.
    # If an activity occurs $n$ times,
    # all information is entered in $n$ different Activiteit objects,
    # each with different time, and the last $n-1$ have the first Activiteit
    # as "repetition parent".
    # The issue with this is keeping track of changes,
    # e.g. when the second event changes, should the first and third change too?
    # And what if the change is a different starting time?

    # Another attempt:
    # There is a parent class Activiteit with children
    # ActiviteitInformatie and ActiviteitHerhaling.
    # These loosely correspond to Activity and Event respectively,
    # where ActiviteitInformatie also represented the first Event
    # and ActiviteitHerhaling for all following ones.
    # Having ActiviteitInformatie be special in this way was confusing.
    # Having one class for description and one for time should be easier.

    # So here is yet another attempt!

    activity: Activity = models.ForeignKey(Activity, on_delete=models.CASCADE)

    subtitle: str = models.CharField(max_length=1024)
    description: str = models.TextField(blank=True, null=True)
    event_poster = models.ImageField(upload_to="posters/events/", null=True, blank=True)
    location: str = models.TextField(blank=True, null=True)
    time_start: datetime = models.DateTimeField()
    time_end: datetime = models.DateTimeField()

    def __str__(self) -> str:
        return self.activity.title + ": " + self.subtitle

    def get_poster(self):
        return self.event_poster or self.activity.activity_poster

    class Meta:
        ordering = ["-time_start", "-time_end"]
