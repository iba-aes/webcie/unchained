from decimal import Decimal

from django.db import models
from django.utils import timezone

from .event import Event


class Admittance(models.Model):
    """Indicates how people can register for an Event.

    Upon registration, a Ticket is generated,
    recording the enrollment.
    """

    event: Event = models.ForeignKey(Event, on_delete=models.CASCADE)

    # Maximum price is €999.99
    price: Decimal = models.DecimalField(max_digits=5, decimal_places=2)

    # Registration is the creation of a Ticket; deregistration the deletion.
    # If this field is set to a value before the current time,
    # registration is not possible, and similar for deregistration.
    # The deadlines are useful for the organisers to know who is coming.
    # These are simply limits for activity's visitors,
    # the organisers and board can still create a Ticket.
    # TODO: what happens if a user starts an iDeal transaction before the deadline,
    # and finishes it after the deadline? Is the iDeal timeout short enough?
    registration_deadline: timezone = models.DateTimeField(blank=True, null=True)
    deregistration_deadline: timezone = models.DateTimeField(blank=True, null=True)

    max_tickets: int = models.IntegerField(blank=True, null=True)

    def __str__(self) -> str:
        return f"Admittance '{self.event}' €{self.price}"

    class Meta:
        ordering = ["-registration_deadline", "-deregistration_deadline"]
