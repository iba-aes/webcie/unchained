from django.contrib import admin

from .models.activity import Activity, ActivityCategory
from .models.admittance import Admittance
from .models.event import Event
from .models.ticket import Answer, Question, Ticket


@admin.register(ActivityCategory)
class ActivityCategoryAdmin(admin.ModelAdmin):
    pass


class EventInline(admin.StackedInline):
    model = Event
    extra = 1


@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    inlines = [EventInline]


class AdmittanceInline(admin.StackedInline):
    model = Admittance
    extra = 1


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    inlines = [AdmittanceInline]


class AnswerInline(admin.StackedInline):
    model = Answer
    extra = 1


@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    inlines = [AnswerInline]


class QuestionInline(admin.StackedInline):
    model = Question
    extra = 1


@admin.register(Admittance)
class AdmittanceAdmin(admin.ModelAdmin):
    inlines = [QuestionInline]
