# Generated by Django 3.2.16 on 2022-11-15 16:10

from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("activities", "0001_initial"),
        ("groups", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="activity",
            name="organisers",
            field=models.ManyToManyField(to="groups.Group"),
        ),
    ]
