from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views import View
from django.views.generic import DetailView, ListView, TemplateView
from django.views.generic.edit import FormView

from activities.models.activity import Activity
from activities.models.admittance import Admittance
from activities.models.ticket import Ticket
from ideal.models import Transaction


class IndexView(LoginRequiredMixin, ListView):
    template_name = "activities/index.html"
    context_object_name = "activity_list"

    def get_queryset(self):
        """Returns all activities"""
        return Activity.objects.all()


class ActivityDetailsView(LoginRequiredMixin, DetailView):
    model = Activity
    template_name = "activities/detail.html"
    context_object_name = "activity"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        contact = self.request.user.person.contact
        context["events"] = []
        for event in context["activity"].event_set.all():
            admittances = []
            for admittance in event.admittance_set.all():
                admittances.append(
                    (
                        admittance,
                        can_enroll(admittance, contact),
                        can_unroll(admittance, contact),
                    )
                )
            context["events"].append((event, admittances))
        return context


class ActivityEnrollmentSuccessView(LoginRequiredMixin, DetailView):
    model = Admittance
    template_name = "activities/enroll_success.html"
    context_object_name = "admittance"


class ActivityUnrollmentSuccessView(LoginRequiredMixin, DetailView):
    model = Admittance
    template_name = "activities/unroll_success.html"
    context_object_name = "admittance"


@login_required
def enroll(request, admittanceID):
    """Given an admittance, enroll into the admittance with a ticket."""

    admittance = get_object_or_404(Admittance, pk=admittanceID)
    contact = request.user.person.contact

    if can_enroll(admittance, contact):
        # TODO: Fix transactions using iDeal.
        with transaction.atomic():
            transaction_object = Transaction.objects.create(buyer=contact)

            ticket = Ticket.objects.create(
                admittance=admittance,
                participant=request.user.person.contact,
                purchase=transaction_object,
                moment_registration=timezone.now(),
            )
            ticket.save()

        return HttpResponseRedirect(
            reverse("activities:enroll_success", args=(admittanceID,))
        )

    raise Http404("Deze activiteit bestaat niet of is niet inschrijfbaar.")


@login_required
def unroll(request, admittanceID):
    """Given an admittance, unroll out of the admittance."""

    admittance = get_object_or_404(Admittance, pk=admittanceID)
    contact = request.user.person.contact

    if can_unroll(admittance, contact):
        tickets = Ticket.objects.filter(admittance=admittance, participant=contact)
        for ticket in tickets:
            ticket.delete()

        return HttpResponseRedirect(
            reverse("activities:unroll_success", args=(admittanceID,))
        )

    raise Http404("Deze activiteit bestaat niet of is niet uitschrijfbaar.")


def is_user_registered(admittance, contact):
    return Ticket.objects.filter(admittance=admittance, participant=contact).exists()


def can_enroll(admittance, contact):
    # Check if the enrollment is on time, if the activity is not full and the user is not yet enrolled.
    ticketcount = len(Ticket.objects.filter(admittance=admittance))
    return (
        (
            admittance.registration_deadline is None
            or admittance.registration_deadline > timezone.now()
        )
        and (admittance.max_tickets is None or ticketcount < admittance.max_tickets)
        and (not is_user_registered(admittance, contact))
    )


def can_unroll(admittance, contact):
    # Check if the contact unrolls on time and if they are actually enrolled.
    return (
        admittance.deregistration_deadline is None
        or admittance.deregistration_deadline > timezone.now()
    ) and is_user_registered(admittance, contact)
