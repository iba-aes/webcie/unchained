from unittest import mock

from django.conf import settings
from django.test import TestCase

from authentication.models import User
from members.models import Contact, Person

from .models import Transaction


def createUsers():
    person1, _ = Person.create_with_contact(
        legal_name="hoi",
        full_name="Mao Zedong",
        given_name="Zedong",
        email="great-leader@example.com",
        birth_date="2000-1-1",
    )

    person2, _ = Person.create_with_contact(
        legal_name="hi", full_name="Maedong", given_name="Zeg", email="grr@example.com"
    )

    person1.save()
    person2.save()

    return person1, person2


class NoTransactionTestCase(TestCase):
    def setUp(self):
        self.test_contact = Contact.objects.create(
            name="test_1", email="someone@example.org"
        )
        self.test_contact.save()

    def testTransactionCreation(self):
        """
        test if creating a transaction calls the mollie api
        """
        fake_payment = mock.MagicMock()
        fake_payment.id = "wooo ITS A TEST ID!"
        mock.seal(fake_payment)

        with mock.patch(
            "ideal.mollie_client._mollie_client",
        ) as fake_client:
            # replace the api client with a mock
            fake_client.payments.create.return_value = fake_payment
            # set attribute which will be accessed
            mock.seal(fake_client)
            # close off the mock
            # if we don't do this, when django tries to access undefined fields,
            # the mock will happily create them on the fly, and when it evaluates them,
            # it will error. now it throws a keyerror (which is caught by hasattr)

            amount = 20

            testTransaction = Transaction.objects.create_transaction(
                amount,
                "EUR",
                "this is a test-transaction",
                self.test_contact,
                "/yay/its/a/test/redirect.url",
            )
            fake_client.payments.create.assert_called_once_with(
                {
                    "amount": {"currency": "EUR", "value": f"{amount:.2f}"},
                    "description": "this is a test-transaction",
                    "redirectUrl": "https://"
                    + settings.BASE_URL
                    + "/yay/its/a/test/redirect.url",
                    "webhookUrl": "https://"
                    + settings.BASE_URL
                    + "/ideal/mollie-webhook/",
                }
            )
            created_transaction = Transaction.objects.get(id=testTransaction.id)
            self.assertEqual(created_transaction.amount, amount)
            self.assertEqual(created_transaction.currency, "EUR")
            self.assertEqual(
                created_transaction.description, "this is a test-transaction"
            )
            self.assertEqual(created_transaction.buyer, self.test_contact)
            self.assertEqual(
                created_transaction._mollie_payment_id, "wooo ITS A TEST ID!"
            )


class OneTransactionTestCase(TestCase):
    def setUp(self):
        self.test_transaction = Transaction(
            _mollie_payment_id="wooo ITS A TEST ID!",
            amount=69,
            currency="EUR",
            description="THIS IS A TEST-TRANSACTION.\n "
            "If you can read this on the live site, PANIC.",
            payment_status="OPEN",
        )
        self.test_transaction.save()
        self.myPerson, self.otherPerson = createUsers()

        self.user = User.objects.create_user("username", self.myPerson.pk, "password")
        self.client.force_login(self.user)

    def testCallback(self):
        """
        test if the mollie callback updates database transaction objects
        """
        fake_payment = mock.MagicMock()
        fake_payment.id = "wooo ITS A TEST ID!"
        fake_payment.is_paid.return_value = True
        fake_payment.is_open.return_value = False
        fake_payment.is_pending.return_value = False
        mock.seal(fake_payment)

        with mock.patch("ideal.mollie_client._mollie_client") as fake_client:
            fake_client.payments.get = mock.MagicMock(return_value=fake_payment)
            mock.seal(fake_client)

            self.client.post(
                "/ideal/mollie-webhook/", {"id": "WOOOOT ITS A THIRD TEST ID"}
            )
            fake_client.payments.get.assert_called_once_with(
                "WOOOOT ITS A THIRD TEST ID"
            )
        fake_payment.is_paid.assert_called_once()
        fake_payment.is_open.assert_not_called()
        fake_payment.is_pending.assert_not_called()

        updated_transaction = Transaction.objects.get(id=self.test_transaction.id)
        self.assertEqual(updated_transaction.payment_status, "PAID")
