from django.conf import settings
from mollie.api.client import Client

mollie_api_key = settings.MOLLIE_API_KEY

# create the mollie client only once, on import.
# this also makes it easier to test when it gets used,
# as we can override it with mocks like this.
_mollie_client = Client()
_mollie_client.set_api_key(mollie_api_key)


def mollie_client():
    return _mollie_client
