from django.contrib import admin

from .models import Transaction


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ("__str__", "buyer", "amount", "payment_status", "origin")
    list_filter = ("buyer", "origin", "payment_status")
    list_per_page = 25  # display only 25 transactions per page

    fieldsets = (
        (
            None,
            {
                "fields": (
                    ("buyer", "origin"),
                    "description",
                )
            },
        ),
        ("amount", {"fields": (("amount", "currency", "payment_status"),)}),
    )

    readonly_fields = ("_mollie_payment_id", "origin", "amount", "currency")
