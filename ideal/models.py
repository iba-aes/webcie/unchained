from django.conf import settings
from django.db import models, transaction

# TODO: move this to an iDeal app
from members.models import Contact

from .mollie_client import mollie_client


class AppOrigin(models.Model):
    """
    This represents different reasons to create a transaction, which need different handling.
    Because transaction has a foreignkey to this, different apps can register a listener to
    the "post_save"-signal (which gets sent after a transaction gets saved(, changed or new)).
    Then, they can see if the saved transaction is relevant to them by checking if it came
    originally from their app.
    """

    origin_name = models.CharField(unique=True, max_length=20, null=True)
    active = models.BooleanField(default=False)


class TransactionManager(models.Manager):
    @transaction.atomic
    def create_transaction(
        self,
        amount: float,
        currency: str,
        description: str,
        buyer: Contact,
        redirect: str,
        origin: AppOrigin = None,  # absolute-url path form, i.e. "/home/" or "/activiteiten/1203/"
    ):
        payment = mollie_client().payments.create(
            {
                "amount": {"currency": currency, "value": f"{amount:.2f}"},
                "description": description,
                "redirectUrl": "https://" + settings.BASE_URL + redirect,
                "webhookUrl": "https://" + settings.BASE_URL + "/ideal/mollie-webhook/",
            }
        )
        return self.create(
            _mollie_payment_id=payment.id,
            amount=amount,
            currency=currency,
            description=description,
            buyer=buyer,
            origin=origin,
        )


class Transaction(models.Model):
    """Someone paid money for something.
    when u wanna make a new transaction object, dont use
    `new_transaction = Transaction( *args, **kwargs)`, but use
    `new_transaction = Transaction.objects.create_transaction(*args, **kwargs)`"""

    _mollie_payment_id = models.CharField(
        unique=True, null=True, blank=True, max_length=30
    )

    amount = models.DecimalField(
        help_text="the amount for this transaction",
        decimal_places=2,
        max_digits=10,
        default=0,
    )
    # maximum amount now is €99.999.999,99 -- max is 10 digits
    CURRENCIES = (("EUR", "Euro"),)

    currency = models.CharField(
        help_text="the currency for this transaction",
        max_length=3,
        choices=CURRENCIES,
        default="EUR",
    )
    description = models.TextField(
        help_text="a description of the transaction",
        max_length=200,
        blank=False,
        null=True,
    )

    buyer: Contact = models.ForeignKey(
        "members.Contact",
        help_text="the contact who makes the transaction",
        null=True,
        on_delete=models.SET_NULL,
    )
    PaymentStatus = models.TextChoices(
        "PaymentStatus", "PAID PENDING OPEN CANCELLED EXPIRED"
    )
    payment_status = models.CharField(
        help_text="current status of this payment",
        choices=PaymentStatus.choices,
        default="OPEN",
        max_length=10,
    )

    # we don't want transactions to be deleted,
    # and we want to remember where the transaction came from
    origin = models.ForeignKey(AppOrigin, on_delete=models.PROTECT, null=True)

    objects = TransactionManager()

    @property
    def checkout_url(self):
        if self._mollie_payment_id and self._mollie_payment_id != "":
            payment = mollie_client().payments.get(self._mollie_payment_id)
            return payment.checkout_url
        else:
            return "https://www.example.com"

    def __str__(self):
        if self._mollie_payment_id and self._mollie_payment_id != "":
            return f"mollie_{self._mollie_payment_id}_{self.description}"
        else:
            return f"custom_payment_{self.description}"
