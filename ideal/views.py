from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db.transaction import atomic
from django.http import HttpRequest, HttpResponse, HttpResponseNotFound
from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt

from authentication.models import User

from .models import Transaction
from .mollie_client import mollie_client

# Create your views here.


@csrf_exempt
def mollie_webhook(request: HttpRequest):
    # handeling transaction completion happens inside a database transaction
    with atomic():
        if "id" not in request.POST:
            return HttpResponseNotFound("unknown payment id")

        payment_id = request.POST.get("id")
        payment = mollie_client().payments.get(payment_id)
        transaction = Transaction.objects.get(_mollie_payment_id=payment.id)

        if payment.is_paid():
            transaction.payment_status = "PAID"
        elif payment.is_open():
            transaction.payment_status = "OPEN"
        elif payment.is_pending():
            transaction.payment_status = "PENDING"
        else:
            transaction.payment_status = "CANCELLED"

        transaction.save()

        return HttpResponse(status=204)
    # if an error occurs in getting the payment status:
    # notify mollie, so they can try again later.
    # NOTE: the below line of code is not dead, thats just syntax highlighting thinking
    # python never crashes
    return HttpResponse(status=500)
