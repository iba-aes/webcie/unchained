from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path("mollie-webhook/", views.mollie_webhook, name="mollie_webhook"),
]
