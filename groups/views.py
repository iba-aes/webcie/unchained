from django.db.models import Q
from django.shortcuts import render
from django.template.defaulttags import register
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html
from django.views import generic

from activities.models import Event

from .models import Group, GroupCategory, GroupMembership


@register.filter
def enum_name(key, names):
    return names.get(key, key)


@register.simple_tag
def group_link(group):
    return format_html(
        '<a href="{}">{}</a>',
        reverse("groups:group_detail", args=[group.id]),
        group.name,
    )


@register.simple_tag
def group_member_link(member):
    return format_html(
        '<a href="{}">{}</a>',
        reverse("members:detail", args=[member.person.id]),
        member.person,
    )


@register.simple_tag
def group_category_link(cat):
    return format_html(
        '<a href="{}">{}</a>', reverse("groups:cat_detail", args=[cat.id]), cat.title
    )


class GroupDetailView(generic.DetailView):
    model = Group
    template_name = "groups/detail.html"
    context_object_name = "group"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        group = context["group"]
        activities = group.activity_set.all()
        events = Event.objects.filter(activity__in=activities)
        curr_time = timezone.now()
        context["events_upcoming"] = events.filter(time_end__gt=curr_time)
        context["events_past"] = events.filter(time_end__lte=curr_time)
        related_groups = Group.objects.filter(categories=group.categories).exclude(
            id=group.pk
        )
        context["related"] = related_groups
        members = group.groupmembership_set
        context["members_current"] = members.filter(
            Q(until__isnull=True) | Q(until__gte=curr_time)
        )
        context["members_past"] = members.filter(
            Q(until__isnull=False) & Q(until__lt=curr_time)
        )
        return context


class GroupCategoryDetailView(generic.DetailView):
    model = GroupCategory
    template_name = "groups/cat_detail.html"


class GroupMembershipDetailView(generic.DetailView):
    model = GroupMembership
    template_name = "groups/member_detail.html"


class GroupListView(generic.ListView):
    model = Group
    template_name = "groups/group_list.html"

    def get(self, request, *args, **kwargs):
        committees = Group.objects.filter(kind=Group.COMMITTEE)
        groups = Group.objects.filter(kind=Group.GROUP)
        societies = Group.objects.filter(kind=Group.SOCIETY)

        return render(
            request,
            self.template_name,
            {
                "committees": committees,
                "groups": groups,
                "societies": societies,
            },
        )
