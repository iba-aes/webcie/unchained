import datetime
from typing import Dict, Optional

import django.utils.timezone as timezone
from django.conf import settings
from django.conf.urls.static import static
from django.db import models

from authentication.models import AuthGroup
from members.models.person import Person
from unchained.junk_drawer import enum_field
from unchained.types.queryset import QueryType

# TODO: Maybe make this relative or dynamic instead of hardcoded?
# TODO: Exposing Person from the members app to the root module doesn't seem to work, AppRegistryNotReady error if I put this in __init__.py of the members app:
# Apparently its impossible to import models or other subpackages in the root package of an application. Maybe we should find a workaround, as it would come in handy if different
# apps can only access exposed models etc.


class GroupCategory(models.Model):
    """Different types of groups."""

    title: str = models.CharField(max_length=256)

    def __str__(self) -> str:
        return self.title


class Group(models.Model):
    """Various kinds of groups of people."""

    COMMITTEE: str = "committee"
    GROUP: str = "group"
    SOCIETY: str = "SOCIETY"
    KIND: Dict[str, str] = {COMMITTEE: "committee", GROUP: "group", SOCIETY: "society"}
    KIND_NL: Dict[str, str] = {
        COMMITTEE: "commissie",
        GROUP: "groep",
        SOCIETY: "dispuut",
    }

    name: str = models.CharField(max_length=32)
    description: str = models.TextField(max_length=128, blank=True, null=True)
    email: str = models.CharField(max_length=64, blank=True, null=True)

    kind: str = enum_field(KIND, default=COMMITTEE)
    since: datetime = models.DateField(default=timezone.now)
    until: datetime = models.DateField(blank=True, null=True)
    poster = models.ImageField(upload_to="posters/groups", null=True, blank=True)
    website = models.CharField(max_length=64, blank=True, null=True)

    auth_groups: QueryType[AuthGroup] = models.ManyToManyField(AuthGroup, blank=True)

    members: QueryType[Person] = models.ManyToManyField(
        Person, through="GroupMembership"
    )

    categories: Optional[GroupCategory] = models.ForeignKey(
        GroupCategory, on_delete=models.PROTECT, blank=True, null=True
    )

    def __str__(self) -> str:
        return f"{self.name}"

    def get_poster_url(self):
        if self.poster:
            return self.poster.url
        else:
            return settings.MEDIA_URL + "posters/groups/defaultposter.jpg"


class GroupMembership(models.Model):
    """Records that a Contact is a member of a group."""

    person: Person = models.ForeignKey(Person, on_delete=models.CASCADE)
    group: Group = models.ForeignKey(Group, on_delete=models.CASCADE)

    since: datetime = models.DateField(default=timezone.now)
    until: datetime = models.DateField(blank=True, null=True)

    def __str__(self):
        return f"{self.person} is a member of {self.group}"
