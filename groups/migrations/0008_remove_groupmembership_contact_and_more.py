# Generated by Django 4.2.9 on 2024-03-21 18:19

import django.db.models.deletion
from django.db import migrations, models


# We manually set the new person column to the person related to the current conatct.
# Note that this means that this migration will fail if there is
# a group membership with a contact that does not have a related person.
def add_person(apps, schema_editor):
    MyModel = apps.get_model("groups", "GroupMembership")
    PersonModel = apps.get_model("members", "Person")
    for row in MyModel.objects.all():
        row.person = PersonModel.objects.get(contact=row.contact)
        row.save(update_fields=["person"])


class Migration(migrations.Migration):
    dependencies = [
        ("members", "0001_initial"),
        ("groups", "0007_alter_group_website"),
    ]

    operations = [
        migrations.AddField(
            model_name="groupmembership",
            name="person",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.CASCADE,
                to="members.person",
            ),
            preserve_default=False,
        ),
        migrations.RunPython(add_person, reverse_code=migrations.RunPython.noop),
        migrations.RemoveField(
            model_name="groupmembership",
            name="contact",
        ),
        migrations.AlterField(
            model_name="group",
            name="members",
            field=models.ManyToManyField(
                through="groups.GroupMembership", to="members.person"
            ),
        ),
    ]
