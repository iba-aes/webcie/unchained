from django.contrib import admin

from .models import Group, GroupCategory, GroupMembership


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    pass


@admin.register(GroupCategory)
class GroupCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(GroupMembership)
class GroupMembershipAdmin(admin.ModelAdmin):
    pass
