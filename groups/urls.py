from django.urls import path

from . import views

app_name = "groups"
urlpatterns = [
    path("", views.GroupListView.as_view(), name="groups"),
    path("<int:pk>/", views.GroupDetailView.as_view(), name="group_detail"),
    path(
        "lid/<int:pk>", views.GroupMembershipDetailView.as_view(), name="member_detail"
    ),
    path(
        "categorie/<int:pk>/",
        views.GroupCategoryDetailView.as_view(),
        name="cat_detail",
    ),
]
