from django.urls import path
from django.views.generic import RedirectView

from . import views


def cause_error(request):
    raise Exception("Hello, I'm an error. Isn't that great?")


app_name = "members"
urlpatterns = [
    path("<int:pk>/", views.DetailView.as_view(), name="detail"),
    path(
        "<int:pk>/achievements",
        RedirectView.as_view(pattern_name="userachievements"),
        name="redirect_userachievements",
    ),
    path("verjaardagen/", views.BirthdaysView, name="birthdays"),
    path("ingelogd", views.logged_in_redirect, name="logged_in"),
    path("", views.PersonListView.as_view(), name="list"),
    path("bewerken/<int:pk>", views.EditView.as_view(), name="edit"),
    path("error", cause_error),
]
