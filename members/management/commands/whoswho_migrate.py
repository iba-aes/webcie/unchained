import os
from datetime import datetime, timedelta

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Max, Min
from whoswhopy.auth import Auth
from whoswhopy.database import geef_wsw4db
from whoswhopy.gen.Boekweb2.Leverancier import Leverancier
from whoswhopy.gen.Contact import Contact as WSWContact
from whoswhopy.gen.ContactAdres import ContactAdres
from whoswhopy.gen.ContactTelnr import ContactTelnr
from whoswhopy.gen.Lid import Lid
from whoswhopy.gen.LidStudie import LidStudie
from whoswhopy.gen.Organisatie import Organisatie
from whoswhopy.gen.Persoon import Persoon
from whoswhopy.gen.Spookweb2.Bedrijf import Bedrijf
from whoswhopy.gen.Studie import Studie

from members.models.membership import Membership
from members.models.person import Address
from members.models.person import Contact as UnchContact
from members.models.person import Person, PhoneNumber
from members.models.study_program import Enrollment, StudyProgram

# Naming conventions:
# If there is a name clash between classes in WhosWho4 and Unchained,
# we disambiguate with `WSW` and `Unch` prefixes respectively.
# If there is a name clash in variables,
# we disambiguate them with `wsw_` and `unch_` prefixes.


class OverrideAuth(Auth):
    """An Auth implementation that grants all rights and reads
    configuration from environment variables."""

    def check_recht(self, recht):
        return True

    def geef_db_auth(self):
        return ("root", os.environ.get("GOD_PASSWORD"))


class UnmigratableError(Exception):
    """This exception is raised when an error occurs during migration.

    It records the table and id of the object that could not be migrated,
    for easier debugging in scripts.
    """

    def __init__(self, table, id):
        self.table = table
        self.id = id

    def __str__(self):
        return f"Error during migration of {self.table} {self.id}"


def full_name(persoon: WSWContact):
    """Determine the full name of a Contact from WhosWho.

    Includes special support for a Contact which is not a Persoon.
    """
    if isinstance(persoon, Persoon):
        naam = persoon.voornaam
        if not naam:
            naam = persoon.voorletters
        if not naam:
            naam = ""
        if persoon.tussenvoegsels:
            naam += " " + persoon.tussenvoegsels
        naam += " " + persoon.achternaam
        return naam
    else:
        return persoon.naam


def legal_name(persoon: Persoon):
    """Determine the legal name of a Persoon from WhosWho."""
    naam = persoon.voornaam
    if not naam:
        naam = persoon.voorletters
    if not naam:
        naam = ""
    if persoon.titelsPrefix:
        naam = persoon.titelsPrefix + " " + naam
    if persoon.tussenvoegsels:
        naam += " " + persoon.tussenvoegsels
    naam += " " + persoon.achternaam
    if persoon.titelsPostfix:
        naam += " " + persoon.titelsPostfix
    return naam


def given_name(persoon: Persoon):
    """Determine the given name of a Persoon from WhosWho."""
    naam = persoon.voornaam
    if not naam:
        naam = persoon.voorletters
    if not naam:
        naam = persoon.achternaam
    return naam


def phone_number_kind(telnr: ContactTelnr):
    """Map a WhosWho ContactTelnr to PhoneNumber kinds."""
    return {
        "THUIS": PhoneNumber.HOME,
        "MOBIEL": PhoneNumber.MOBILE,
        "WERK": PhoneNumber.WORK,
        "OUDERS": PhoneNumber.EMERGENCY,
        "RECEPTIE": PhoneNumber.WORK,  # These shouldn't be too relevant anymore.
        "FAX": PhoneNumber.WORK,  # These shouldn't be too relevant anymore.
    }[telnr.soort]


def address_kind(adres: ContactAdres):
    """Map a WhosWho ContactAdres to Address kinds."""
    return {
        "THUIS": Address.HOME,
        "OUDERS": Address.PARENTS,
        "WERK": Address.POSTAL,
        "POST": Address.POSTAL,
        "BEZOEK": Address.HOME,
        "FACTUUR": Address.BILLING,
    }[adres.soort]


def study_phase(studie: Studie):
    """Map a WhosWho Studie to StudyProgram phases."""
    return {"BA": StudyProgram.BACHELOR, "MA": StudyProgram.MASTER}[studie.fase]


def enrollment_until(lidstudie: LidStudie):
    """Map a WhosWho LidStudie to a plausible end date.

    In WhosWho, the status of a LidStudie is controlled by an enum `status`,
    together with an end date. Because those values can desync, we want to
    standardize on only using end dates.

    This function uses the end date of a LidStudie if applicable, otherwise it
    makes up a reasonable value.
    """
    if lidstudie.datumEind:
        return lidstudie.datumEind

    if lidstudie.status in {"ALUMNUS", "GESTOPT"}:
        return lidstudie.datumBegin + timedelta(days=5 * 365)

    # The enrollment has not ended, so use None as the end date.
    return None


# These values for Lid.lidToestand indicate no "real" membership,
# and were used for storing the data associated with membership between registration and paying membership fee,
# or for some mildly ugly workarounds.
lidToestand_with_no_membership = {"BIJNALID", "INGESCHREVEN", "BEESTJE"}


def membership_kind(lid: Lid):
    """Map a WhosWho Lid to a Membership kind.

    Some values in the enumeration map to None, indicating this kind of lidToestand does not correspond to being a member of the association.
    See also `lidToestand_with_no_membership`.
    """
    return {
        "LID": Membership.MEMBER,
        "OUDLID": Membership.MEMBER,  # This is just a fancy way of saying Membership.until < now()
        "GESCHORST": Membership.MEMBER,  # This is just a fancy way of saying Membership.until < now(), together with an angry letter from the board
        "BAL": Membership.BAL,
        "LIDVANVERDIENSTE": Membership.MERIT,
        "ERELID": Membership.HONORARY,
        # These will be represented differently in the new schema:
        "BIJNALID": None,
        "INGESCHREVEN": None,
        "GESCHORST": None,
        "BEESTJE": None,
    }[lid.lidToestand]


def membership_since(lid: Lid):
    """Map a WhosWho Lid to a plausible starting date.

    In WhosWho, the starting date of a membership is required for newly-edited
    objects, but not for existing data imported from older versions.

    This function uses the starting date of a Lid if applicable, otherwise it
    makes up a reasonable value.
    """
    if lid.lidVan:
        return lid.lidVan

    # If there is study enrollment information,
    # assume the membership started when the first enrollment started.
    from_enrollments = Enrollment.objects.filter(
        person=Person.objects.get(pk=lid.contactID)
    ).aggregate(Min("since"))
    if from_enrollments["since__min"]:
        return from_enrollments["since__min"]
    # If that doesn't help, just make up something.
    return datetime(1971, 2, 10)  # "Time immemorial": the founding of A–Eskwadraat


def membership_until(lid: Lid):
    """Map a WhosWho Lid to a plausible end date.

    In WhosWho, the status of a Lid is controlled by an enum `lidToestand`,
    together with an end date. Because those values can desync, we want to
    standardize on only using end dates.

    This function uses the end date of a Lid if applicable, otherwise it
    makes up a reasonable value.
    """
    if lid.lidTot:
        return lid.lidTot

    if lid.lidToestand == "OUDLID":
        # If there is study enrollment information,
        # assume the membership stopped when the last enrollment stopped.
        from_enrollments = Enrollment.objects.filter(
            person=Person.objects.get(pk=lid.contactID)
        ).aggregate(Max("until"))
        if from_enrollments["until__max"]:
            return from_enrollments["until__max"]
        # Otherwise, they should probably have finished studying within 5 years.
        if lid.lidVan:
            return lid.lidVan + timedelta(days=5 * 365)
        # If that doesn't help, just make up something.
        return datetime(1971, 2, 10)  # "Time immemorial": the founding of A–Eskwadraat

    # The membership has not ended, so use None as the end date.
    return None


class Command(BaseCommand):
    help = "Migrate the WhosWho4 database to Unchained"

    def add_arguments(self, parser):
        parser.add_argument("--min_contact", default=0, type=int)
        parser.add_argument("--min_phone_number", default=0, type=int)
        parser.add_argument("--min_address", default=0, type=int)
        parser.add_argument("--min_study_program", default=0, type=int)
        parser.add_argument("--min_enrollment", default=0, type=int)
        parser.add_argument("--min_membership", default=0, type=int)

    def handle(self, *args, **options):
        wsw4db = geef_wsw4db(auth=OverrideAuth())

        # The following tables will be populated:
        # Contact
        # Person
        # PhoneNumber
        # Address
        # StudyProgram
        # Enrollment
        # Membership

        for wsw_contact in wsw4db.query(WSWContact).filter(
            WSWContact.contactID >= options["min_contact"]
        ):
            try:
                email = wsw_contact.email
                if not email:
                    email = f"lid{wsw_contact.contactID}@a-eskwadraat.nl"
                unch_contact = UnchContact(
                    id=wsw_contact.contactID, name=full_name(wsw_contact), email=email
                )
                unch_contact.save()
            except Exception as e:
                raise UnmigratableError(WSWContact, wsw_contact.contactID) from e

            if isinstance(wsw_contact, Persoon):
                try:
                    unch_person = Person(
                        id=wsw_contact.contactID,
                        contact=unch_contact,
                        legal_name=legal_name(wsw_contact),
                        given_name=given_name(wsw_contact),
                        birth_date=wsw_contact.datumGeboorte,
                    )
                    unch_person.save()
                except Exception as e:
                    raise UnmigratableError(Persoon, wsw_contact.contactID) from e

        for telnr in wsw4db.query(ContactTelnr).filter(
            ContactTelnr.telnrID >= options["min_phone_number"]
        ):
            try:
                person = Person.objects.get(pk=telnr.contact_contactID)
            except Person.DoesNotExist:
                # Only migrate telephone numbers for Person objects
                continue
            try:
                phone_number = PhoneNumber(
                    id=telnr.telnrID, person=person, number=telnr.telefoonnummer
                )
                phone_number.save()
            except Exception as e:
                raise UnmigratableError(ContactTelnr, telnr.telnrID) from e

        for adres in wsw4db.query(ContactAdres).filter(
            ContactAdres.adresID >= options["min_address"]
        ):
            try:
                person = Person.objects.get(pk=adres.contact_contactID)
            except Person.DoesNotExist:
                # Only migrate addresses for Person objects
                continue
            try:
                address = Address(
                    id=adres.adresID,
                    person=person,
                    kind=address_kind(adres),
                    street=adres.straat1
                    + (", " + adres.straat2 if adres.straat2 else ""),
                    street_number=adres.huisnummer if adres.huisnummer else "",
                    extra="",
                    postal_code=adres.postcode,
                    city=adres.woonplaats,
                    country=adres.land if adres.land else "",
                )
                address.save()
            except Exception as e:
                raise UnmigratableError(ContactAdres, adres.adresID) from e

        for studie in wsw4db.query(Studie).filter(
            Studie.studieID >= options["min_study_program"]
        ):
            try:
                study_program = StudyProgram(
                    id=studie.studieID, name=studie.naam_NL, phase=study_phase(studie)
                )
                study_program.save()
            except Exception as e:
                raise UnmigratableError(Studie, studie.studieID) from e

        for lidstudie in wsw4db.query(LidStudie).filter(
            LidStudie.lidStudieID >= options["min_enrollment"]
        ):
            try:
                enrollment = Enrollment(
                    id=lidstudie.lidStudieID,
                    person=Person.objects.get(pk=lidstudie.lid_contactID),
                    program=StudyProgram.objects.get(pk=lidstudie.studie_studieID),
                    since=lidstudie.datumBegin,
                    until=enrollment_until(lidstudie),
                )
                enrollment.save()
            except Exception as e:
                raise UnmigratableError(LidStudie, lidstudie.lidStudieID) from e

        for lid in wsw4db.query(Lid).filter(Lid.contactID >= options["min_membership"]):
            # The Lid object was also used to store information about the person.
            # If the person is not a "real" member, but we needed to store the information anyway,
            # we assigned them a non-membership value for `lidToestand`:
            if lid.lidToestand in lidToestand_with_no_membership:
                print(f"Membership {lid.contactID} skipped ({lid.lidToestand})...")
                continue

            try:
                membership = Membership(
                    id=lid.contactID,
                    person=Person.objects.get(pk=lid.contactID),
                    kind=membership_kind(lid),
                    since=membership_since(lid),
                    until=membership_until(lid),
                )

                membership.save()
            except Exception as e:
                raise UnmigratableError(Lid, lid.contactID) from e
