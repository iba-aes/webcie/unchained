from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import HttpRequest
from django.shortcuts import redirect, render
from django.template.defaulttags import register
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html
from django.views import generic

from .forms import AddressFormSet, ContactForm, PersonForm, PhoneNumberFormSet
from .models import Address, Person, PhoneNumber


class PersonListView(LoginRequiredMixin, generic.ListView):
    model = Person
    paginate_by = 25  # https://docs.djangoproject.com/en/stable/topics/pagination/#using-paginator-in-view

    template_name = "members/person_list"
    context_object_name = "person_list"


@register.simple_tag
def person_link(person):
    return format_html(
        '<a href="{}">{}</a>',
        reverse("members:detail", args=[person.id]),
        person.full_name,
    )


class DetailView(LoginRequiredMixin, generic.DetailView):
    model = Person
    template_name = "members/detail.html"


@login_required
def BirthdaysView(request: HttpRequest):
    try:
        date = datetime.strptime(request.GET.get("maand", ""), "%m")
    except ValueError:
        date = timezone.now()
    if date == None:
        date = timezone.now()

    persons = Person.objects.filter(birth_date__month=date.month)
    persons_by_date: dict[int, list[Person]] = {}
    for person in persons:
        day = person.birth_date.day
        if not day in persons_by_date:
            persons_by_date[day] = [person]
        else:
            persons_by_date[day].append(person)

    context = {"date": date, "persons_by_date": sorted(persons_by_date.items())}

    return render(request, "members/birthdays.html", context=context)


class EditView(LoginRequiredMixin, generic.DetailView):
    model = Person
    template_name = "members/edit.html"

    def get(self, request, *args, **kwargs):
        if request.user.person.id != kwargs["pk"]:
            raise PermissionDenied
        viewed_person = Person.objects.get(pk=kwargs["pk"])

        # The birth_date has to be in ISO8601 format for the date picker to work.
        # https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/date#value
        if viewed_person.birth_date is not None:
            viewed_person.birth_date = viewed_person.birth_date.isoformat()

        contactForm = ContactForm(instance=viewed_person.contact)
        personForm = PersonForm(instance=viewed_person)

        phoneFormSet = PhoneNumberFormSet(prefix="phone", instance=viewed_person)
        addressFormSet = AddressFormSet(prefix="address", instance=viewed_person)
        return render(
            request,
            self.template_name,
            {
                "nameForms": [contactForm, personForm],
                "phoneFormset": phoneFormSet,
                "addressFormset": addressFormSet,
                "person": viewed_person,
            },
        )

    def post(self, request, *args, **kwargs):
        if request.user.person.id != kwargs["pk"]:
            raise PermissionDenied
        viewed_person = Person.objects.get(pk=kwargs["pk"])
        form_id = request.POST.get("form_id")

        contactForm = ContactForm(instance=viewed_person.contact)
        personForm = PersonForm(instance=viewed_person)
        phoneFormSet = PhoneNumberFormSet(prefix="phone", instance=viewed_person)
        addressFormSet = AddressFormSet(prefix="address", instance=viewed_person)

        form_valid_flag = False

        if form_id == "nameForm":
            contactForm = ContactForm(request.POST, instance=viewed_person.contact)
            personForm = PersonForm(request.POST, instance=viewed_person)
            if contactForm.is_valid() and personForm.is_valid():
                contactForm.save()
                personForm.save()
                form_valid_flag = True

        elif form_id == "phoneForms":
            phoneFormSet = PhoneNumberFormSet(
                request.POST, prefix="phone", instance=viewed_person
            )
            if phoneFormSet.is_valid():
                phoneFormSet.save()
                form_valid_flag = True

        elif form_id == "addressForms":
            addressFormSet = AddressFormSet(
                request.POST, prefix="address", instance=viewed_person
            )
            if addressFormSet.is_valid():
                addressFormSet.save()
                form_valid_flag = True

        else:
            pass
        if not form_valid_flag:
            return render(
                request,
                self.template_name,
                {
                    "nameForm": [contactForm, personForm],
                    "phoneFormset": phoneFormSet,
                    "addressFormset": addressFormSet,
                    "person": viewed_person,
                },
            )

        return redirect(f'/leden/{kwargs["pk"]}', DetailView)


@login_required
def logged_in_redirect(request):
    """Redirects to the details page of the logged in member."""
    return redirect("members:detail", pk=request.user.person.id)
