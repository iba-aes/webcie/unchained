from django import forms
from django.forms import inlineformset_factory, modelform_factory, modelformset_factory
from django.utils.translation import gettext_lazy as _
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import RegionalPhoneNumberWidget

from ..models.person import Address, Contact, Person, PhoneNumber

ContactForm = modelform_factory(
    Contact, fields=("name", "email"), labels={"name": _("name"), "email": _("email")}
)
PersonForm = modelform_factory(
    Person,
    fields=("legal_name", "given_name", "birth_date"),
    labels={
        "legal_name": _("legal name"),
        "given_name": _("given_name"),
        "birth_date": _("birth_date"),
    },
    widgets={"birth_date": forms.DateInput(attrs={"type": "date"})},
)


AddressFormSet = inlineformset_factory(
    Person,
    Address,
    fields=(
        "kind",
        "street",
        "street_number",
        "extra",
        "postal_code",
        "city",
        "country",
    ),
    extra=1,
)

PhoneNumberFormSet = inlineformset_factory(
    Person,
    PhoneNumber,
    fields=("kind", "number"),
    labels={"kind": _("Soort"), "number": _("Nummer")},
    extra=1,
)
