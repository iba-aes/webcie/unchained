import datetime

import pytest
from django.core.exceptions import PermissionDenied, ValidationError
from django.http.request import HttpRequest
from django.test import TestCase

from authentication.models import User, UserManager
from members.forms.userdata_form import ContactForm, PersonForm
from members.views import EditView

from .models import Address, Contact, Person, PhoneNumber


@pytest.mark.django_db
def test_person_unusual_name():
    person, contact = Person.create_with_contact(
        legal_name="Adolph Blaine Charles David Earl Frederick Gerald Hubert Irvin John Kenneth Lloyd Martin Nero Oliver Paul Quincy Randolph Sherman Thomas Uncas Victor William Xerxes Yancy Zeus Wolfe­schlegel­stein­hausen­berger­dorff­welche­vor­altern­waren­gewissen­haft­schafers­wessen­schafe­waren­wohl­gepflege­und­sorg­faltig­keit­be­schutzen­vor­an­greifen­durch­ihr­raub­gierig­feinde­welche­vor­altern­zwolf­hundert­tausend­jah­res­voran­die­er­scheinen­von­der­erste­erde­mensch­der­raum­schiff­genacht­mit­tung­stein­und­sieben­iridium­elek­trisch­motors­ge­brauch­licht­als­sein­ur­sprung­von­kraft­ge­start­sein­lange­fahrt­hin­zwischen­stern­artig­raum­auf­der­suchen­nach­bar­schaft­der­stern­welche­ge­habt­be­wohn­bar­planeten­kreise­drehen­sich­und­wo­hin­der­neue­rasse­von­ver­stand­ig­mensch­lich­keit­konnte­fort­pflanzen­und­sicher­freuen­an­lebens­lang­lich­freude­und­ru­he­mit­nicht­ein­furcht­vor­an­greifen­vor­anderer­intelligent­ge­schopfs­von­hin­zwischen­stern­art­ig­raum Sr.",
        full_name="Hubert Blaine Wolfeschlegelsteinhausenbergerdorff",
        given_name="Hubert",
        email="wolfeschlegelsteinhausenbergerdorff@example.com",
    )

    # See: https://en.wikipedia.org/wiki/Hubert_Blaine_Wolfeschlegelsteinhausenbergerdorff_Sr.

    # Check that the objects are valid, i.e. there is no ValidationError.
    print(person)
    print(person.contact)
    contact.full_clean()
    contact.save()
    person.full_clean()


@pytest.mark.django_db
def test_person_unicode_name():
    person, contact = Person.create_with_contact(
        legal_name="毛泽东",
        full_name="Mao Zedong",
        given_name="Zedong",
        email="great-leader@example.com",
    )

    # Check that this Person object is valid, i.e. there is no ValidationError.
    contact.full_clean()
    contact.save()
    person.full_clean()


@pytest.mark.django_db
def test_empty_forms():
    person, contact = Person.create_with_contact(
        legal_name="Test Person",
        full_name="Test Person",
        given_name="Test",
        email="test@example.com",
    )
    contactForm = ContactForm(data={}, instance=contact)
    assert contactForm.errors["name"] == ["Dit veld is verplicht."]
    personForm = PersonForm(data={}, instance=person)
    assert personForm.errors["given_name"] == ["Dit veld is verplicht."]


def createUsers():
    person1, _ = Person.create_with_contact(
        legal_name="hoi",
        full_name="Mao Zedong",
        given_name="Zedong",
        email="great-leader@example.com",
        birth_date="2000-1-1",
    )

    person2, _ = Person.create_with_contact(
        legal_name="hi", full_name="Maedong", given_name="Zeg", email="grr@example.com"
    )

    person1.save()
    person2.save()

    return person1, person2


class TestUserEdit(TestCase):
    def setUp(self):
        self.myPerson, self.otherPerson = createUsers()

        self.user = User.objects.create_user("username", self.myPerson.pk, "password")

    def test_wrong_user_edit_get(self):
        self.client.force_login(self.user)

        response = self.client.get(f"/leden/bewerken/{self.otherPerson.pk}")
        self.assertEqual(response.status_code, 403)

        view = EditView()
        req = HttpRequest()
        req.user = self.user
        self.assertRaises(
            PermissionDenied, lambda: view.get(req, pk=self.otherPerson.pk)
        )

    def test_wrong_user_edit_post(self):
        self.client.force_login(self.user)

        response = self.client.post(f"/leden/bewerken/{self.otherPerson.pk}")
        self.assertEqual(response.status_code, 403)
        view = EditView()
        req = HttpRequest()
        req.user = self.user
        self.assertRaises(
            PermissionDenied, lambda: view.get(req, pk=self.otherPerson.pk)
        )

    def test_right_user_get_edit(self):
        self.client.force_login(self.user)

        response = self.client.get(f"/leden/bewerken/{self.myPerson.pk}")

        self.assertIs(response.status_code, 200)
        self.assertTemplateUsed(response, "members/edit.html")

    def test_right_user_least_post(self):
        self.client.force_login(self.user)

        response = self.client.post(
            f"/leden/bewerken/{self.myPerson.pk}",
            {
                "given_name": self.myPerson.given_name,
                "legal_name": self.myPerson.legal_name,
                "name": self.myPerson.contact.name,
                "email": self.myPerson.contact.email,
                "birth_date": self.myPerson.birth_date,
                "form_id": "nameForm",
            },
        )

        self.assertEqual(response.status_code, 302)

    def test_right_user_wrong_date_format(self):
        self.client.force_login(self.user)
        response = self.client.post(
            f"/leden/bewerken/{self.myPerson.pk}",
            {
                "given_name": self.myPerson.given_name,
                "legal_name": self.myPerson.legal_name,
                "name": self.myPerson.contact.name,
                "email": self.myPerson.contact.email,
                "birth_date": "20-01-2000",
                "form_id": "nameForm",
            },
        )
        self.assertEqual(response.status_code, 302)

        self.assertEqual(
            datetime.date(2000, 1, 20),
            Person.objects.get(pk=self.myPerson.pk).birth_date,
        )

    def test_add_phone_number(self):
        self.client.force_login(self.user)
        response = self.client.post(
            f"/leden/bewerken/{self.myPerson.pk}",
            {
                "phone-TOTAL_FORMS": "1",
                "phone-INITIAL_FORMS": "0",
                "phone-0-id": "",
                "phone-0-kind": "home",
                "phone-0-number": "+31623456789",
                "form_id": "phoneForms",
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            "+31623456789", PhoneNumber.objects.get(person=self.myPerson.pk).number
        )
        self.assertEqual("home", PhoneNumber.objects.get(person=self.myPerson.pk).kind)

    def test_add_address(self):
        self.client.force_login(self.user)
        response = self.client.post(
            f"/leden/bewerken/{self.myPerson.pk}",
            {
                "address-TOTAL_FORMS": "1",
                "address-INITIAL_FORMS": "0",
                "address-0-id": "",
                "address-0-kind": "home",
                "address-0-street": "Dorpsstraat",
                "address-0-street_number": "2",
                "address-0-extra": "",
                "address-0-postal_code": "1234AB",
                "address-0-city": "Utrecht",
                "address-0-country": "MK",
                "form_id": "addressForms",
            },
        )
        self.assertEqual(response.status_code, 302)
        addr = Address.objects.get(person=self.myPerson.pk)
        self.assertEqual("home", addr.kind)
        self.assertEqual("Dorpsstraat", addr.street)
        self.assertEqual("2", addr.street_number)
        self.assertEqual("1234AB", addr.postal_code)
        self.assertEqual("Utrecht", addr.city)
        self.assertEqual("MK", addr.country)

    def test_remove_address(self):
        self.client.force_login(self.user)
        addr = Address.objects.create(
            kind="home",
            street="Bierlaan",
            street_number="3",
            postal_code="1212AB",
            city="Moskou",
            country="MK",
            person_id=self.myPerson.pk,
        )
        response = self.client.post(
            f"/leden/bewerken/{self.myPerson.pk}",
            {
                "address-TOTAL_FORMS": "1",
                "address-INITIAL_FORMS": "1",
                "address-0-id": addr.pk,
                "address-0-kind": "home",
                "address-0-street": "Dorpsstraat",
                "address-0-street_number": "2",
                "address-0-extra": "",
                "address-0-postal_code": "1234AB",
                "address-0-city": "Utrecht",
                "address-0-country": "MK",
                "address-0-DELETE": "on",
                "form_id": "addressForms",
            },
        )
        self.assertEqual(response.status_code, 302)

        self.assertRaises(
            Address.DoesNotExist,
            lambda: Address.objects.get(person_id=self.myPerson.pk),
        )

    def test_remove_phone_number(self):
        self.client.force_login(self.user)
        number = PhoneNumber.objects.create(
            kind="home", number="0612345678", person_id=self.myPerson.pk
        )
        response = self.client.post(
            f"/leden/bewerken/{self.myPerson.pk}",
            {
                "phone-TOTAL_FORMS": "1",
                "phone-INITIAL_FORMS": "1",
                "phone-0-id": number.pk,
                "phone-0-kind": "home",
                "phone-0-number": "0612345678",
                "phone-0-DELETE": "on",
                "form_id": "phoneForms",
            },
        )
        self.assertEqual(response.status_code, 302)

        self.assertRaises(
            PhoneNumber.DoesNotExist,
            lambda: PhoneNumber.objects.get(person_id=self.myPerson.pk),
        )


class TestBirthdays(TestCase):
    def setUp(self):
        self.myPerson, self.otherPerson = createUsers()
        self.user = User.objects.create_user("username", self.myPerson.pk, "password")

    # Tests if someone appears on the page it belongs and if the site works.
    def test_site_correct(self):
        self.otherPerson.birth_date = "2000-3-3"
        self.otherPerson.save()

        self.client.force_login(self.user)

        response = self.client.get(f"/leden/verjaardagen/?maand=3")

        self.assertIs(response.status_code, 200)
        self.assertContains(response, self.otherPerson.full_name)
        self.assertTemplateUsed(response, "members/birthdays.html")

        self.otherPerson.birth_date = None
        self.otherPerson.save()

    # Tests if someone that doesn't have a birthday doesn't show up and if the site still works.
    def test_no_birthday(self):
        self.assertIsNone(self.otherPerson.birth_date)
        self.client.force_login(self.user)

        response = self.client.get(f"/leden/verjaardagen/?maand=1")

        self.assertIs(response.status_code, 200)
        self.assertNotContains(response, self.otherPerson.full_name)
        self.assertTemplateUsed(response, "members/birthdays.html")
