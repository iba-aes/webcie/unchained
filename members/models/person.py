from __future__ import annotations

from datetime import datetime
from typing import Dict, Optional, Tuple

from django.apps import apps
from django.db import models
from django_countries.fields import CountryField
from phonenumber_field.modelfields import PhoneNumberField

from unchained.junk_drawer import enum_field


class Contact(models.Model):
    """Some minimal information to identify a person.

    This is the main API endpoint for members:
    other apps can use a Contact to identify people.

    If we need full personal details, use a Person
    that has this Contact as foreign key.
    """

    # Corresponds to Person.full_name
    # e.g. "Kobayashi von Servingman"
    name: str = models.CharField(max_length=1024)
    # Corresponds to Person.email
    email: str = models.EmailField()

    def __str__(self) -> str:
        return self.name


class PersonManager(models.Manager):
    """
    A manager for the Person model which doesn't return the anonymous user.
    """

    def get_queryset(self):
        User = apps.get_model("authentication.user")
        # this gets the User class as defined in authentication.models
        # not a straight import to prevent circular imports
        return super().get_queryset().exclude(pk=User.get_anonymous().person.pk)


class Person(models.Model):
    """A model for storing full personal details of people.

    Member administration uses this over Contact if they need the additional fields.
    Other apps should access the information via Contact.
    """

    contact: Contact = models.ForeignKey(Contact, on_delete=models.CASCADE)

    # e.g "Prof. Kobayashi Iras von Servingman"
    legal_name: str = models.CharField(max_length=1024)
    # e.g. "Kobayashi"
    given_name: str = models.CharField(max_length=1024)

    birth_date: datetime = models.DateField(null=True, blank=True)

    objects = PersonManager()
    # special manager returning all people except the person associated with the anonymous user

    all_objects = models.Manager()
    # TODO: comments? (made by the board)

    @classmethod
    def create_with_contact(
        cls,
        *,
        full_name: str,
        email: str,
        legal_name: str,
        given_name: str,
        birth_date: Optional[datetime] = None,
    ) -> Tuple[Person, Contact]:
        contact = Contact.objects.create(name=full_name, email=email)
        person = Person.all_objects.create(
            contact=contact,
            legal_name=legal_name,
            given_name=given_name,
            birth_date=birth_date,
        )
        return person, contact

    @property
    def full_name(self) -> str:
        # We don't include a setter, since the caller would also need to save the Contact.
        return self.contact.name

    @property
    def email(self) -> str:
        """The e-mail address of this person.

        Reads it out from the Contact information.
        """
        # We don't include a setter, since the caller would also need to save the Contact.
        return self.contact.email

    def __str__(self) -> str:
        return self.full_name

    @property
    def groups(self):
        group_memberships = self.groupmembership_set.all()
        groups = [group_membership.group for group_membership in group_memberships]
        return groups

    class meta:
        _default_manager_name = "PersonManager"


class PhoneNumber(models.Model):
    """A phone number associated with a person."""

    HOME: str = "home"
    MOBILE: str = "mobile"
    WORK: str = "work"
    EMERGENCY: str = "emergency"
    KIND: Dict[str, str] = {
        HOME: "home",
        MOBILE: "mobile",
        WORK: "work",
        EMERGENCY: "emergency",
    }

    person: Person = models.ForeignKey(Person, on_delete=models.CASCADE)

    kind: str = enum_field(KIND, default=HOME)
    number: str = PhoneNumberField()

    def __str__(self) -> str:
        return f"{self.number} ({PhoneNumber.KIND[self.kind]})"


class Address(models.Model):
    """An address associated with a person."""

    HOME: str = "home"
    BILLING: str = "billing"
    PARENTS: str = "parents"
    POSTAL: str = "postal"
    KIND: Dict[str, str] = {
        HOME: "home address",
        BILLING: "billing address",
        PARENTS: "parents' address",
        POSTAL: "postal address",
    }

    person: Person = models.ForeignKey(Person, on_delete=models.CASCADE)

    kind: str = enum_field(KIND, default=HOME)
    street: str = models.CharField(max_length=256)
    street_number: str = models.CharField(max_length=16)
    extra: Optional[str] = models.CharField(max_length=256, blank=True)
    postal_code: str = models.CharField(max_length=16)
    city: str = models.CharField(max_length=256)
    country: str = CountryField(default="NL")

    class Meta:
        verbose_name_plural = "addresses"

    def __str__(self) -> str:
        return f"{self.street} {self.street_number} {self.extra} ({Address.KIND[self.kind]})"
