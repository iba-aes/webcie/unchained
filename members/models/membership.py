import datetime
from typing import Dict

from django.db import models
from django.db.models import F, Q

from unchained.junk_drawer import enum_field

from .person import Person


class Membership(models.Model):
    MEMBER: str = "member"
    BAL: str = "bal"  # Bijzonder Actief Lid
    MERIT: str = "merit"  # Lid van Verdienste
    HONORARY: str = "honorary"  # Erelid
    KIND: Dict[str, str] = {
        MEMBER: "member",
        BAL: "BAL",
        MERIT: "member of merit",
        HONORARY: "honorary member",
    }

    person: Person = models.ForeignKey(Person, on_delete=models.CASCADE)
    kind: str = enum_field(KIND, default=MEMBER)
    since: datetime = models.DateField()
    until: datetime = models.DateField(blank=True, null=True)

    def __str__(self) -> str:
        return f"{self.person} registered as a {self.kind}"

    class Meta:
        constraints = [
            models.CheckConstraint(
                name="membership_date_range_valid",
                check=Q(since__lte=F("until")) | Q(until=None),
            )
        ]
