import datetime
from typing import Dict

from django.db import models
from django.db.models import F, Q

from unchained.junk_drawer import enum_field
from unchained.types.queryset import QueryType

from .person import Person


class StudyProgram(models.Model):
    """A specific study program that students may follow at UU.

    Dutch: 'studie' or 'opleiding'.
    """

    BACHELOR: str = "ba"
    MASTER: str = "ma"
    PHASE: Dict[str, str] = {BACHELOR: "bachelor", MASTER: "master"}

    # TODO: use a library like django-modeltranslation if we translate this field
    name: str = models.CharField(max_length=256)
    phase: str = enum_field(PHASE)
    # TODO: here we can also add an abbreviation (for translating user data to StudyProgram objects)

    students: QueryType[Person] = models.ManyToManyField(Person, through="Enrollment")

    def __str__(self) -> str:
        return f"{self.name} ({StudyProgram.PHASE[self.phase]})"


class Enrollment(models.Model):
    """Records that a person (typically a member of the association) is enrolled in a study program."""

    person: Person = models.ForeignKey(Person, on_delete=models.CASCADE)
    program: StudyProgram = models.ForeignKey(StudyProgram, on_delete=models.PROTECT)

    since: datetime = models.DateField()
    until: datetime = models.DateField(blank=True, null=True)

    class Meta:
        constraints = [
            models.CheckConstraint(
                name="enrollment_date_range_valid",
                check=Q(since__lte=F("until")) | Q(until=None),
            )
        ]

    def __str__(self) -> str:
        return f"{self.person} enrolled in {self.program}"
