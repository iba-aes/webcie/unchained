from django.contrib import admin

from .models import (
    Address,
    Contact,
    Enrollment,
    Membership,
    Person,
    PhoneNumber,
    StudyProgram,
)


class AddressInline(admin.StackedInline):
    model = Address
    extra = 1


class ContactInline(admin.StackedInline):
    model = Contact
    extra = 0


class EnrollmentInline(admin.TabularInline):
    model = Enrollment
    extra = 1


class PhoneNumberInline(admin.TabularInline):
    model = PhoneNumber
    extra = 1


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    pass


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    inlines = [PhoneNumberInline, AddressInline, EnrollmentInline]

    def get_queryset(self, request):
        if request.user.is_superuser:
            return Person.all_objects
        else:
            super().get_queryset(request)


@admin.register(Membership)
class MembershipAdmin(admin.ModelAdmin):
    pass


@admin.register(StudyProgram)
class StudyProgramAdmin(admin.ModelAdmin):
    pass


@admin.register(Enrollment)
class EnrollmentAdmin(admin.ModelAdmin):
    pass
