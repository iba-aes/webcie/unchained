from typing import Generic, Iterator, TypeVar

from django.db.models import QuerySet

_Z = TypeVar("_Z")


# Type to represent Django's QuerySet object. This is used whenever Django's models return some list of objects,
# for example in a Many-To-Many relation.
# Based on the type implementation by https://github.com/Vieolo/django-hint, however adding a dependency for a single
# classed seemed a bit overkill.
class QueryType(Generic[_Z], QuerySet):
    def __iter__(self) -> Iterator[_Z]:
        ...
