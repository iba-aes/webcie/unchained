"""unchained URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/stable/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.generic import RedirectView, TemplateView

from wrinklypages import views as wpviews

from . import views

urlpatterns = [
    path("", RedirectView.as_view(pattern_name="home")),
    path("home/", views.index, name="home"),
    path("admin/doc/", include("django.contrib.admindocs.urls")),
    path("admin/", admin.site.urls),
    path("activiteiten/", include("activities.urls")),
    path("service/docuweb/", include("docuweb.urls")),
    path("pages/", include("django.contrib.flatpages.urls")),
    path("leden/", include("members.urls")),
    path("vereniging/nieuws/", include("news.urls")),
    path("accounts/", include("authentication.urls")),
    path("vereniging/commissies/", include("groups.urls")),
    path("achievements/", include("achievements.urls")),
    path("i18n/", include("django.conf.urls.i18n")),
    path("ideal/", include("ideal.urls")),
    path("carriere/", include("career.urls")),
    path("publisher/", include("wrinklypages.urls")),
    path("tinymce/", include("tinymce.urls")),
    path("__debug__/", include("debug_toolbar.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
