import os
import sys
from urllib.parse import urlparse

from pyngrok import conf, ngrok

ngrok_path = os.environ.get("NGROK_PATH")
print(f"ngrok_path: {ngrok_path}")

if ngrok_path:
    conf.get_default().ngrok_path = ngrok_path

conf.get_default().ngrok_version = "v3"


def get_ngrok_connection(port=None):
    port = get_port_or_default(port)
    connection = ngrok.connect(port)

    print(f'ngrok tunnel "{connection.public_url}" -> "http://127.0.0.1:{port}"')

    return connection


def get_port_or_default(port=None):
    """
    selects the port based on a list of priorities.
    1. the port argument to this function
    2. the port part of the `addrport` argument to runserver,
    which is the last one when specified.
    3. 8000
    getting the addrport argument parsed is a bit of a hassle, as the urllib library is shitty.
    also because the addrport argument can be an ip-adress, a port, or both, or neither.
    """
    if port is None:
        addrport = urlparse(f"http://{sys.argv[-1]}")
        if addrport.netloc and addrport.port:
            port = addrport.port
        else:
            try:
                addrport = urlparse(f"http://localhost:{addrport.hostname}")
                port = addrport.port
            except ValueError:
                port = 8000
    return port


class ngrok_connection:
    def __init__(self, port: int = None):
        self.port = get_port_or_default(port)

    def __enter__(self):
        self.connection = get_ngrok_connection(self.port)
        return self.connection

    def __exit__(self, *args):
        ngrok.disconnect(self.connection.public_url)
        self.connection.close()


def main():
    print(
        "when this python program ends, all connections will be lost.\n"
        "If you want to try this module, "
        "try importing this module in the interpreter"
    )


if __name__ == "__main__":
    main()
