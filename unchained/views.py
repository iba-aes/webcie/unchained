from django.shortcuts import render

from news.views import getNewsContext


def index(request):
    context = getNewsContext(request)
    return render(request, "index.html", context=context)
