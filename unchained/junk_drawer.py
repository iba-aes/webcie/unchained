from django.db import models


def enum_field(options: dict, **kwargs):
    """Convert a dictionary of 'enum' options to a CharField.

    >>> # TODO: doctest
    """

    return models.CharField(
        max_length=max(len(k) for k in options.keys()),
        choices=options.items(),
        **kwargs,
    )
