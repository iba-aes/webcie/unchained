import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";
import { EnvironmentProvider } from "./environment";

if (!EnvironmentProvider.isDevelopment()) {
  Sentry.init({
    dsn: "https://7af72be9bed24d3e9c499a2abd29c1f7@o226068.ingest.sentry.io/5463967",
    environment: EnvironmentProvider.getEnvironment(),
    integrations: [new Integrations.BrowserTracing()],
    // This gets replaced by webpack when bundling
    release: process.env.GIT_SHA,

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
  });
}
