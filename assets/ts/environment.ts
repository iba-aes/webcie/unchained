export class EnvironmentProvider {

    static isDevelopment(): boolean {
        return this.getEnvironment() === 'development';
    }

    static isProduction(): boolean {
        return this.getEnvironment() === 'production';
    }

    static isStaging(): boolean {
        return this.getEnvironment() === 'staging';
    }

    static getEnvironment(): string {
        return process.env['NODE_ENV'] ?? 'development';
    }
}

