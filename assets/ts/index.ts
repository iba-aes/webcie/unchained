import "./sentry";
import "../scss/style.scss";

// https://fontawesome.com/
// https://fontawesome.com/how-to-use/on-the-web/setup/using-package-managers
// https://stackoverflow.com/questions/52376720/how-to-make-font-awesome-5-work-with-webpack
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'

function test(str: string): void {
    console.log(str);
}

test("Welkom op de nieuwe website van A-Eskwadraat!");


declare global {
    interface Window { addTabEventListeners: () => void; addFileUploadInputEventListeners: () => void; addNotificationDeleteEventListeners: () => void; addMobileMenuToggleEventListener: () => void  }
}

//Based on code from https://codepen.io/t7team/pen/ZowdRN
function openTab(event: MouseEvent, tabNumber: number): void {
    // Retrieve contents of different tabs.
    const contentTabs: HTMLCollectionOf<Element> = document.getElementsByClassName("js-tab-content");

    // Set all tab contents to be invisible.
    for (let i = 0; i < contentTabs.length; i++) {
        const current: HTMLElement = contentTabs[i] as HTMLLIElement;
        current.style.display = "none";
    }

    // Retrieve the different links in the tabs component.
    const tabLinks: HTMLCollectionOf<Element> = document.getElementsByClassName("js-tab");

    // Set all tab links to inactive.
    for (let i = 0; i < tabLinks.length; i++) {
        tabLinks[i].className = tabLinks[i].className.replace(" is-active", "");
    }

    // Switch to the correct tab.
    const tab: HTMLElement = contentTabs[tabNumber] as HTMLLIElement;
    tab.style.display = "block";
    const eventTarget: HTMLLIElement = event.currentTarget as HTMLLIElement;
    eventTarget.className += " is-active";
}

// Adds event listeners on pages that use the Bulma 'tabs' component.
// The event listeners make switching tabs possible.
function addTabEventListeners(): void {
    const tabLinks: HTMLCollectionOf<Element> = document.getElementsByClassName("js-tab");

    for (let i = 0; i < tabLinks.length; i++) {
        const tab = tabLinks[i] as HTMLLIElement;
        tab.addEventListener('click', e => openTab(e, i), false)
    }
}

window.addTabEventListeners = addTabEventListeners;

// Based on code from https://bulma.io/documentation/form/file/#javascript
function addFileUploadInputEventListeners(): void {
    // Find all "file elements" of the form: <div class="file" ...> ... </div>
    const fileElements: HTMLCollectionOf<Element> = document.getElementsByClassName("file");
    if (fileElements.length == 0)
        console.error("Tried to add event listeners to file upload inputs, but no file form elements were found.");

    for (let i = 0; i < fileElements.length; i++) {
        // Find all "file inputs" within the current file element of the form: <input type="file" ...> ... </input>
        const fileInputs: NodeListOf<Element> = fileElements[i].querySelectorAll("input[type=file]");
        if (fileInputs.length == 0)
            console.error("Tried to add event listeners to file upload inputs, but no file upload inputs were found.");

        for (let j = 0; j < fileInputs.length; j++) {
            const fileInput: HTMLInputElement = fileInputs[i] as HTMLInputElement;

            fileInput.onchange = (): void => {
                if (fileInput.files === null)
                    return;
                else if (fileInput.files.length > 0) {
                    // Find all "file names" within the current file element of the form: <span class="file-name" ...> ... </span>
                    const fileNames: HTMLCollectionOf<Element> = fileElements[i].getElementsByClassName("file-name");

                    if (fileNames.length > 1)
                        console.error("A file upload input contains multiple file-names.");

                    for (let k = 0; k < fileNames.length; k++) {
                        fileNames[k].textContent = fileInput.files[0].name;
                    }
                }
            };
        }
    }
}

window.addFileUploadInputEventListeners = addFileUploadInputEventListeners;

// Based on code from https://bulma.io/documentation/elements/notification/#javascript-example
function addNotificationDeleteEventListeners(): void {
    (document.querySelectorAll(".notification .delete") || []).forEach(($delete) => {
        const $notification = $delete.parentNode;

        $delete.addEventListener("click", () => {
            $notification?.parentNode?.removeChild($notification);
        });
    });
}

window.addNotificationDeleteEventListeners = addNotificationDeleteEventListeners
// Source: https://bulma.io/documentation/components/navbar/#navbar-menu
function addMobileMenuToggleEventListener(): void {
    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    if ($navbarBurgers.length > 0) {
        $navbarBurgers.forEach(el => {
            el.addEventListener('click', () => {
                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);

                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                el.classList.toggle('is-active');
                $target?.classList.toggle('is-active');
            });
        });
    }
}

window.addMobileMenuToggleEventListener = addMobileMenuToggleEventListener;
