FROM python:3.11.6-alpine as devbuild

ENV PATH "$PATH:/root/.local/bin"

WORKDIR /root/unchained

# Needs to be installed for building typed-ast dependency
RUN apk add gcc build-base
# Needed for the Pillow dependency
RUN apk add zlib libjpeg-turbo-dev libpng-dev freetype-dev lcms2-dev libwebp-dev harfbuzz-dev fribidi-dev tcl-dev tk-dev
# Some usefull tools
RUN apk add git
# NPM
RUN apk add npm

RUN pip install pipenv

# Copy the pipfile first, so we can skip the pipenv sync even if some files changed
# in the project
COPY Pipfile .
COPY Pipfile.lock .
RUN pipenv sync --dev

# Now copy the package.json and the package-lock.json
COPY package.json .
COPY package-lock.json .
RUN npm install --also=dev

# Copy the rest of the files
COPY . .

EXPOSE 8000
ENTRYPOINT ["./entrypoint.sh"]

####### Staging image #######
FROM devbuild as stagingbuild
# Run webpack
RUN npx webpack --config webpack.staging.js

####### Prod image #######
FROM devbuild as prodbuild
# Run webpack
RUN npx webpack --config webpack.prod.js
