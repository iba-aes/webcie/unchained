from typing import List, Optional

# The custom backend below does not implement all optional authentication methods, such
# as has_perm. For now, the backend subclasses ModelBackend. For more information, see:
# https://docs.djangoproject.com/en/stable/topics/auth/customizing/#customizing-authentication-in-django
# https://docs.djangoproject.com/en/stable/ref/contrib/auth/#module-django.contrib.auth.backends
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import Permission
from django.core.handlers.wsgi import WSGIRequest

from groups.models import AuthGroup

from .models import User


class MemberBackend(ModelBackend):
    def get_user(self, user_id: int) -> Optional[User]:
        """
        Retrieves a User.
        """
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    def authenticate(
        self,
        request: Optional[WSGIRequest],
        username: Optional[str] = None,
        password: Optional[str] = None,
    ) -> Optional[User]:
        """
        Authenticates a User.
        """
        try:
            user = User.objects.get_by_natural_key(username)
        except User.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a nonexistent user (#20760).
            User().set_password(password)
            return None
        if user.check_password(password) and self.user_can_authenticate(user):
            return user

    def user_can_authenticate(self, user: User) -> bool:
        """
        Reject users with is_active=False. Custom user models that don't have
        that attribute are allowed.
        """
        is_active = getattr(user, "is_active", None)
        return is_active or is_active is None

    def get_group_permissions(self, user_obj: User, obj=None) -> List[Permission]:
        """
        Finds a User's permissions by finding all member Groups of this User's
        corresponding Person, and bundling all the member Group's AuthGroup permissions.
        """
        # Retrieve all member Groups.
        groups = list(user_obj.person.group_set.all())

        # Bundle all AuthGroups.
        auth_groups = []
        for group in groups:
            for auth_group in group.auth_groups.all():
                auth_groups.append(auth_group)

        # Retrieve all permissions from the bundled AuthGroups.
        permissions = []
        for auth_group in auth_groups:
            for permission in auth_group.permissions.all():
                permissions.append(permission)

        permissions = list(set(permissions))

        return permissions
