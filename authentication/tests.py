import pytest
from django.contrib.auth import authenticate
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from guardian.utils import get_anonymous_user

from authentication.backends import MemberBackend
from authentication.models import AuthGroup, User
from groups.models import Group, GroupMembership
from members.models import Person

pytestmark = pytest.mark.django_db

backend = MemberBackend()


def create_test_person():
    """
    Creates a basic person for test purposes.
    """
    person, contact = Person.create_with_contact(
        legal_name="Admin van Adminstein",
        full_name="Adje van Stein",
        given_name="Ad",
        email="admin@example.com",
    )
    person.full_clean()
    person.save()
    contact.full_clean()
    contact.save()
    return person


class UserManagerTests:
    def test_create_user_succesful(self):
        """
        Succesful base case.
        """
        person = create_test_person()
        user = User.objects.create_user(
            username="admin", person=person.pk, password="testpassword"
        )
        user.full_clean()
        user.save()
        assert User.objects.get(username="admin") == user

    def test_create_user_no_username(self):
        """
        Username is not passed correctly.
        """
        person = create_test_person()
        try:
            user = User.objects.create_user(
                username=None, person=person.pk, password="testpassword"
            )
        except ValueError as err:
            assert str(err) == "The given username must be set"

    def test_create_superuser_succesful(self):
        """
        Succesful base case.
        """
        person = create_test_person()
        superuser = User.objects.create_superuser(
            username="admin", person=person.pk, password="testpassword"
        )
        superuser.full_clean()
        superuser.save()
        assert User.objects.get(username="admin") == superuser

    def test_create_superuser_not_staff(self):
        """
        is_staff is wrongly set to False.
        """
        person = create_test_person()
        try:
            superuser = User.objects.create_superuser(
                username="admin",
                person=person,
                password="testpassword",
                **{"is_staff": False},
            )
        except ValueError as err:
            str(err) == "Superuser must have is_staff=True"

    def test_create_superuser_not_superuser(self):
        """
        is_superuser is wrongly set to False.
        """
        person = create_test_person()
        try:
            superuser = User.objects.create_superuser(
                username="admin",
                person=person,
                password="testpassword",
                **{"is_superuser": False},
            )
        except ValueError as err:
            str(err) == "Superuser must have is_superuser=True"


class UserTests:
    def test_email(self):
        """
        Email should return a placeholder email for now.
        """
        person = create_test_person()
        user = User.objects.create_user(
            username="admin", person=person.pk, password="testpassword"
        )
        user.full_clean()
        user.save()

        user.email == "placeholder@email.com"


class MemberBackendTests:
    def test_get_user_succesful(self):
        """
        The succesful base case.
        """
        person = create_test_person()
        user = User.objects.create_user(
            username="admin", person=person.pk, password="testpassword"
        )
        user.full_clean()
        user.save()

        assert backend.get_user(user.id) == user

    def test_anonymous_user(self):
        """
        Getting the first user should return the anonymous user
        """
        user = backend.get_user(1)
        assert user.id == get_anonymous_user().id

    def test_get_user_failure(self):
        """
        Getting the user fails because the User does not exist.
        """
        user = backend.get_user(2)
        assert user is None

    def test_authenticate_success(self):
        """
        The succesful base case.
        """
        person = create_test_person()
        user = User.objects.create_user(
            username="admin", person=person.pk, password="testpassword72"
        )
        user.full_clean()
        user.save()

        user_authenticated = backend.authenticate(
            None, username="admin", password="testpassword72"
        )

        assert user == user_authenticated

    def test_authenticate_failure(self):
        """
        Authenticating fails because the User does not exist.
        """
        user_authenticated = backend.authenticate(
            None, username="admin", password="testpassword"
        )

        assert user_authenticated is None

    def test_authenticate_password_failure(self):
        """
        The password that was entered was wrong.
        """
        person = create_test_person()

        user = User.objects.create_user(
            username="admin", person=person.pk, password="testpassword72"
        )

        user.full_clean()
        user.save()

        user_authenticated = backend.authenticate(
            None, username="admin", password="testpassword73"
        )

        assert user_authenticated is None

    def test_get_group_permissions(self):
        """
        Retrieve group permissions of a User.
        """
        person = create_test_person()

        user = User.objects.create_user(
            username="admin", person=person.pk, password="testpassword"
        )
        user.full_clean()
        user.save()

        committee = Group(name="TestGroup", kind=Group.COMMITTEE, since=timezone.now())
        committee.full_clean()
        committee.save()

        # Create a few auth groups.
        auth_group1 = AuthGroup(name="TestAuthGroupy")
        auth_group1.full_clean()
        auth_group1.save()

        auth_group2 = AuthGroup(name="GroupieWoupie")
        auth_group2.full_clean()
        auth_group2.save()

        # Create some permissions.
        content_type = ContentType.objects.get_for_model(User)
        permission = Permission.objects.create(
            codename="test_permission",
            name="Permission for a test",
            content_type=content_type,
        )
        permission2 = Permission.objects.create(
            codename="another_test_permission",
            name="Another permission for a test",
            content_type=content_type,
        )
        permission3 = Permission.objects.create(
            codename="another_test_permission_aswell",
            name="Want my permission?",
            content_type=content_type,
        )
        permission4 = permission3

        # Add a few permissions to a few auth groups.
        auth_group1.permissions.add(permission)
        auth_group1.permissions.add(permission2)
        auth_group2.permissions.add(permission3)
        auth_group2.permissions.add(permission4)

        # Add auth groups to the committee
        committee.auth_groups.add(auth_group1)
        committee.auth_groups.add(auth_group2)
        committee.full_clean()
        committee.save()

        # Add a member to the committee
        group_membership = GroupMembership(person=person, group=committee)
        group_membership.full_clean()
        group_membership.save()

        permissions = user.get_group_permissions(user)

        assert (
            permissions is not None
            and permission in permissions
            and permission2 in permissions
            and permission3 in permissions
            and len(permissions) == 3
        )
