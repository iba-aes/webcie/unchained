from django.contrib.auth import views as auth_views
from django.urls import include, path

app_name = "authentication"
urlpatterns = [
    path("password_reset/", auth_views.PasswordResetView.as_view(success_url="done")),
    path("", include("django.contrib.auth.urls")),
]
