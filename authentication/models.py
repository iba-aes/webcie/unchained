from __future__ import annotations

from typing import Optional

from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import Group as BaseGroup
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from guardian.mixins import GuardianUserMixin

from members.models import Person


class UserManager(BaseUserManager):
    def _create_user(
        self, username: str, person: Person, password: str, **extra_fields
    ) -> User:
        """
        Create and save a user with the given username, email, and password.
        """
        if not username:
            raise ValueError("The given username must be set")
        username = self.model.normalize_username(username)
        user = self.model(
            username=username, person=Person.objects.get(pk=person), **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(
        self,
        username: str,
        person: Person,
        password: Optional[str] = None,
        **extra_fields,
    ) -> User:
        """
        Set regular user.
        """
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(username, person, password, **extra_fields)

    def create_superuser(
        self,
        username: str,
        person: Person,
        password: Optional[str] = None,
        **extra_fields,
    ) -> User:
        """
        Create a superuser.
        """
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(username, person, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin, GuardianUserMixin):
    username_validator = UnicodeUsernameValidator()

    person = models.OneToOneField(
        Person, on_delete=models.CASCADE, blank=False, null=False
    )

    username = models.CharField(
        "username",
        max_length=150,
        unique=True,
        help_text="Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.",
        validators=[username_validator],
        error_messages={"unique": "A user with that username already exists."},
    )

    is_staff = models.BooleanField(
        "staff status",
        default=False,
        help_text="Designates whether the user can log into this admin site.",
    )

    is_active = models.BooleanField(
        "active status",
        default=True,
        help_text="Designates whether the user is active.",
    )

    objects = UserManager()

    @property
    def email(self):
        return "placeholder@email.com"  # self.person.email

    USERNAME_FIELD = "username"
    EMAIL_FIELD = "person__contact__email"

    REQUIRED_FIELDS = ["person"]

    class Meta:
        permissions = (("active", "Is part of a committee"), ("member", "Is a member"))


class AuthGroup(BaseGroup):
    pass


def create_anonymous_user(userclass):
    name = settings.ANONYMOUS_USER_NAME
    person, contact = Person.create_with_contact(
        full_name=name,
        email="nonoya.business@example.com",
        legal_name=name,
        given_name="Anon",
    )
    return userclass(person=person, username=settings.ANONYMOUS_USER_NAME)
