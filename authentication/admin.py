from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group as BaseGroup
from django.utils.translation import gettext_lazy as _

from authentication.models import AuthGroup, User

admin.site.unregister(BaseGroup)
admin.site.register(AuthGroup)


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("username", "password1", "password2", "person"),
            },
        ),
    )
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("person",)}),
        (
            _("Permissions"),
            {"fields": ("is_staff", "is_superuser", "user_permissions")},
        ),
        (_("Important dates"), {"fields": ("last_login",)}),
    )
    list_filter = tuple()
    list_display = ("person", "username", "email", "is_staff")
