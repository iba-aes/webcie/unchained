from django.contrib.auth.mixins import PermissionRequiredMixin
from django.utils import timezone
from django.views import generic

from .models import NewsMessage


class NewsListView(generic.ListView):
    paginate_by = 10
    model = NewsMessage

    template_name = "news/news_list.html"
    context_object_name = "news_list"

    def get_queryset(self):
        return (
            NewsMessage.getVisibleMessages(self.request.user)
            .filter(start__lte=timezone.now())
            .order_by("-end")
        )


class NewsDetailView(PermissionRequiredMixin, generic.DetailView):
    model = NewsMessage

    template_name = "news/news_detail.html"
    context_object_name = "news"

    def get_permission_required(self):
        target = self.get_object().target
        if target == 2:
            return ["authentication.active"]
        elif target == 1:
            return ["authentication.member"]
        else:
            return []


# Returns the context containing the news messages for the home page.
def getNewsContext(request):
    context = {
        "news_list": NewsMessage.getVisibleMessages(request.user)
        .filter(start__lte=timezone.now(), end__gte=timezone.now())
        .order_by("-priority", "-start")
    }
    return context
