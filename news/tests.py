from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.db.utils import IntegrityError
from django.test import RequestFactory, TestCase
from django.utils import timezone

from authentication.models import User
from members.models import Person

from .models import NewsMessage
from .views import getNewsContext


class NewsMessageTests(TestCase):
    # Tests that a news message will be succesfully created.
    def testSuccess(self):
        self.news1 = NewsMessage.objects.create(
            title="Testmessage1",
            content="Testcontent1",
            target=0,
            priority="2",
            start=timezone.now() - timezone.timedelta(days=1),
            end=timezone.now() + timezone.timedelta(days=1),
        )
        self.news1.save()
        self.assertEqual(NewsMessage.objects.all()[0], self.news1)

    # Tests that you can not create a new message with an end before the start.
    def testFail(self):
        with self.assertRaises(IntegrityError):
            with transaction.atomic():
                NewsMessage.objects.create(
                    title="Testmessage1",
                    content="Testcontent1",
                    target=0,
                    priority="2",
                    start=timezone.now() + timezone.timedelta(days=1),
                    end=timezone.now() - timezone.timedelta(days=1),
                )
        self.assertEqual(len(NewsMessage.objects.all()), 0)


class NewsContextTests(TestCase):
    def setUp(self):
        self.news1 = NewsMessage.objects.create(
            title="Testmessage1",
            content="Testcontent1",
            target=0,
            priority="2",
            start=timezone.now() - timezone.timedelta(days=1),
            end=timezone.now() + timezone.timedelta(days=1),
        )
        self.news2 = NewsMessage.objects.create(
            title="Testmessage2",
            content="Testcontent2",
            target=1,
            priority="1",
            start=timezone.now() - timezone.timedelta(days=1),
            end=timezone.now() + timezone.timedelta(days=1),
        )
        self.news3 = NewsMessage.objects.create(
            title="Testmessage3",
            content="Testcontent3",
            target=2,
            priority="0",
            start=timezone.now() - timezone.timedelta(days=1),
            end=timezone.now() + timezone.timedelta(days=1),
        )
        self.factory = RequestFactory()

        (self.person, self.contact) = Person.create_with_contact(
            legal_name="hoi",
            full_name="Mao Zedong",
            given_name="Zedong",
            email="great-leader@example.com",
            birth_date="2000-1-1",
        )

        self.user = User.objects.create_user("username", self.person.pk, "password")

    # Tests that a non membser user can only see public news messages.
    def testPublic(self):
        request = self.factory.get("/home")
        request.user = self.user
        context = getNewsContext(request)
        self.assertEqual(len(context["news_list"]), 1)
        self.assertEqual(context["news_list"][0], self.news1)

    # Tests that a not active member can only see news messages for everyone and for members.
    def testMember(self):
        request = self.factory.get("/home")
        request.user = self.user
        content_type = ContentType.objects.get_for_model(User)
        request.user.user_permissions.add(
            Permission.objects.get(content_type=content_type, codename="member")
        )
        context = getNewsContext(request)
        self.assertEqual(len(context["news_list"]), 2)
        self.assertEqual(context["news_list"][0], self.news1)
        self.assertEqual(context["news_list"][1], self.news2)

    # Tests that an active user can see every news message.
    def testActive(self):
        request = self.factory.get("/home")
        request.user = self.user
        content_type = ContentType.objects.get_for_model(User)
        request.user.user_permissions.add(
            Permission.objects.get(content_type=content_type, codename="member")
        )
        request.user.user_permissions.add(
            Permission.objects.get(content_type=content_type, codename="active")
        )
        context = getNewsContext(request)
        self.assertEqual(len(context["news_list"]), 3)
        self.assertEqual(context["news_list"][0], self.news1)
        self.assertEqual(context["news_list"][1], self.news2)
        self.assertEqual(context["news_list"][2], self.news3)
