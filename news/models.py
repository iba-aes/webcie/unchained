from datetime import datetime
from typing import Dict

from django.db import models
from django.db.models import F, Q
from django.db.models.constraints import CheckConstraint
from django.utils.translation import gettext_lazy as _
from tinymce.models import HTMLField

from unchained.junk_drawer import enum_field


class NewsMessage(models.Model):
    """A news message for on the homepage."""

    class Target(models.IntegerChoices):
        EVERYONE = 0, _("Iedereen")
        MEMBERS = 1, _("Leden")
        ACTIVE = 2, _("Actief")

    class Priority(models.IntegerChoices):
        HIGH = "2", _("Hoog")
        MEDIUM = "1", _("Normaal")
        LOW = "0", _("Laag")

    title: str = models.CharField(max_length=1024)
    content: str = HTMLField()

    target: str = models.IntegerField(choices=Target.choices, default=Target.EVERYONE)
    priority: str = models.IntegerField(choices=Priority.choices, default=Priority.HIGH)

    start: datetime = models.DateTimeField()
    end: datetime = models.DateTimeField()

    class Meta:
        constraints = [
            CheckConstraint(check=Q(start__lte=F("end")), name="start_before_end")
        ]

    def __str__(self) -> str:
        return self.title

    @classmethod
    def getVisibleMessages(cls, user):
        """Returns the news messages the current user has access to."""
        if not user.has_perm("authentication.member"):
            access = 0
        elif not user.has_perm("authentication.active"):
            access = 1
        else:
            access = 2
        return cls.objects.filter(target__lte=access)
