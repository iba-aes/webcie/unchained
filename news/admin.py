from django.contrib import admin

from .models import NewsMessage


@admin.register(NewsMessage)
class NewsAdmin(admin.ModelAdmin):
    pass
