from django import forms

from docuweb.models import Document, DocumentCategory


class DocumentCreateForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ["title", "category", "file"]
        widgets = {
            "title": forms.TextInput(attrs={"class": "input", "type": "text"}),
            "file": forms.ClearableFileInput(
                attrs={"class": "file-input", "type": "file", "name": "document"}
            ),
        }
