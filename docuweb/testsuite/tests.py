from unittest.mock import MagicMock, patch

import pytest

from docuweb.models import Category, Uploadable
from docuweb.testsuite.specs import (
    spec_category_str,
    spec_uploadable_path,
    spec_uploadable_str,
    spec_validate_pdf,
)

pytestmark = pytest.mark.django_db


def test_validate_pdf():
    test_file_1 = MagicMock()
    test_file_1.name = "testfile.png"
    assert spec_validate_pdf(test_file_1)

    test_file_2 = MagicMock()
    test_file_2.name = "testfile.pdf"
    assert spec_validate_pdf(test_file_2)


def test_uploadable_path():
    test_uploadable_1 = MagicMock()
    test_uploadable_1.__class__.__name__ = "document"

    assert spec_uploadable_path(test_uploadable_1, "testfile.pdf")

    test_uploadable_2 = MagicMock()
    test_uploadable_2.__class__.__name__ = "document"

    with patch("django.conf.settings.DOCUWEB_SAFETY_NBYTES", None):
        assert spec_uploadable_path(test_uploadable_2, "testfile.pdf")


def test_model_strs():
    class ProxyUploadable(Uploadable):
        pass

    # necessary to avoid typeerror: you can't instantiate abstract classes

    test_uploadable = ProxyUploadable(title="Title")
    assert spec_uploadable_str(test_uploadable)

    class ProxyCategory(Category):
        pass

    # necessary to avoid typeerror: you can't instantiate abstract classes

    test_category = ProxyCategory(name="Name")
    assert spec_category_str(test_category)
