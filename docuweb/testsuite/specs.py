from os import path
from secrets import token_urlsafe
from unittest.mock import patch

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models.fields.files import FileField

from docuweb.models import Category, Uploadable, uploadable_path, validate_pdf


def spec_validate_pdf(filepath: FileField) -> bool:
    ret_val = None
    exception = None

    try:
        ret_val = validate_pdf(filepath)
    except ValidationError as err:
        exception = err

    (filename, ext) = path.splitext(filepath.name)

    if ext != ".pdf":
        return type(exception) is ValidationError

    return ret_val is None


@patch("docuweb.models.token_urlsafe")
@patch("docuweb.models.logger")
def spec_uploadable_path(
    instance: Uploadable, filepath, logger_mock, token_urlsafe_mock
) -> bool:
    token_urlsafe_mock.return_value = token_urlsafe(32)

    ret_val = uploadable_path(instance, filepath)

    safety_nbytes = getattr(settings, "DOCUWEB_SAFETY_NBYTES", None)
    print(settings.DOCUWEB_SAFETY_NBYTES)
    if safety_nbytes is None:
        safety_nbytes = 32
        if not logger_mock.called_once():
            return False

    (name, ext) = path.splitext(filepath)

    filename = f"{token_urlsafe_mock.return_value}{ext}"
    new_filepath = path.join(
        "docuweb", str.lower(instance.__class__.__name__), filename
    )

    called: bool = token_urlsafe_mock.called_once_with(safety_nbytes)
    return ret_val == new_filepath and called


def spec_uploadable_str(instance: Uploadable):
    return instance.__str__() == str(instance.title)


def spec_category_str(instance: Category):
    return instance.__str__() == str(instance.name)
