from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import RequestFactory, TestCase

from authentication.models import User
from docuweb.models import Document, DocumentCategory
from docuweb.views import DocumentListView
from members.models.person import Person


class DocumentListViewUnitTest(TestCase):
    def setUp(self):
        minutes_category = DocumentCategory.objects.create(name="Minutes")
        self.doc1 = Document.objects.create(
            title="Minutes 1", file=None, category=minutes_category
        )
        self.doc2 = Document.objects.create(
            title="Minutex 2", file=None, category=minutes_category
        )

    def test_get_queryset_without_query(self):
        request = RequestFactory().get(path="/")
        view = DocumentListView()
        view.setup(request)

        queryset = view.get_queryset()
        self.assertIn(self.doc1, queryset)
        self.assertIn(self.doc2, queryset)

    def test_get_queryset_with_query_1(self):
        request = RequestFactory().get(path="/", data={"q": "minutes"})
        view = DocumentListView()
        view.setup(request)

        queryset = view.get_queryset()
        self.assertIn(self.doc1, queryset)
        self.assertNotIn(self.doc2, queryset)

    def test_get_queryset_with_query_2(self):
        request = RequestFactory().get(path="/", data={"q": "minutex"})
        view = DocumentListView()
        view.setup(request)

        queryset = view.get_queryset()
        self.assertNotIn(self.doc1, queryset)
        self.assertIn(self.doc2, queryset)

    def test_get_queryset_empty_result(self):
        request = RequestFactory().get(path="/", data={"q": "minutez"})
        view = DocumentListView()
        view.setup(request)

        queryset = view.get_queryset()
        self.assertNotIn(self.doc1, queryset)
        self.assertNotIn(self.doc2, queryset)

    def test_get_context_data(self):
        request = RequestFactory().get(path="/", data={"q": "minutes"})
        view = DocumentListView()
        view.setup(request)

        view.object_list = view.get_queryset()
        context_data = view.get_context_data()
        self.assertIn("q", context_data)


class DocumentListViewIntegrationTest(TestCase):
    def setUp(self):
        minutes_category = DocumentCategory.objects.create(name="Minutes")
        self.doc1 = Document.objects.create(
            title="Minutes 1", file=None, category=minutes_category
        )
        self.doc1.file = SimpleUploadedFile("Minutes 1.pdf", b"some test data")
        self.doc1.save()

        person, contact = Person.create_with_contact(
            legal_name="Admin van Adminstein",
            full_name="Adje van Stein",
            given_name="Ad",
            email="admin@example.com",
        )
        self.user = User.objects.create_user(
            username="admin", person=person.pk, password="secret"
        )

    def test_view_logged_in(self):
        if not self.client.login(username="admin", password="secret"):
            raise Exception("Improperly configured test.")

        response = self.client.get("/service/docuweb/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed("docuweb/document_list.html")

    def test_view_not_logged_in(self):
        response = self.client.get("/service/docuweb/")
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, expected_url="/accounts/login/?next=/service/docuweb/"
        )
        self.assertTemplateUsed("docuweb/document_list.html")
