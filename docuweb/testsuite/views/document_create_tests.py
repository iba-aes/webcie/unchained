import pytest
from django.contrib.auth.models import Permission
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import RequestFactory, TestCase

from authentication.models import User
from docuweb.models import Document, DocumentCategory
from docuweb.views import DocumentCreateView
from members.models import Person


class DocumentCreateViewUnitTests(TestCase):
    def setUp(self):
        minutes_category = DocumentCategory.objects.create(name="Minutes")
        finance_category = DocumentCategory.objects.create(name="Finance")
        self.doc1 = Document.objects.create(
            title="Minutes 1", file=None, category=minutes_category
        )
        self.doc2 = Document.objects.create(
            title="Finance 1", file=None, category=finance_category
        )

    pass


class DocumentCreateViewIntegrationTests(TestCase):
    def setUp(self):
        self.minutes_category = DocumentCategory.objects.create(name="Minutes")
        self.doc1 = Document.objects.create(
            title="Minutes 1", file=None, category=self.minutes_category
        )
        self.doc1.file = SimpleUploadedFile("Minutes 1.pdf", b"some test data")
        self.doc1.save()

        admin_person, _ = Person.create_with_contact(
            legal_name="Admin van Adminstein",
            full_name="Adje van Stein",
            given_name="Ad",
            email="admin@example.com",
        )
        self.admin_user: User = User.objects.create_user(
            username="admin", person=admin_person.pk, password="secret"
        )

        document_add_permission = Permission.objects.get(codename="add_document")
        self.admin_user.user_permissions.add(document_add_permission)

        regular_person, _ = Person.create_with_contact(
            legal_name="Lidje van Lidjestein",
            full_name="Lidje van Stein",
            given_name="Lidje",
            email="lidje@example.com",
        )
        self.regular_user = User.objects.create_user(
            username="lidje", person=regular_person.pk, password="evenM0reSecret"
        )

    def test_view__get_not_logged_in(self):
        response = self.client.get("/service/docuweb/toevoegen/")
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, expected_url="/accounts/login/?next=/service/docuweb/toevoegen/"
        )
        self.assertTemplateUsed("docuweb/document_create_form.html")

    def test_view__get_logged_in_and_no_permission(self):
        if not self.client.login(username="lidje", password="evenM0reSecret"):
            raise Exception("Improperly configured test.")

        response = self.client.get("/service/docuweb/toevoegen/")
        self.assertEqual(response.status_code, 403)
        self.assertTemplateUsed("docuweb/document_create_form.html")

    def test_view_get(self):
        if not self.client.login(username="admin", password="secret"):
            raise Exception("Improperly configured test.")

        response = self.client.get("/service/docuweb/toevoegen/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed("docuweb/document_create_form.html")

    def test_view__post_not_logged_in(self):
        response = self.client.post("/service/docuweb/toevoegen/", {})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, expected_url="/accounts/login/?next=/service/docuweb/toevoegen/"
        )
        self.assertTemplateUsed("docuweb/document_create_form.html")

    def test_view__post_logged_in_and_no_permission(self):
        if not self.client.login(username="lidje", password="evenM0reSecret"):
            raise Exception("Improperly configured test.")

        response = self.client.post("/service/docuweb/toevoegen/", {})
        self.assertEqual(response.status_code, 403)
        self.assertTemplateUsed("docuweb/document_create_form.html")

    def test_view__post(self):
        if not self.client.login(username="admin", password="secret"):
            raise Exception("Improperly configured test.")

        from io import BytesIO

        img = BytesIO(b"mybinarydata")
        img.name = "myimage.pdf"

        response = self.client.post(
            "/service/docuweb/toevoegen/",
            {"title": "Minutes 2", "category": self.minutes_category.pk, "file": img},
            follow=True,
        )
        self.assertRedirects(response, expected_url="/service/docuweb/")
        self.assertNotEqual(len(list(response.context["messages"])), 0)
        self.assertIn(
            "Minutes 2 was created successfully",
            [str(m) for m in list(response.context["messages"])],
        )

    def test_view__post_error(self):
        if not self.client.login(username="admin", password="secret"):
            raise Exception("Improperly configured test.")

        response = self.client.post(
            "/service/docuweb/toevoegen/",
            {
                "title": "Minutes 2",
                "category": self.minutes_category.pk,
                "file": "test",
            },
            follow=True,
        )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed("docuweb/document_create_form.html")
        self.assertGreater(len(list(response.context["form"].errors)), 0)
