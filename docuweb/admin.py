from django.contrib import admin

from .models import Document, DocumentCategory, Manual, ManualCategory


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "date_uploaded",
        "date_last_changed",
        "title",
        "category",
        "file",
    )
    list_display_links = ("title",)


@admin.register(DocumentCategory)
class DocumentCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Manual)
class ManualAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "date_uploaded",
        "date_last_changed",
        "title",
        "category",
        "file",
    )
    list_display_links = ("title",)


@admin.register(ManualCategory)
class ManualCategoryAdmin(admin.ModelAdmin):
    pass
