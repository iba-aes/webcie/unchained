from __future__ import annotations

import logging
import math
from os import path
from secrets import token_urlsafe

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.fields.files import FieldFile, FileField
from django.utils.timezone import datetime
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


def validate_pdf(filepath: FileField):
    """Validates a file by checking if it is a PDF.

    Todo:
        * There should also be a check to see if the content of the file is also in PDF format,
            instead of only checking the extension.
    """

    (filename, ext) = path.splitext(filepath.name)
    if ext != ".pdf":
        raise ValidationError(
            _('File "%(file)s" should be a PDF.'), params={"file": filepath.name}
        )


def uploadable_path(instance: Uploadable, filepath):
    """Creates a path for the given Uploadable instance."""

    safety_nbytes = getattr(settings, "DOCUWEB_SAFETY_NBYTES", None)

    # The amount of safety bytes defaults to 32.
    if safety_nbytes is None:
        safety_nbytes = 32
        logger.warning(
            _("[WARNING] DOCUWEB_SAFETY_NBYTES is not defined in the settings module.")
        )

    (name, ext) = path.splitext(filepath)

    filename = f"{token_urlsafe(safety_nbytes)}{ext}"
    return path.join("docuweb", str.lower(instance.__class__.__name__), filename)


# Abstract classes
class Uploadable(models.Model):
    """An abstract class for Uploadable documents."""

    title: str = models.CharField(max_length=128, unique=True)
    date_uploaded: datetime = models.DateTimeField(auto_now_add=True)
    date_last_changed: datetime = models.DateTimeField(auto_now=True)
    file: FieldFile = models.FileField(
        upload_to=uploadable_path, validators=[validate_pdf]
    )

    class Meta:
        abstract = True
        ordering = ["id"]

    @property
    def filesize_human_readable(self) -> str:
        """Reads the filesize in bytes and converts it to a nice, human readable format."""

        size_bytes = self.file.size
        size_names = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
        i = int(math.floor(math.log(size_bytes, 1024)))
        power = math.pow(1024, i)
        size = round(size_bytes / power, 2)
        return f"{size:.3f} {size_names[i]}"

    def __str__(self) -> str:
        return str(self.title)


class Category(models.Model):
    """An abstract class for Uploadable Categories."""

    name = models.CharField(max_length=256)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.name)


# Documents
class Document(Uploadable):
    """A general document, like BV-minutes or AV-presentations."""

    category: DocumentCategory = models.ForeignKey(
        to="docuweb.DocumentCategory", on_delete=models.PROTECT
    )


class DocumentCategory(Category):
    """A Category model for the Document model."""

    pass


# Manuals
class Manual(Uploadable):
    """A manual document, like the treasurer manual."""

    category: ManualCategory = models.ForeignKey(
        to="docuweb.ManualCategory", on_delete=models.PROTECT
    )


class ManualCategory(Category):
    """A Category model for the Manual model."""

    pass
