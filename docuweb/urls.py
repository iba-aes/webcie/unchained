from django.urls import path

from docuweb.views import DocumentCreateView, DocumentListView

app_name = "docuweb"
urlpatterns = [
    path("", DocumentListView.as_view(), name="list"),
    path("toevoegen/", DocumentCreateView.as_view(), name="create"),
]
