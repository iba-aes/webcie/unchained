from django.apps import AppConfig


class DocuwebConfig(AppConfig):
    name = "docuweb"
