from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView

from docuweb.forms import DocumentCreateForm
from docuweb.models import Document


class DocumentListView(LoginRequiredMixin, ListView):
    model = Document
    paginate_by = 10

    template_name = "docuweb/document_list.html"
    context_object_name = "document_list"

    def get_queryset(self):
        result = super().get_queryset()
        query = self.request.GET.get("q")
        if query:
            return result.filter(title__icontains=query)
        else:
            return result

    def get_context_data(self, **kwargs):
        kwargs["q"] = self.request.GET.get("q")
        kwargs["placeholder"] = "Zoek een document..."
        return super().get_context_data(**kwargs)


class DocumentCreateView(
    LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView
):
    model = Document
    form_class = DocumentCreateForm
    template_name_suffix = "_create_form"
    success_url = reverse_lazy("docuweb:list")
    success_message = "%(title)s was created successfully"
    permission_required = "docuweb.add_document"
