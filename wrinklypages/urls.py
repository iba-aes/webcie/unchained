from django.urls import path

from .views import (
    WrinklyPageCreateView,
    WrinklyPageDeleteView,
    WrinklyPageUpdateView,
    wrinklyPageView,
)

urlpatterns = [
    path("pagina/nieuw/", WrinklyPageCreateView.as_view(), name="wrinklypage-create"),
    path("pagina/<int:pk>/", wrinklyPageView, name="wrinklypage"),
    path(
        "pagina/<int:pk>/aanpassen/",
        WrinklyPageUpdateView.as_view(),
        name="wrinklypage-edit",
    ),
    path(
        "pagina/<int:pk>/verwijderen/",
        WrinklyPageDeleteView.as_view(),
        name="wrinklypage-delete",
    ),
]
