from django.db import models
from django.db.models import Q
from django.utils.translation import gettext as _


class RedirectDestination(models.Model):
    """Represents a single link destination."""

    name = models.CharField(max_length=50, unique=True, null=False)
    next_url = models.URLField(
        _("next url"), help_text="the url to redirect to", unique=True
    )

    def __str__(self) -> str:
        return f"Redirect Destination {self.name}, redirecting to {self.next_url}"

    def __repr__(self) -> str:
        return (
            f"RedirectDestination_{self.pk} = {{"
            f"name:{self.name}, "
            f"next_url:{self.next_url}, "
            f"}}"
        )

    @property
    def total_url(self):
        return self.next_url

    class Meta:
        permissions = [
            ("redirect_notify", _("get shown a link when you would be redirected"))
        ]
