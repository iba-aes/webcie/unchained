from django.db import models
from django.db.models import Q
from django.utils.translation import gettext as _

from .localurlfield import LocalUrlField
from .redirectdestination import RedirectDestination
from .wrinklypage import WrinklyPage


# Usually this would be defined in the model class,
# but added this here instead.
# Otherwise, we can't access it in the Meta class.
class _ContentType(models.TextChoices):
    """
    Specifies the behaviour of an UrlSpace:
    Redirect Destination:
    - This UrlSpace redirects.
    - The destination is in the RedirectDestination object
    - REQUIRES the `redirect_destination` field to be set
    Wrinkly Page:
    - This UrlSpace shows a wrinkly page
    - The contents of the page is in the WrinklyPage object
    - REQUIRES the `wrinkly_page` field to be set
    Reserved:
    - This UrlSpace currently does not show content
    - This may be because it is not yet connected to a WrinklyPage or a RedirectDestination
    - It may also be because someone wanted to (temporarily) disable the page/redirect
    - or in general when you want to make sure this UrlSpace isn't used
    """

    REDIRECT_DESTINATION = ("Rd", _("Redirect Destination"))
    WRINKLY_PAGE = ("Wp", _("Wrinkly Page"))
    RESERVED = ("Re", _("Reserved"))
    __empty__ = _("(Unknown)")


class UrlSpace(models.Model):
    """Represents a single url-path."""

    url = LocalUrlField(_("Local Url"), unique=True, db_index=True, null=False)
    # TODO: Add security options to stop anons from being able to access all wrinklypages.

    ContentType = _ContentType

    contenttype = models.CharField(
        _("Content type"),
        choices=ContentType.choices,
        max_length=2,
        null=False,
        default=ContentType.RESERVED,
    )

    wrinkly_page = models.OneToOneField(
        WrinklyPage,
        verbose_name=_("Maybe Wrinkly Page"),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )

    redirect_destination = models.ForeignKey(
        RedirectDestination,
        verbose_name=_("Maybe Redirect Destination"),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )

    def __str__(self) -> str:
        if self.contenttype == self.ContentType.WRINKLY_PAGE:
            return f"UrlSpace at {self.url} with Wrinkly Page {self.wrinkly_page}"
        elif self.contenttype == self.ContentType.REDIRECT_DESTINATION:
            return f"UrlSpace at {self.url} part of Redirect Destination to {self.redirect_destination}"
        elif self.contenttype == self.ContentType.RESERVED:
            return f"Reserved UrlSpace at {self.url}"
        else:
            return f"UrlSpace at {self.url} with Unknown content"

    def __repr__(self) -> str:
        return (
            f"UrlSpace_{self.id} = {{"
            f"url:{self.url}, "
            f"contenttype:{self.contenttype}, "
            f"wrinkly_page:{self.wrinkly_page}, "
            f"redirect_destination:{self.redirect_destination}"
            f"}}"
        )

    class Meta:
        # We want to make sure that if the contenttype says there is something on the page,
        # the object knows what that something is.
        # We do this by adding these database constraints.
        constraints = [
            models.CheckConstraint(
                check=~Q(contenttype=_ContentType.WRINKLY_PAGE)
                | Q(wrinkly_page__isnull=False),
                name="if_url_is_wrinkly_then_url_has_wrinklypage",
            ),
            models.CheckConstraint(
                check=~Q(contenttype=_ContentType.REDIRECT_DESTINATION)
                | Q(redirect_destination__isnull=False),
                name="if_url_is_redirect_then_url_has_redirectdestination",
            ),
        ]
