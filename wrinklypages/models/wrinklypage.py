from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext as _
from lxml.html.clean import Cleaner
from tinymce.models import HTMLField

# see https://lxml.de/api/lxml.html.clean.Cleaner-class.html for docs on this class
cleaner = Cleaner(
    scripts=True,  # remove <script> tags
    javascript=False,  # don't remove stylesheets (the onclick gets removed anyway)
    style=False,  # don't remove <style> tags
    inline_style=False,  # don't remove style attributes
    host_whitelist=[
        "www.a-eskwadraat.nl",
        "a-eskwadraat.nl",
    ]  # allow links to our site for content.
    + (["localhost", "127.0.0.1"] if settings.DEBUG else []),
)


class WrinklyPage(models.Model):
    """Models a simple page with no varying content."""

    name = models.CharField(max_length=50)
    title = models.CharField(_("title"), max_length=30, blank=True)
    content = HTMLField(
        _("content"),
    )
    content.pre_save = lambda model_instance, add: cleaner.clean_html(
        model_instance.content
    )

    def __str__(self) -> str:
        return f"Wrinkly page {self.name}"

    def __repr__(self) -> str:
        return (
            f"WrinklyPage_{self.pk} = {{"
            f"name:{self.name}, "
            f"title:{self.title}, "
            f"content:{self.content}, "
            f"}}"
        )

    def get_absolute_url(self):
        return reverse("wrinklypage", kwargs={"pk": self.pk})
