from django import forms
from django.core import validators
from django.db import models
from django.utils.translation import gettext as _

# This file defines a modelfield for storing relative urls,
# along with whatever else it needs.
unreserved_characters = r"[A-Za-z0-9!*'(),$\-_.+]"
escape = r"%([A-Fa-f0-9]){2}"  # Match percent char followed by two hexadecimal chars.
uchar = f"{unreserved_characters}|{escape}"
slugchar = f"{uchar}|[;:@&=]"

regex = rf"^/(({slugchar})*/)+?$"
# ^ -> start of string
# / -> match a slash character
# ( -> start a group
#   ( -> start a group
#       {slugchar} -> expands to something matching any character pattern
#                     allowed in slugs
#   ) -> end group
#   * -> match any number of the previous group in succession
#   / -> match a slash character
# ) -> end group
# + -> match the previous group at least once or more.
# ? -> the previous match shouldn't be greedy
# $ -> match the end of the string

local_url_validator = validators.RegexValidator(
    regex=regex,
)


class LocalUrlField(models.CharField):
    default_validators = [local_url_validator]
    description = _("URL_Path")

    def __init__(self, verbose_name=None, name=None, **kwargs):
        kwargs.setdefault("max_length", 200)
        super().__init__(verbose_name, name, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        if kwargs.get("max_length") == 200:
            del kwargs["max_length"]
        return name, path, args, kwargs

    def formfield(self, **kwargs):
        # As with CharField, this will cause URL validation to be performed
        # twice.
        return super().formfield(
            **{
                "form_class": LocalUrlFormField,
                **kwargs,
            }
        )


class LocalUrlFormField(forms.CharField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("validators", [])
        kwargs["validators"].append(local_url_validator)
        super().__init__(*args, **kwargs)
