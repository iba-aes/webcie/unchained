from typing import Callable

from django.http import Http404, HttpRequest, HttpResponse
from django.urls import is_valid_path

from .views import urlSpaceView

# Type alias FTW.
# Also, viewLIKE because it might be a view wrapped in any number of middleware layers.
ViewLike = Callable[
    [
        HttpRequest,
    ],
    HttpResponse,
]


class WrinklyMiddleware:
    def __init__(self, get_response: ViewLike):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        urlconf = getattr(request, "urlconf", None)
        if not is_valid_path(request.path_info, urlconf):
            try:
                return urlSpaceView(request)
            except Http404:
                return self.get_response(request)
        else:
            return self.get_response(request)

        # I don't think we want to catch valid urls if they fail.
        # The flatpages app isn't a substitute for making proper entries
        # in the database... but in case we need it:

        # else:
        #     try:
        #         response : HttpResponse = get_response(request)
        #     except Http404:
        #         return urlSpaceView(request)
        #     else:
        #         return response
