from django.apps import AppConfig


class WrinklyPagesConfig(AppConfig):
    name = "wrinklypages"

    def ready(self):
        """
        This runs after the app is instanciated, for instance when starting the server.
        Important: this is the only place in this file we can assume the models to be loaded.
        """

        # Register the signals for this app.
        # Registering signals can only be done when the models are loaded.
        # Signals should only be registered once, which is why it is done here
        # (as soon as the app is loaded).
        from . import signals
