from django.contrib import admin

from .models import RedirectDestination, UrlSpace, WrinklyPage


@admin.register(RedirectDestination)
class RedirectDestinationAdmin(admin.ModelAdmin):
    pass


@admin.register(UrlSpace)
class UrlSpaceAdmin(admin.ModelAdmin):
    pass


@admin.register(WrinklyPage)
class WrinklyPageAdmin(admin.ModelAdmin):
    pass
