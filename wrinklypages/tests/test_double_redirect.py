from django.test import TestCase

from ..models import RedirectDestination, UrlSpace, WrinklyPage


class DoubleRedirectAbstractTestCase:
    def setUp(self):
        # Create the first redirect.
        self.redirectOne = RedirectDestination(
            name="redirect1", next_url="http://localhost:8000/test1/"
        )
        # Create the second redirect and save it in the testDB,
        # as double redirect tests should behave the same,
        # regardless of the contents of the second redirect.
        self.redirectTwo = RedirectDestination(
            name="redirect2", next_url="http://localhost:8000/test2/"
        )
        self.redirectTwo.save()

        # Create the first urlspace.
        self.urlspaceOne = UrlSpace(
            url="/start/",
            contenttype=UrlSpace.ContentType.REDIRECT_DESTINATION,
            redirect_destination=self.redirectOne,
        )

        # Create the second urlspace.
        self.urlspaceTwo = UrlSpace(
            url="/test1/",
            contenttype=UrlSpace.ContentType.REDIRECT_DESTINATION,
            redirect_destination=self.redirectTwo,
        )

        # Create a wrinklypage and save it,
        # because these tests are only about the redirects,
        # not the actual contents of urls.
        self.wrinklypage = WrinklyPage(
            name="testpage",
            title="testpagina",
            content="<h1> weewooo het is een testpagina, hoera! </h1>",
        )
        self.wrinklypage.save()

    def saveEverything(self):
        raise NotImplementedError(
            "This class is not to be run directly as tests.\n"
            "Use dual inheritance with this class instead, like so:\n"
            "class YourClass(DoubleRedirectAbstractTestCase, django.tests.TestCase):\n"
            "    ..."
        )

    # If the first urlspace is not redirecting but reserved, we should be gucci.
    def testSecondUrlspaceReservedSucceeds(self):
        self.urlspaceTwo.contenttype = UrlSpace.ContentType.RESERVED

        self.saveEverything()

    # If the second urlspace is not redirecting but reserved, we should be gucci.
    def testFirstUrlspaceReservedSucceeds(self):
        self.urlspaceOne.contenttype = UrlSpace.ContentType.RESERVED

        self.saveEverything()

    # If the first urlspace is not redirecting but wrinkly, we should be gucci.
    def testFirstUrlWrinklySucceeds(self):
        self.urlspaceOne.wrinkly_page = self.wrinklypage
        self.urlspaceOne.contenttype = UrlSpace.ContentType.WRINKLY_PAGE

        self.saveEverything()

    # If the second urlspace is not redirecting but wrinkly, we should be gucci.
    def testSecondUrlspaceWrinklySucceeds(self):
        self.urlspaceTwo.wrinkly_page = self.wrinklypage
        self.urlspaceTwo.contenttype = UrlSpace.ContentType.WRINKLY_PAGE

        self.saveEverything()

    # If the first redirect urlpath doesn't point to the second urlspace, we should be gucci.
    def testDifferentUrlPathSucceeds(self):
        self.redirectOne.next_url = "http://localhost/test2/"

        self.saveEverything()

    # If the first redirect host isn't an allowed host, we should be gucci.
    def testDifferentUrlHostSucceeds(self):
        self.redirectOne.next_url = "http://some.other.host/test1/"

        self.saveEverything()


# These tests check that saving a redirectdestination cannot
# cause a double redirect to exist in the database.
class DoubleRedirectFirstRedirectTestCase(DoubleRedirectAbstractTestCase, TestCase):
    def setUp(self):
        # NOTE: Python inheritance means this is DoubleRedirectAbstractTestCase.setUp,
        # and not TestCase.setUp which does nothing by default.
        super().setUp()
        self.redirectOne.next_url = "https://localhost/placeholder/"
        self.redirectOne.save()
        self.redirectOne.next_url = "https://localhost/test1/"

    def saveEverything(self):
        self.urlspaceOne.save()
        self.urlspaceTwo.save()
        self.redirectOne.save()

    # First urlspace points to second points somewhere: no gucci.
    def testFirstRedirectSaveFails(self):
        self.urlspaceTwo.save()
        self.urlspaceOne.save()

        self.assertRaises(ValueError, self.redirectOne.save)


# These tests are to test that saving a urlspace cannot cause it to become second in a
# redirect chain.
class DoubleRedirectSecondUrlSpaceTestCase(DoubleRedirectAbstractTestCase, TestCase):
    def saveEverything(self):
        self.redirectOne.save()
        self.urlspaceOne.save()
        self.urlspaceTwo.save()

    # First urlspace points to second points somewhere: no gucci.
    def testSecondUrlSpaceSaveFails(self):
        self.redirectOne.save()
        self.urlspaceOne.save()
        self.assertRaises(ValueError, self.urlspaceTwo.save)


# These tests are to test that saving a urlspace cannot cause it to become first in a
# redirect chain.
class DoubleRedirectFirstUrlspaceTestCase(DoubleRedirectAbstractTestCase, TestCase):
    # First urlspace points to second points somewhere: no gucci.
    def testFirstUrlSpaceSaveFails(self):
        self.redirectOne.save()
        self.urlspaceTwo.save()
        self.assertRaises(ValueError, self.urlspaceOne.save)

    def saveEverything(self):
        self.redirectOne.save()
        self.urlspaceTwo.save()
        self.urlspaceOne.save()
