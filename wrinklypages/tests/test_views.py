from django.conf import settings
from django.test import TestCase

from authentication.models import User
from members.models import Person

from ..models import RedirectDestination, UrlSpace, WrinklyPage


def createPersons():
    person1, _ = Person.create_with_contact(
        legal_name="hoi",
        full_name="Mao Zedong",
        given_name="Zedong",
        email="great-leader@example.com",
    )

    person2, _ = Person.create_with_contact(
        legal_name="hi", full_name="Maedong", given_name="Zeg", email="grr@example.com"
    )

    person1.save()
    person2.save()

    return person1, person2


class ViewTestCase(TestCase):
    """Test that the views work and that the flatpages are being shown."""

    def setUp(self):
        self.wrinklypage = WrinklyPage.objects.create(
            name="testpage", title="testpage", content="<h1>testpage</h1>"
        )
        self.wrinkly_urlspace = UrlSpace.objects.create(
            url="/test1/",
            contenttype=UrlSpace.ContentType.WRINKLY_PAGE,
            wrinkly_page=self.wrinklypage,
        )
        self.redirectdestination = RedirectDestination.objects.create(
            name="test_redirect", next_url="https://test.example.com/test/"
        )
        self.redirect_urlspace = UrlSpace.objects.create(
            url="/test2/",
            contenttype=UrlSpace.ContentType.REDIRECT_DESTINATION,
            redirect_destination=self.redirectdestination,
        )

        self.myPerson, self.otherPerson = createPersons()

        self.user = User.objects.create_user("username", self.myPerson.pk, "password")

        self.client.force_login(self.user)

    def testWrinklyPage(self):
        response = self.client.get("/test1/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "wrinklypages/wrinklypage.html")
        self.assertEqual(response.context["wrinkly_page"], self.wrinklypage)

    def testSlashAppendage(self):
        response = self.client.get("/home")
        if settings.APPEND_SLASH:
            self.assertEqual(response.status_code, 301)
            self.assertEqual(response.headers["Location"], "/home/")
        else:
            self.assertEqual(response.status_code, 404)

    def testRedirectPage(self):
        response = self.client.get("/test2/")
        self.assertRedirects(
            response=response,
            expected_url="https://test.example.com/test/",
            fetch_redirect_response=False,
        )

    def testWrinklyPageTurnedOff(self):
        self.wrinkly_urlspace.contenttype = UrlSpace.ContentType.RESERVED
        self.wrinkly_urlspace.save()
        response = self.client.get("/test1/")
        self.assertEqual(response.status_code, 404)

    def testRedirectPageTurnedOff(self):
        self.redirect_urlspace.contenttype = UrlSpace.ContentType.RESERVED
        self.redirect_urlspace.save()
        response = self.client.get("/test2/")
        self.assertEqual(response.status_code, 404)
