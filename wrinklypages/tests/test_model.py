from django.db import IntegrityError
from django.test import TestCase

from authentication.models import User
from members.models import Person

from ..models import RedirectDestination, UrlSpace, WrinklyPage


def createPersons():
    person1, _ = Person.create_with_contact(
        legal_name="hoi",
        full_name="Mao Zedong",
        given_name="Zedong",
        email="great-leader@example.com",
    )

    person2, _ = Person.create_with_contact(
        legal_name="hi", full_name="Maedong", given_name="Zeg", email="grr@example.com"
    )

    person1.save()
    person2.save()

    return person1, person2


class InternalModelTestCase(TestCase):
    """Test that constraints (for example uniqueness) within the models are enforced."""

    def setUp(self):
        self.wrinklypage = WrinklyPage.objects.create(
            name="testpage", title="testpage", content="<h1>tespage</h1>"
        )
        self.wrinkly_urlspace = UrlSpace.objects.create(
            url="/test1/",
            contenttype=UrlSpace.ContentType.WRINKLY_PAGE,
            wrinkly_page=self.wrinklypage,
        )
        self.redirectdestination = RedirectDestination.objects.create(
            name="test_redirect", next_url="https://test.example.com/test/"
        )
        self.redirect_urlspace = UrlSpace.objects.create(
            url="/test2/",
            contenttype=UrlSpace.ContentType.REDIRECT_DESTINATION,
            redirect_destination=self.redirectdestination,
        )

        self.myPerson, self.otherPerson = createPersons()

        self.user = User.objects.create_user("username", self.myPerson.pk, "password")

        self.client.force_login(self.user)

    def testUrlSpaceUrlIsUnique(self):
        self.assertRaises(
            IntegrityError,
            UrlSpace.objects.create,
            url="/test1/",
            contenttype=UrlSpace.ContentType.RESERVED,
        )

    def testRedirectUrlSpaceNeedsRedirect(self):
        self.assertRaises(
            IntegrityError,
            UrlSpace.objects.create,
            url="/test3/",
            contenttype=UrlSpace.ContentType.REDIRECT_DESTINATION,
        )

    def testWrinklyUrlSpaceIsWrinkly(self):
        self.assertRaises(
            IntegrityError,
            UrlSpace.objects.create,
            url="/test3/",
            contenttype=UrlSpace.ContentType.WRINKLY_PAGE,
        )

    def testDeleteWrinklyPageMakesUrlReserved(self):
        self.assertEqual(
            self.wrinkly_urlspace.contenttype, UrlSpace.ContentType.WRINKLY_PAGE
        )
        self.wrinklypage.delete()
        self.wrinkly_urlspace.refresh_from_db()
        self.assertEqual(
            self.wrinkly_urlspace.contenttype,
            UrlSpace.ContentType.RESERVED,
        )

    def testDeleteRedirectMakesUrlReserved(self):
        self.assertEqual(
            self.redirect_urlspace.contenttype,
            UrlSpace.ContentType.REDIRECT_DESTINATION,
        )

        self.redirectdestination.delete()
        self.redirect_urlspace.refresh_from_db()
        self.assertEqual(
            self.redirect_urlspace.contenttype, UrlSpace.ContentType.RESERVED
        )

    def testRemovesScriptTag(self):
        self.wrinklypage.content = '<script src="www.example.com/test.js"></script>'
        self.wrinklypage.save()
        self.wrinklypage.refresh_from_db()
        self.assertEqual(self.wrinklypage.content, "<div></div>")

    def testRemoveScriptAttrs(self):
        self.wrinklypage.content = (
            '<img src="www.example.com/test.js" onerror="alert("boo")">'
        )
        self.wrinklypage.save()
        self.wrinklypage.refresh_from_db()
        self.assertEqual(
            self.wrinklypage.content, '<img src="www.example.com/test.js">'
        )
        self.wrinklypage.content = (
            '<a href="www.example.com/test/" onclick="alert("boo")">click me</a>'
        )
        self.wrinklypage.save()
        self.wrinklypage.refresh_from_db()
        self.assertEqual(
            self.wrinklypage.content, '<a href="www.example.com/test/">click me</a>'
        )

    def testKeepsInlineStyle(self):
        self.wrinklypage.content = '<p style="color:red;">blue</p>'
        self.wrinklypage.save()
        self.wrinklypage.refresh_from_db()
        self.assertEqual(self.wrinklypage.content, "<p>blue</p>")

    def testKeepsStyleElement(self):
        self.wrinklypage.content = "<div><style>p {color: #26b72b;}</style></div>"
        self.wrinklypage.save()
        self.wrinklypage.refresh_from_db()
        self.assertEqual(
            self.wrinklypage.content, "<div><style>p {color: #26b72b;}</style></div>"
        )
