from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import Http404, HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from .models import RedirectDestination, UrlSpace, WrinklyPage


def urlSpaceView(request: HttpRequest) -> HttpResponse:
    url_space: UrlSpace = get_object_or_404(UrlSpace, url=request.path_info)
    match url_space.contenttype:
        case UrlSpace.ContentType.REDIRECT_DESTINATION:
            redirect_destination = url_space.redirect_destination
            return customRedirectView(request, url_space.redirect_destination)
        case UrlSpace.ContentType.WRINKLY_PAGE:
            return wrinklyPageView(request, url_space.wrinkly_page.pk)
        case UrlSpace.ContentType.RESERVED:
            raise Http404("page content not found")
        case _:
            raise Http404("url type not recognized")


def customRedirectView(
    request: HttpRequest, redirect_destination: RedirectDestination
) -> HttpResponse:
    return redirect(redirect_destination.total_url)


def wrinklyPageView(request: HttpRequest, pk: int) -> HttpResponse:
    wrinkly_page = WrinklyPage.objects.get(pk=pk)
    context = {"wrinkly_page": wrinkly_page}
    return render(
        request, template_name="wrinklypages/wrinklypage.html", context=context
    )


class WrinklyPageCreateView(PermissionRequiredMixin, CreateView):
    permission_required = ["wrinklypages.add_wrinklypage"]
    model = WrinklyPage
    fields = ["name", "title", "content"]


class WrinklyPageUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = ["wrinklypages.change_wrinklypage"]
    model = WrinklyPage
    fields = ["name", "title", "content"]


class WrinklyPageDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = ["wrinklypages.delete_wrinklypage"]
    model = WrinklyPage
    success_url = reverse_lazy("home")
