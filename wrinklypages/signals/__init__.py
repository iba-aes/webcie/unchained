# For an explanation of signals, see
# https://docs.djangoproject.com/en/stable/topics/signals/ .

# By importing these files, we register all the signal callbacks inside them.
from . import checking_signals, correcting_signals
