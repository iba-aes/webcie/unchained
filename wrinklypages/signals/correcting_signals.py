from django.db.models import Model, signals
from django.dispatch import receiver

from ..models import RedirectDestination, UrlSpace, WrinklyPage


@receiver(
    signals.pre_delete,
    sender=WrinklyPage,
    dispatch_uid="change_contenttype_from_wrinkly",
)
def fix_urlspace_before_wrinkle_delete(sender, instance: WrinklyPage, using, **kwargs):
    """
    Whenever a WrinklyPage gets deleted,
    any Foreignkey (OneToOneField) of a UrlSpace to that object get set to Null.
    However, Urlspace also keeps track of what kind of content it has in a field called contenttype.
    Particularly, it is expected that if contenttype signals that it contains a wrinklypage,
    the foreignkey field is not null.
    This means that before we set the foreignkey to null,
    we need to change the contenttype field to something else, in this case to Reserved.
    """

    # NOTE: quick refresher: The python "try ... except ... else ..." pattern does the following:
    # Execute the try-codeblock.
    # When that throws an error which matches the except condition,
    # start executing the except-codeblock.
    # If instead, it completes executing the try-codeblock without throwing an error,
    # execute the else-codeblock.
    try:
        affected_UrlSpace: UrlSpace = instance.urlspace
    except UrlSpace.DoesNotExist:
        pass
    else:
        if affected_UrlSpace.contenttype == UrlSpace.ContentType.WRINKLY_PAGE:
            affected_UrlSpace.contenttype = UrlSpace.ContentType.RESERVED
            affected_UrlSpace.save()


@receiver(
    signals.pre_delete,
    sender=RedirectDestination,
    dispatch_uid="change_contenttype_from_wrinkly",
)
def fix_urlspace_before_redirect_delete(
    sender, instance: RedirectDestination, using, **kwargs
):
    """
    Whenever a RedirectDestination gets deleted, any foreignkey of a UrlSpace to that object get set to Null.
    However, Urlspace also keeps track of what kind of content it has in a field called contenttype.
    Particularly, it is expected that if contenttype signals that it is part of a RedirectDestination,
    the foreignkey field is not null. This means that before we set the foreignkey to null,
    we need to change the contenttype field to something else, in this case to Reserved.
    """
    affected_UrlSpaces = UrlSpace.objects.filter(
        redirect_destination=instance,
        contenttype=UrlSpace.ContentType.REDIRECT_DESTINATION,
    )
    affected_UrlSpaces.update(contenttype=UrlSpace.ContentType.RESERVED)
