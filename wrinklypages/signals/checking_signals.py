"""
The situation to prevent here is one where we have two consecutive redirects,
which can be caused within this app by the following situation:
UrlSpace1 redirects to RedirectDestination1, which has the same url as
UrlSpace2, which redirects to RedirectDestination2.
This is illustrated in the following diagram:
+----------------+
| UrlSpace 1     |                         +---------------------+
| - contenttype= | redirect_destination -> |RedirectDestination 1|
|       redirect |                         +---------------------+
+----------------+                                     | next_url
                                                       |   ||
                                                       V   url
                                                +----------------+
+---------------------+                         | UrlSpace 2     |
|RedirectDestination 2| <- redirect_destination | - contenttype= |
+---------------------+                         |       redirect |
                                                +----------------+
We want to prevent this because this can cause circular redirects,
which are a source of bad UX.
Preventing this situation cannot be done by the database (via django options),
because that requires a constraint between tables, as opposed to on a single model.
See the answer to https://stackoverflow.com/questions/68168924/how-can-i-have-check-constraint-in-django-which-check-two-fields-of-related-mode.
As such, we must restrict this on save.
Secause this takes lots of code, we put it in several pre_save-signal callbacks,
instead of overriding the save() methods for the affected models.
"""
from urllib.parse import urlparse

from django.conf import settings
from django.db.models import Model, signals
from django.dispatch import receiver
from django.http.request import validate_host
from django.utils.translation import gettext as _

from ..models import RedirectDestination, UrlSpace


@receiver(signals.pre_save, sender=UrlSpace, dispatch_uid="no_double_redirect_urlspace")
def error_on_double_redirect_urlspace_save_attempt(
    sender, instance, raw, using, update_fields, **kwargs
):
    """
    In this signal callback we check if the `instance`-urlspace is like urlspace 1,
    or like urlspace 2 (as in the diagram).
    """

    if raw:
        return

    if update_fields:
        try:
            updated_instance = UrlSpace.objects.get(instance.pk)
        except UrlSpace.DoesNotExist:
            pass
        else:
            for field in update_fields:
                setattr(updated_instance, field, getattr(instance, field))
            instance = updated_instance

    # If the contenttype is redirect, but the foreign key doesn't point to one,
    # we don't throw an error. This is because the database will do that for us,
    # and that error is more important (read: informative) to show than this one.
    # That is because the database error explains the instance itself is incoherent,
    # which is more important than us checking here if the instance fits nicely with the others.

    if (instance.contenttype == UrlSpace.ContentType.REDIRECT_DESTINATION) & (
        instance.redirect_destination is not None
    ):
        # We gotta pull the redirect group from the database,
        # as there might be some non- up-to-date info on that object or something like that.
        redirect_destination = RedirectDestination.objects.get(
            pk=instance.redirect_destination.pk
        )
        # Try to get a redirecting urlspace which the redirect points to.
        parsed_next_url = urlparse(redirect_destination.next_url)

        # If the redirect url has this website as host:
        if validate_host(parsed_next_url.hostname, settings.ALLOWED_HOSTS):
            # Check if the redirect_destination points to this urlspace:
            if parsed_next_url.path == instance.url:
                raise ValueError(f"cannot make {instance.url} redirect to itself")

            # Check if the redirect_destination points to a different urlspace with a redirect.
            try:
                redirects_to = (
                    UrlSpace.objects.filter(
                        contenttype=UrlSpace.ContentType.REDIRECT_DESTINATION
                    )
                    .filter(redirect_destination__isnull=False)
                    .get(url=parsed_next_url.path)
                )
            # If it doesn't exist, we're good on that front.
            except UrlSpace.DoesNotExist:
                pass
            # If it does:
            else:
                # `instance.url` will redirect to `redirects_to.url`,
                # which will redirect to `redirects_to.redirect_destination.next_url`.
                # Instead, we prefer setting `instance.redirect_destination` to the same value as `redirects_to.redirect_destination`.
                # This prevents redirect chains, and by extension, redirect loops.
                raise (
                    ValueError(
                        f"Cannot make {instance.url} redirect, "
                        f"as it will point to {redirects_to.url}, which will redirect. "
                        f"try setting the `redirect_destination`-field to {redirects_to.redirect_destination.name}"
                    )
                )

        # At this point, we'll have thrown an error if this urlspace redirects to another urlspace
        # which redirects.
        # However, this urlspace might be the second redirect in the chain,
        # which also is a case we have to check for.

        # Try to get a redirectdestination which points to this urlspace.
        try:
            redirects_here = RedirectDestination.objects.get(
                next_url__endswith=instance.url
            )
        # When it's not there, we're golden.
        except RedirectDestination.DoesNotExist:
            pass
        # When it is there:
        else:
            # See if the redirect really redirects to this page,
            # i.e. the path is actually the same and the hostname is for this server.
            parsed_next_url = urlparse(redirects_here.next_url)
            if parsed_next_url.path != instance.url or not validate_host(
                parsed_next_url.hostname, settings.ALLOWED_HOSTS
            ):
                return
            # See if there are urlspaces with it as `redirect_destination` field.
            previous_urls = UrlSpace.objects.filter(
                redirect_destination=redirects_here
            ).filter(contenttype=UrlSpace.ContentType.REDIRECT_DESTINATION)

            if not previous_urls.exists():
                return
            examples = previous_urls.values_list("url", flat=True)
            # When there is one, throw an error.
            raise (
                ValueError(
                    "Cannot make this url redirect, "
                    f"as these urls redirect to {instance.url}: \n"
                    f"{[*examples]} \n"
                    f"try making those urlspaces point to {instance.redirect_destination.next_url} instead"
                )
            )


@receiver(
    signals.pre_save,
    sender=RedirectDestination,
    dispatch_uid="no_double_redirect_redirect-group",
)
def error_on_double_redirect_redirectdestination_save_attempt(
    sender, instance, raw, using, update_fields, **kwargs
):
    """
    In this signal callback, we check if the redirectdestination `instance` is like
    redirectdestination 1 in the diagram.
    """

    if raw:
        return

    # When update_fields is specified,
    # it won't necessarily end up exactly like instance currently is.
    # So, we have to calculate what the new instance would look like.
    if update_fields:
        try:
            updated_instance = RedirectDestination.objects.get(instance.pk)
        except RedirectDestination.DoesNotExist:
            pass
        else:
            for field in update_fields:
                setattr(updated_instance, field, getattr(instance, field))
            instance = updated_instance

    # If instance.pk is None, the instance likely hasn't been saved ever before,
    # and so will not have an urlspace associated with it.
    # NOTE: The check of if instance pk is set is necessary, as since django 4.0,
    #       the `urlspace_set` attribute errors if `instance.pk` is not set.
    if instance.pk is None:
        return

    # Then check if a redirecting urlspace points to this instance:
    if not instance.urlspace_set.filter(
        contenttype=UrlSpace.ContentType.REDIRECT_DESTINATION
    ).exists():
        return

    # Next check if the redirect points to a redirecting urlspace.

    parsed_next_url = urlparse(instance.next_url)

    if not validate_host(parsed_next_url.hostname, settings.ALLOWED_HOSTS):
        return

    try:
        redirects_to = UrlSpace.objects.filter(
            contenttype=UrlSpace.ContentType.REDIRECT_DESTINATION
        ).get(url=parsed_next_url.path)
    except UrlSpace.DoesNotExist:
        pass
    else:
        if redirects_to.redirect_destination is not None:
            raise (
                ValueError(
                    f"Saving redirectdestination {instance.next_url} as is, would create a double redirect, "
                    f"as there are urls which use it to redirect to {redirects_to.url}, "
                    f"but that redirects to {redirects_to.redirect_destination.next_url}."
                )
            )
