# Project Unchained

Het is tijd. We gaan de website herschrijven. Gewoon in Python, met Django. 🎉

## Developen
| :exclamation:  Deze README is verouderd, gebruik hem niet!!!! Gebruik daarvoor in de plaats de Wiki in de Git    |
|------------------------------------------------------------------------------------------------------------------|


Om aan de website te werken heb je uiteraard een werkende installatie van Python nodig,
en daarnaast ook [pipenv].

Om alle dependencies te installeren ga je naar de map waar de code staat en draai je:

```sh
pipenv install --dev
npm install --dev
```

Daarna kan je de development server opstarten met:

```sh
pipenv shell
npx webpack
python manage.py runserver
```

Dit project maakt gebruik van een pipeline. Dit betekent dat je commits automatisch worden gecontroleerd op bepaalde (stijl)fouten. Om bepaalde falende tests te voorkomen is het handig om voor je commit de volgende dingen te hebben gerund:

```sh 
black .
isort .
python manage.py test
pytest
```

Als er een test faalt, kan je beter niet committen.

Omdat het best makkelijk is om black en isort te vergeten, is pre-commit aan de dependencies toegevoegd. Deze plugin zal, zodra je commit, automatisch controleren of je black en isort hebt gerund. Dit voorkomt dat de helft van de commits bestaat uit "Nu met black en isort!" Omdat dit automatisch code runt zonder dat je er erg in hebt, heeft dit een opt-in systeem. Om de pre-commit hooks aan te zetten, run je:

```sh
pre-commit install
```

Als je dit gedaan hebt, zal de commit falen als je niet black en isort hebt gerund

[pipenv]: https://pipenv.readthedocs.io/en/latest/

### Typescript

De nieuwe website maakt gebruik van Typescript. Om alles te compilen naar 1 bestand kan je `npx webpack` draaien.

Mocht je dit automatisch willen laten doen dan kan dit ook. `npx webpack --watch`

Daarnaast wordt ESLint gebruikt om standaard fouten te fixen: `npm run lint`