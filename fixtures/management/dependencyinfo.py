custom_apps = [
    "achievements",
    "activities",
    "authentication",
    "career",
    "docuweb",
    "groups",
    "ideal",
    "members",
    "wrinklypages",
]
imported_apps = ["guardian", "sitetree"]
django_apps = ["auth", "sites", "flatpages"]

# IMPORTANT: this dict serves a double role, and has multiple important properties
# it is used to get the dependencies of a fixture, so it is used as a dict
# it is also used as a list of its keys, in order to iterate over all fixtures
# It is importent to keep the dictionary ordered in such a way,
# that all of the dependencies of each fixture are listed ABOVE itself.
# this ensures fixtures are loaded in the correct order.
fixture_dependencies = {
    "auth": (),
    "docuweb": (),
    "members": (),
    "sites": (),
    "wrinklypages": (),
    "achievements": ("members",),
    "authentication": (
        "members",
        "auth",
    ),
    "career": ("members",),
    "flatpages": ("sites",),
    "guardian": ("auth",),
    "ideal": ("members",),
    "sitetree": ("auth",),
    "groups": (
        "members",
        "authentication",
    ),
    "activities": (
        "ideal",
        "groups",
    ),
}
