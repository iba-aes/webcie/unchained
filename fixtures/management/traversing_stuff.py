from django.core.management.base import CommandError

from .dependencyinfo import fixture_dependencies


def get_non_excluded_fixture_list(exclude: list[str]) -> list[str]:
    fixture_list = []
    # note: the keys of fixture_dependencies are ordered lowest-first;
    # the first elements are the ones without dependencies,
    # then the ones which only depend on those before it, etc.
    for fixture in fixture_dependencies:
        if fixture in exclude:
            # this fixture will be excluded, continue to the next
            continue
        dependencies = fixture_dependencies[fixture]
        # because of the ordering of fixture_dependencies, all dependencies to fixture
        # will have been looped over already.
        # this means that if one of them is not in the list,
        # one of its dependencies was excluded, and hence,
        # we should exclude this dependency too.
        if any(dependency not in fixture_list for dependency in dependencies):
            continue
        fixture_list.append(fixture)
    return fixture_list


def create_dependency_list(fixtures: list[str]) -> list[str]:
    """returns a list of all dependencies of fixtures, dependency-first"""
    return depth_first_reverse_add(fixtures, visited=set(), result=[])


def depth_first_reverse_add(
    fixtures: list[str],
    visited: set[str],
    result: list[str],
) -> list[str]:
    """appends all dependencies of fixtures not in result to result,
    then appends those dependencies"""
    for fixture in fixtures:
        if fixture in result:
            continue
        if fixture in visited:
            raise CommandError("circular dependency detected")
        visited.add(fixture)
        result = depth_first_reverse_add(
            fixture_dependencies[fixture], visited=visited, result=result
        )
        result.append(fixture)
    return result
