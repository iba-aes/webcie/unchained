from os import path, remove
from time import sleep

from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = "yeets the database (NON-RECOVERABLE W/O BACKUPS)"

    def add_arguments(self, parser):
        parser.add_argument(
            "--new",
            "-n",
            action="store_true",
        )

    def handle(self, *args, **options):
        if input("are you sure you want to yeet the database? y/N: ") == "y":
            try:
                self.stdout.write("database will be yeeted in t = -5")
                for i in range(5):
                    sleep(1)
                    self.stdout.write(f"t = -{4-i}")
            except KeyboardInterrupt:
                self.stdout.write("yeeting of database has been aborted")
            else:
                if path.exists("db.sqlite3"):
                    remove("db.sqlite3")
                    self.stdout.write("database has been yeeted like a cat")
                else:
                    self.stdout.write(
                        "there was an attempt to yeet the database, but no database was found."
                    )
                    self.stdout.write(
                        "please check that the db.sqlite3 file exists in the current directory"
                    )
                if options["new"]:
                    call_command("migrate")

        else:
            self.stdout.write("yeeting of database has been canceled")
