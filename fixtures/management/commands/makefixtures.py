import os

from django.core.checks import Tags
from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError

from ..dependencyinfo import (
    custom_apps,
    django_apps,
    fixture_dependencies,
    imported_apps,
)
from ..traversing_stuff import create_dependency_list, get_non_excluded_fixture_list


class Command(BaseCommand):
    help = "makes per-app fixtures and puts them in the right place"
    requires_migrations_checks = True
    requires_system_checks = [Tags.database]

    def add_arguments(self, parser):
        parser.add_argument(
            "-p",
            "--preset",
            choices=[
                "bare",
            ],
            help="this option makes a different set of fixtures load. options are 'bare'",
        )
        parser.add_argument(
            "-i",
            "--include",
            action="extend",
            nargs="+",
            help="this option makes the complete fixtures specified load, with their dependent fixtures",
        )
        parser.add_argument(
            "-e",
            "--exclude",
            action="extend",
            nargs="+",
            help="this option specifies which fixtures to prevent from loading at any cost,"
            " and with them their dependent fixtures",
        )

    def handle(self, *args, **options):
        # load the set options, and check if they are valid
        preset_path = ""
        preset = options["preset"]
        if preset is not None:
            preset_path = f"{preset}/"

        include = options["include"]
        exclude = options["exclude"]

        if include is None:
            include = []

        if exclude is None:
            exclude = []

        for fixture in include:
            if not fixture in fixture_dependencies:
                raise CommandError(f"{fixture} is not a known fixture")

        for fixture in exclude:
            if not fixture in fixture_dependencies:
                raise CommandError(f"{fixture} is not a known fixture")

        # create a list of fixtures to be saved to the preset

        default_save_list = get_non_excluded_fixture_list(exclude)

        # check the included fixtures don't depend on excluded dependencies
        for fixture in include:
            if fixture not in default_save_list:
                raise CommandError(
                    f"{fixture} cannot be included while one of it's dependencies is excluded"
                )

        # make the list for fixtures to be saved to special
        special_save_list = create_dependency_list(include)

        for fixture in special_save_list:
            default_save_list.remove(fixture)

        def savefixture(fixture, prefix_path):
            # this function exists to abstract away finding in what folder the fixture should be saved and saving it there
            if fixture in custom_apps:
                path = f"{fixture}/fixtures/{prefix_path}{fixture}.json"
            elif fixture in imported_apps:
                path = f"fixtures/fixtures/imported/{prefix_path}{fixture}.json"
            elif fixture in django_apps:
                path = f"fixtures/fixtures/django/{prefix_path}{fixture}.json"
            else:
                raise CommandError(f"{fixture} is not a known fixture")

            # we want our fixtures to be:
            # - readable, and
            # - not primary-key dependent.
            default_params = [
                "--all",
                "--indent=4",
                "--natural-foreign",
                "--natural-primary",
            ]

            with open(path, "w", encoding="utf-8") as f:
                match fixture:
                    case "sitetree":
                        call_command("sitetreedump", "--indent=4", stdout=f)
                    case "auth":
                        call_command(
                            "dumpdata",
                            fixture,
                            *("--exclude", "auth.Permission"),
                            # The permission table gets automatically filled when migrating, so we don't need to save that in the fixtures
                            *default_params,
                            stdout=f,
                        )
                    case "authentication":
                        # not --all, cus we don't wanna add the anonymous user. that happens automatically
                        call_command(
                            "dumpdata",
                            fixture,
                            "--indent=4",
                            "--natural-foreign",
                            stdout=f,
                        )

                    case _:
                        call_command(
                            "dumpdata",
                            fixture,
                            *default_params,
                            stdout=f,
                        )

                self.stdout.write(f"{fixture} data has been dumped to {path}")

        for fixture in default_save_list:
            savefixture(fixture, preset_path)

        for fixture in special_save_list:
            savefixture(fixture, "")
