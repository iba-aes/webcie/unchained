"""
loadfixtures
    [--preset, -p {bare}] 
    [--include, -i fixture, [fixture ...]] 
    [--exclude, -e fixture, [fixture ...]]

basically: this command will loads as many fixtures with as many database objects as possible.*
*- unless specified otherwise.

it decides this with the following rulez:

when you add a fixture to the include option, it will guarantee that
it and all of its dependencies will be loaded, with as many database objects as possible.

when you add a fixture to the exclude option, it will not load that
it and any other fixture which depends on that one.

for both of these options goes: you can add any number of fixtures to these options.

any fixture for which the above rules don't tell wether or not to load it,
will be loaded at the level specified by preset. by default this is the same level as
things at the `include` level, however the `bare` preset allows you to only import a
small amount of members, as well as a single (admin) user, 
plus a bunch of default permission objects.

an important aspect to keep in mind is to remember the following:
DONT EXCLUDE FIXTURES WHICH AN INCLUDED FIXTURE REQUIRES.
if you do this, the program will scream at you.
(and maybe i will too)
- Incoherent Instances (aka Blizzard_inc, aka Edward)

here are some example uses: 
`py manage.py loadfixtures` will load all fixtures to the max

`py manage.py loadfixtures --preset bare` will load a small amount of members and a single admin user,
as well as all default permission objects. that way you have a lean database from which you can
test whatever you want.

`py manage.py loadfixtures --excude achievements docuweb` will load every fixture, except those
for the achievements app and for the docuweb app.

`py manage.py loadfixtures --preset bare --include flatpages` will load the same as the previous
"bare" example, in addition to the complete "flatpages" fixture, and the "sites" fixture,
as the "flatpages" app depends on the "sites" app.


"""
from typing import Optional

from django.core.checks import Tags
from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError

from ..dependencyinfo import fixture_dependencies
from ..traversing_stuff import create_dependency_list, get_non_excluded_fixture_list


class Command(BaseCommand):
    help = "loads per-app fixtures created by makefixtures"
    requires_migrations_checks = True
    requires_system_checks = [Tags.database]

    def add_arguments(self, parser):
        parser.add_argument(
            "--onetransaction",
            action="store_true",
            help="make all fixtures load in a single transaction",
        )
        parser.add_argument(
            "-p",
            "--preset",
            choices=[
                "bare",
            ],
            help="this option makes a different set of fixtures load. options are 'bare'",
        )
        parser.add_argument(
            "-i",
            "--include",
            action="extend",
            nargs="+",
            help="this option makes the complete fixtures specified load, with their dependent fixtures",
        )
        parser.add_argument(
            "-e",
            "--exclude",
            action="extend",
            nargs="+",
            help="this option specifies which fixtures to prevent from loading at any cost,"
            " and with them their dependent fixtures",
        )

    def handle(self, *args, **options):
        preset_path = ""
        preset = options["preset"]
        if preset is not None:
            preset_path = f"{preset}/"
        exclude = options["exclude"]
        if exclude is None:
            exclude = []
        include = options["include"]
        if include is None:
            include = []

        for fixture in exclude:
            if fixture not in fixture_dependencies:
                raise CommandError(f"{fixture} is not a known fixture")

        for fixture in include:
            if fixture not in fixture_dependencies:
                raise CommandError(f"{fixture} is not a known fixture")

        self.stdout.write("calculating non-excluded fixtures...")

        fixture_load_stack: list[str] = get_non_excluded_fixture_list(exclude=exclude)
        for fixture in include:
            if fixture not in fixture_load_stack:
                raise CommandError(
                    f"{fixture} is in --include, but one of it's dependencies is in --exclude"
                )
        fixture_load_stack = [preset_path + fixture for fixture in fixture_load_stack]

        self.stdout.write("non-excluded fixtures calculated.")

        self.stdout.write("calculating included fixtures...")

        fixture_load_stack += create_dependency_list(include)

        self.stdout.write("included fixtures calculated.")

        self.stdout.write("loading fixtues...")

        if options["onetransaction"]:
            call_command("loaddata", *fixture_load_stack)
        else:
            for path in fixture_load_stack:
                call_command("loaddata", path)
                self.stdout.write(f"{path} fixture has been loaded")

        self.stdout.write("finished!")
