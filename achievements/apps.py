from django.apps import AppConfig


class AchievementsConfig(AppConfig):
    name = "achievements"

    def ready(self):
        """
        this runs after the app is instanciated, for instance when starting the server.
        Important: this is the only place in this file we can assume the models to be loaded.
        """
        from .rewarding.reward_butler import setup_automation_dicts

        setup_automation_dicts()  # puts the automation dictionaries on the heap

        from . import signals  # registers the signals

        # registering signals can only be done when the models are loaded.
        # signals should only be registered once, which is why it is done here
        # (as soon as the app is loaded).
