import logging

from django.db import models

from members.models.person import Person

from .medals import Medal

logger = logging.getLogger(__name__)


class Received(models.Model):
    """
    A model for the connections between Persons and Medals.
    Relates to :model:`achievements.Medal` and :model:`members.Person`
    """

    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        help_text="de persoon die een medaille verdiend heeft.",
    )
    medal = models.ForeignKey(
        Medal,
        on_delete=models.CASCADE,
        help_text="de medaille die verdiend is",
    )  # thing that is received
    received_on = models.DateField(
        auto_now=True, help_text="de datum waarop de medaille verdiend is"
    )
    AIR = 0
    BRONZE = 1
    SILVER = 2
    GOLD = 3
    PLATINUM = 4

    METALS = [
        (AIR, "Air"),
        (BRONZE, "Bronze"),
        (SILVER, "Silver"),
        (GOLD, "Gold"),
        (PLATINUM, "Platinum"),
    ]
    rank = models.IntegerField(
        choices=METALS, default=AIR, help_text="de soort van de medaille."
    )
    count = models.IntegerField(
        default=0, help_text="aantal van datgene waarvoor de medaille is"
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["person", "medal"], name="one_medal_pp")
        ]
        ordering = ["medal", "person"]

    # makes sure each person-medal combination has at most one occurence in this model

    def __str__(self):
        """returns a text representation of this instance"""
        return str(self.medal.abbrev + "-" + self.person.full_name)
