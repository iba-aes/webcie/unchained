import logging

from django.db import models
from django.urls import reverse  # Used to generate URLs by reversing the URL pattern

from members.models.person import Person

from .extrafield import LowerCharField

logger = logging.getLogger(__name__)


# Create your models here.
class Medal(models.Model):
    """
    A model for the bsgp (Bronze, Silver, Gold, Platinum) achievements, aka Medals.
    Related to :model:`members.Person` through :model:`achievements.Received`
    """

    # NOTE: how to add a new medal type:
    # 1. think of a short typename
    # 2. All-caps it to create a variable, and assign it the typename
    # 3. Add a 4-tuple with the four multipliers in TYPEMULTYPLIERS with the typename as key
    # 4. Append a 2-tuple consisting of the typename, as wel as a verbose description of the type to TYPES

    name = models.CharField(max_length=200, help_text="geef de achievement een naam")
    abbrev = LowerCharField(
        max_length=20,
        help_text="een unieke!! voor user leesbare afkorting, die wordt gebruikt in urls",
        unique=True,
    )
    description = models.CharField(
        max_length=200, help_text="een omschrijving van de achievement", default=""
    )

    # Medal types are how we specify the difficulty curve for getting levels in the medal.
    # for BSGP1234 the progression is linear, while for BSGP125A it gets steeper.
    BSGP1234 = "bsgp1234"
    BSGP125A = "bsgp125A"

    TYPEMULTIPLIERS = {
        BSGP1234: (1, 2, 3, 4),
        BSGP125A: (1, 2, 5, 10),
    }

    TYPES = (
        (BSGP1234, "BronzeSilverGoldPlatinum, short multiplier(1,2,3,4)"),
        (BSGP125A, "BronzeSilverGoldPlatinum, long multiplier(1,2,5,10)"),
    )

    LITERAL_TYPES = tuple(TYPEMULTIPLIERS)  # shorthand to create a tuple of the keys
    # simplifies adding another medal type

    type = models.CharField(
        max_length=8,
        choices=TYPES,
        default=BSGP1234,
        help_text="het type medal. bepaalt hoe de verschillende ranks tot elkaar verhouden",
    )
    value = models.IntegerField(
        help_text="de basisfactor voor de medal. bepaalt de requirement voor brons.",
        null=True,
        blank=True,
    )
    continuous_checks = models.BooleanField(
        default=False,
        help_text="bepaalt of de medal automatisch geupdated wordt. Zet deze op False als je de medal deactiveert.",
    )

    receivers = models.ManyToManyField(
        Person,
        through="Received",
        help_text="de mensen die een rank hebben in deze medal.",
    )
    # NOTE: This links directly to the related Persons, not to the related Receiveds.
    # NOTE: For related Receiveds, use .received or __received, depending on context.

    @property
    def absolute_url(self):
        """Returns the url to access a detail record for this achievement."""
        return reverse("medal-detail", args=[str(self.abbrev)])

    def __str__(self):
        """Returns a text representation of the instance"""
        return self.name

    @property
    def type_multipliers(self):
        """returns a tuple of the multipliers for (bronze,silver,gold,platinum) respectively,
        for this specific instance"""
        return self.TYPEMULTIPLIERS[self.type]

    @property
    def bronze_requirement(self):
        """returns the required count for getting bronze in this medal"""
        return self.type_multipliers[0] * self.value

    @property
    def silver_requirement(self):
        """returns the required count for getting silver in this medal"""
        return self.type_multipliers[1] * self.value

    @property
    def gold_requirement(self):
        """returns the required count for getting gold in this medal"""
        return self.type_multipliers[2] * self.value

    @property
    def platinum_requirement(self):
        """returns the required count for getting platinum in this medal"""
        return self.type_multipliers[3] * self.value

    def calculate_rank(self, count):
        """returns the rank applicable for this medal given the count"""
        if self.type not in self.LITERAL_TYPES:
            logger.error(
                f"there has been an attempt to award the medal {self.name} "
                f"which has type {self.type}. This type hasn't been implemented yet. "
                "for now, 0 has been returned as a temp fix. Have fun implementing the new type!\n "
                "with love 😘, Edward"
            )
            return 0
        relativecount = count // self.value
        multipliers = self.type_multipliers
        if relativecount < multipliers[0]:
            rank = 0  # Received().AIR
        elif relativecount < multipliers[1]:
            rank = 1  # Received().BRONZE
        elif relativecount < multipliers[2]:
            rank = 2  # Received().SILVER
        elif relativecount < multipliers[3]:
            rank = 3  # Received().GOLD
        else:
            rank = 4  # Received().PLATINUM
        return rank

    class Meta:
        ordering = ["name", "abbrev"]
