from django.db import models


# NOTE: new field which only saves lowercase, used for keeping url clean.
class LowerCharField(models.SlugField):
    def __init__(self, *args, **kwargs):
        super(LowerCharField, self).__init__(*args, **kwargs)

    def get_prep_value(self, value):
        return str(value).lower()
