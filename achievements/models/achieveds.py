import logging

from django.db import models

from members.models import Person

from .achievements import Achievement

logger = logging.getLogger(__name__)


class Achieved(models.Model):
    """A model for the manytomany-relation between
    :model:`members.Person` and :model:`achievements.Achievement`.
    It records when the related achievement was achieved by the related person."""

    person = models.ForeignKey(
        Person, on_delete=models.CASCADE, help_text="the person that has achieved"
    )
    achievement = models.ForeignKey(
        Achievement,
        on_delete=models.CASCADE,
        help_text="the achievement that has been achieved",
    )
    has_achieved = models.BooleanField(
        default=False,
        help_text="bool indicating wether or not this person has achieved this achievement",
    )
    achieved_on = models.DateField(
        auto_now=True,
        help_text="date the associated achievement was "
        "achieved by the associated person",
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["person", "achievement"], name="unique_achievement"
            )
        ]
        ordering = ["achievement", "person"]

    # NOTE: makes sure each person-achievement combination only has at most one occurence in this model

    def __str__(self):
        """Returns a description of the instance."""
        return str(self.person.id) + " - " + self.achievement.abbrev
