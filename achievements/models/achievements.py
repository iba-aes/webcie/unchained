import logging

from django.db import models
from django.urls import reverse  # Used to generate URLs by reversing the URL patterns

from members.models.person import Person

from .extrafield import LowerCharField

logger = logging.getLogger(__name__)


# Create your models here.
class Achievement(models.Model):
    """
    A model for the achievements.
    connects to :model:`members.Person` through :model:`achievements.Achieved`.
    """

    name = models.CharField(max_length=200, help_text="de naam van de achievement")
    abbrev = LowerCharField(
        max_length=20,
        help_text="een unieke!! voor user leesbare afkorting, die wordt gebruikt in urls",
        unique=True,
    )
    description = models.CharField(
        max_length=200,
        help_text="een omschrijving van de achievement",
        default="",
    )
    hidden = models.BooleanField(
        default=True,
        help_text="zegt of de achievement verborgen is voor mensen die hem niet hebben",
    )
    continuous_checks = models.BooleanField(
        default=False,
        help_text="zegt of automatisch en herhaaldelijk "
        "gecontroleerd wordt of mensen deze achievement moeten krijgen",
    )

    achievers = models.ManyToManyField(
        Person,
        through="Achieved",
        help_text="de mensen die deze achievement hebben gehaald",
    )

    @property
    def absolute_url(self):
        """Returns the url to access a detail record for this achievement."""
        return reverse("achievement-detail", args=[str(self.abbrev)])

    def __str__(self):
        """returns a string to represent the model"""
        return self.name

    class Meta:
        permissions = (
            ("can_see_all_achievements", "See all achievements"),
            ("reward_achievement", "Reward any achievement"),
        )
        ordering = ["name", "abbrev"]
