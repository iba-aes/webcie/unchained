from django.db.models import Count, Q
from django.test import TestCase

from authentication.models import User
from members.models import Person

from ..models import Achievement, Medal, Received
from ..rewarding.achievement_utilities import person_has_achievement
from .testutils import create_users, new_testachievement, new_testmedal


# Test series 5: web interfaces.
class ViewsTestCase(TestCase):
    def setUp(self):
        self.achievement1 = new_testachievement("one")
        self.achievement2 = new_testachievement("two", hidden=True)
        self.achievement_connections = new_testachievement("connections")
        self.medal1 = new_testmedal("countone", "bsgp1234", 1)
        self.medal2 = new_testmedal("counttwo", "bsgp125A", 1)

        self.myPerson, self.otherPerson = create_users()

        self.user = User.objects.create_user("username", self.myPerson.pk, "password")

        self.client.force_login(self.user)

    def TearDown(self):
        self.achievement1.delete()
        self.achievement2.delete()
        self.achievement_connections.delete()
        self.medal1.delete()
        self.medal2.delete()

        self.user.delete()
        self.myPerson.delete()
        self.otherPerson.delete()

    def testViews(self):
        response = self.client.get("/achievements/")
        # testing the webpage was fetched correctly
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "achievements/achievement_list.html")
        # setting up testing the content of the webpage
        achievement_list = response.context["achievement_list"]
        medal_list = response.context["medal_list"]
        # check the numbers are correct.
        for achievement in achievement_list:
            achievement_from_pk = Achievement.objects.filter(abbrev=achievement.abbrev)
            self.assertEqual(achievement_from_pk.get(), achievement)
            num_achiever = Count("achieved", filter=Q(achieved__has_achieved=True))
            annotated_from_pk = achievement_from_pk.annotate(num_achiever=num_achiever)
            self.assertEqual(
                annotated_from_pk.get().num_achiever, achievement.num_achiever
            )

        # check all medals are intact and not mixed up somehow
        for medal in medal_list:
            medal_from_pk = Medal.objects.get(abbrev=medal.abbrev)
            self.assertEqual(medal_from_pk, medal)
            self.assertEqual(
                Received.objects.filter(medal=medal_from_pk).filter(rank=1).count(),
                medal.num_bronze,
            )
            self.assertEqual(
                Received.objects.filter(medal=medal_from_pk).filter(rank=2).count(),
                medal.num_silver,
            )
            self.assertEqual(
                Received.objects.filter(medal=medal_from_pk).filter(rank=3).count(),
                medal.num_gold,
            )
            self.assertEqual(
                Received.objects.filter(medal=medal_from_pk).filter(rank=4).count(),
                medal.num_platinum,
            )
        # check achievements are only visible if they are not hidden or achieved by the visitor
        for achievement in Achievement.objects.all():
            #            if achievement.is_visible_to(self.myPerson):
            if not achievement.hidden:
                self.assertTrue(achievement in achievement_list)
            elif person_has_achievement(self.myPerson, achievement):
                self.assertTrue(achievement in achievement_list)
            elif self.user.has_perm("can_see_all"):
                self.assertTrue(achievement in achievement_list)
            else:
                self.assertFalse(achievement in achievement_list)
        # check all medals are shown
        self.assertEqual(list(medal_list), list(Medal.objects.all()))

        response = self.client.get(
            f"/achievements/achievement/{self.achievement1.abbrev}"
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "achievements/achievement_detail.html")

        response = self.client.get(f"/leden/{self.myPerson.pk}/achievements")
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.headers["Location"], f"/achievements/leden/{self.myPerson.pk}"
        )
        response = self.client.get(f"/achievements/leden/{self.myPerson.pk}")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "achievements/user_achievement_list.html")

        response = self.client.get(f"/achievements/medaille/{self.medal1.abbrev}")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "achievements/medal_detail.html")
