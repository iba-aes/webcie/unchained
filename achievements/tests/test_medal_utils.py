from django.test import TestCase

from ..rewarding import medal_utilities as mu
from . import testutils as tu


class MedalUtilitiesTestCase(TestCase):
    def setUp(self):
        mu.reset_counter_dict(__name__)
        self.medal = tu.new_testmedal(
            "testmedal1", "bsgp1234", 1, continous_checks=True
        )

    def test_Get_All_Counters(self):
        self.assertEqual(list(mu.get_all_counters()), [""])
        self.assertIs(mu.get_all_counters()[""], mu.deactivated_counter)

    def test_Get_Counter(self):
        emptymedal = tu.new_testmedal("", "bsgp1234", 1)
        fakemedal = tu.new_testmedal("thisisnotarealmedal", "bsgp1234", 1, save=False)
        self.assertIs(mu.get_counter(emptymedal), mu.deactivated_counter)
        self.assertIs(mu.get_counter(fakemedal), mu.deactivated_counter)
        emptymedal.delete()

    def test_add_counter(self):
        foo = lambda x: 0
        mu.add_counter("testmedal1")(foo)
        self.assertIs(foo, mu.get_counter(self.medal))

    def tearDown(self):
        mu.reset_counter_dict(__name__)
        self.medal.delete()


class RewardingTestCase(TestCase):
    def setUp(self):
        mu.reset_counter_dict(__name__)
        self.medal = tu.new_testmedal(
            "testmedal1", "bsgp1234", 1, continous_checks=True
        )
        self.person1, self.person2 = tu.create_users()

    def test_Get_Incrementor_1(self):
        self.assertEqual(tu.person_medal_count(self.person1, self.medal), 0)
        testmedal_incrementor = mu.get_incrementor(self.medal.abbrev)
        for i in range(4):
            with self.subTest(msg="incrementor failed at", iteration=i):
                testmedal_incrementor(self.person1)
                self.assertEqual(tu.person_medal_count(self.person1, self.medal), i + 1)

    def test_Get_Incrementor_2(self):
        self.assertEqual(tu.person_medal_count(self.person1, self.medal), 0)
        testmedal_incrementor = mu.get_incrementor(self.medal.abbrev)
        for i in range(4):
            with self.subTest(msg="incrementing by 2 failed at ", iteration=i):
                testmedal_incrementor(self.person1, amount=2)
                self.assertEqual(
                    tu.person_medal_count(self.person1, self.medal), 2 * (i + 1)
                )

    def test_Get_Medal_Rewarder(self):
        self.assertEqual(tu.person_medal_count(self.person1, self.medal), 0)
        reward_medal = mu.get_medal_rewarder(medal=self.medal)
        for i in range(5):
            with self.subTest(msg="rewarding medal by increasing count", count=i):
                reward_medal(self.person1, i)
                self.assertEqual(tu.person_medal_count(self.person1, self.medal), i)
            for j in range(i):
                with self.subTest(
                    msg="rewarding medal to check it uses the max count",
                    current_count=i,
                    attempted_count=j,
                ):
                    reward_medal(self.person1, j)
                    self.assertEqual(tu.person_medal_count(self.person1, self.medal), i)

    def test_Get_Count_Setter_Soft(self):
        self.assertEqual(tu.person_medal_count(self.person1, self.medal), 0)
        set_count = mu.get_count_setter(medal=self.medal)
        for i in range(5):
            with self.subTest(msg="setting medal count increasing order", count=i):
                set_count(self.person1, i)
                self.assertEqual(tu.person_medal_count(self.person1, self.medal), i)
                self.assertEqual(
                    tu.person_medal_rank(self.person1, self.medal),
                    self.medal.calculate_rank(i),
                )
        for i in reversed(range(5)):
            with self.subTest(msg="setting medal count decreasing order", count=i):
                set_count(self.person1, i)
                self.assertEqual(tu.person_medal_count(self.person1, self.medal), i)
                self.assertEqual(
                    tu.person_medal_rank(self.person1, self.medal),
                    self.medal.calculate_rank(4),
                )
        for i in [0, 3, 4, 2, 2, 4, 1, 4, 0, 3]:
            with self.subTest(
                msg="setting medal count (consistent) arbitrary order", count=i
            ):
                set_count(self.person1, i)
                self.assertEqual(tu.person_medal_count(self.person1, self.medal), i)
                self.assertEqual(
                    tu.person_medal_rank(self.person1, self.medal),
                    self.medal.calculate_rank(4),
                )

    def test_Get_Count_Setter_Hard(self):
        self.assertEqual(tu.person_medal_count(self.person1, self.medal), 0)
        set_count = mu.get_count_setter(medal=self.medal, allow_reduce=True)
        for i in range(5):
            with self.subTest(
                msg="force setting medal count increasing order", count=i
            ):
                set_count(self.person1, i)
                self.assertEqual(tu.person_medal_count(self.person1, self.medal), i)
                self.assertEqual(
                    tu.person_medal_rank(self.person1, self.medal),
                    self.medal.calculate_rank(i),
                )
        for i in reversed(range(5)):
            with self.subTest(
                msg="force setting medal count decreasing order", count=i
            ):
                set_count(self.person1, i)
                self.assertEqual(tu.person_medal_count(self.person1, self.medal), i)
                self.assertEqual(
                    tu.person_medal_rank(self.person1, self.medal),
                    self.medal.calculate_rank(i),
                )
        for i in [0, 3, 4, 2, 2, 4, 1, 4, 0, 3]:
            with self.subTest(
                msg="force setting medal count (consistent) arbitrary order", count=i
            ):
                set_count(self.person1, i)
                self.assertEqual(tu.person_medal_count(self.person1, self.medal), i)
                self.assertEqual(
                    tu.person_medal_rank(self.person1, self.medal),
                    self.medal.calculate_rank(i),
                )

    def tearDown(self):
        mu.reset_counter_dict(__name__)

        self.medal.delete()
        self.person1.delete()
        self.person2.delete()
