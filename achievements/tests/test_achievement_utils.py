from django.test import TestCase

from groups.models import Group
from members.models import Person, person

from ..models import Achievement
from ..rewarding import achievement_utilities as au
from . import testutils as tu


class AchievementUtilitiesTestCase(TestCase):
    def setUp(self):
        au.reset_qualifier_dict(__name__)

        self.achievement = tu.new_testachievement(
            "testachievement1", continous_checks=True
        )

    def test_Add_Qualifier(self):
        foo = lambda x: False
        au.add_qualifier(self.achievement.abbrev)(foo)
        self.assertIs(foo, au.get_qualifier(self.achievement))
        self.assertIs(foo, au.get_all_qualifiers()[self.achievement.abbrev])

    def test_Get_Qualifier(self):
        emptyachievement = tu.new_testachievement("")
        fakeachievement = tu.new_testachievement("thisisnotanachievement", save=False)

        self.assertIs(au.get_qualifier(emptyachievement), au.deactivated_qualifier)
        self.assertIs(au.get_qualifier(fakeachievement), au.deactivated_qualifier)

        emptyachievement.delete()

    def test_Get_All_Qualifiers(self):
        self.assertEqual(list(au.get_all_qualifiers()), [""])
        self.assertIs(au.get_all_qualifiers()[""], au.deactivated_qualifier)

    def test_Get_Group_Qualifier(self):
        group_qualifier = au.create_group_qualifier(
            group_name="testgroup", achievement_abbrev=self.achievement.abbrev
        )
        self.assertIs(au.get_qualifier(self.achievement), group_qualifier)

    def tearDown(self):
        au.reset_qualifier_dict(__name__)

        self.achievement.delete()


class RewarderTestCase(TestCase):
    def setUp(self):
        au.reset_qualifier_dict(__name__)

        self.achievement = tu.new_testachievement(
            "testachievement1", continous_checks=True
        )
        self.person1, self.person2 = tu.create_users()

    def test_Get_Achievement_Rewarder(self):
        self.assertFalse(au.person_has_achievement(self.person1, self.achievement))
        self.assertFalse(au.person_has_achievement(self.person2, self.achievement))

        reward_testachievement = au.get_achievement_rewarder(
            achievement=self.achievement
        )
        reward_testachievement(self.person1)

        self.assertTrue(au.person_has_achievement(self.person1, self.achievement))
        self.assertFalse(au.person_has_achievement(self.person2, self.achievement))

    def tearDown(self):
        au.reset_qualifier_dict(__name__)

        self.achievement.delete()
        self.person1.delete()
        self.person2.delete()
