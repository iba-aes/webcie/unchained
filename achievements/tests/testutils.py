from groups.models import Group
from members.models import Person

from ..models import Achieved, Achievement, Medal, Received


def new_testachievement(abbrev, **kwargs):
    kwordlist = list(kwargs)
    name = kwargs["name"] if "name" in kwordlist else "can you hear me?"
    description = (
        kwargs["description"]
        if "description" in kwordlist
        else "This is a test. Do not panic."
    )
    hidden = kwargs["hidden"] if "hidden" in kwordlist else False
    continuous_checks = (
        kwargs["continuous_checks"] if "continuous_checks" in kwordlist else False
    )
    save = kwargs["save"] if "save" in kwordlist else True
    newachievement = Achievement(
        name=name,
        abbrev=abbrev,
        description=description,
        continuous_checks=continuous_checks,
        hidden=hidden,
    )
    if save:
        newachievement.save()

    return newachievement


# function to create new testmedal
def new_testmedal(abbrev: str, type: str, value: int, **kwargs) -> Medal:
    kwordlist = list(kwargs)
    name = kwargs["name"] if "name" in kwordlist else "test one two three"
    description = (
        kwargs["description"]
        if "description" in kwordlist
        else "[lightly hits mic with hand]. test test 123 123"
    )
    continuous_checks = (
        kwargs["continuous_checks"] if "continuous_checks" in kwordlist else True
    )
    save = kwargs["save"] if "save" in kwordlist else True
    newmedal = Medal(
        name=name,
        abbrev=abbrev,
        description=description,
        type=type,
        value=value,
        continuous_checks=continuous_checks,
    )
    if save:
        newmedal.save()
    return newmedal


def create_users():
    person1, _ = Person.create_with_contact(
        legal_name="hoi",
        full_name="Mao Zedong",
        given_name="Zedong",
        email="great-leader@example.com",
        birth_date="2000-1-1",
    )

    person2, _ = Person.create_with_contact(
        legal_name="hi", full_name="Maedong", given_name="Zeg", email="grr@example.com"
    )

    person1.save()
    person2.save()

    return person1, person2


def new_test_group(name, save: bool = True):
    new_group = Group(name=name)
    if save:
        new_group.save()
    return new_group


def get_medal_records(person: Person, medal: Medal):
    """returns all records in this medal for this person."""
    received, created = Received.objects.all().get_or_create(
        person=person, medal=medal, defaults={"count": 0, "rank": 0}
    )
    return received
    # returns a person's record for this medal


def person_medal_rank(person, medal):
    return get_medal_records(person, medal).rank


def person_medal_count(person, medal):
    return get_medal_records(person, medal).count
