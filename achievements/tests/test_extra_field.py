from django.test import TestCase

from . import testutils as tu


class LowerFieldTestCase(TestCase):
    def setUp(self):
        self.achievement = tu.new_testachievement("TEST", save=False)

    def testField(self):
        self.achievement.save()
        self.achievement.refresh_from_db()
        self.assertEqual("test", self.achievement.abbrev)

    def tearDown(self):
        self.achievement.delete()
