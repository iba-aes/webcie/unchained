from django.test import TestCase

from groups.models import Group
from members.models import Person

from ..models import Achievement, Medal
from ..rewarding import achievement_utilities as au
from ..rewarding import medal_utilities as mu
from ..rewarding.reward_butler import butler
from . import testutils as tu


class RewardingAchievementTestCase(TestCase):
    def setUp(self):
        au.reset_qualifier_dict(__name__)
        self.person1, _ = tu.create_users()
        self.group = tu.new_test_group("testgroup")
        self.group.members.add(self.person1)
        self.group.save()
        self.achievement = tu.new_testachievement(
            "testachievement",
            continuous_checks=False,
        )

    def test_Achievement_non_active(self):
        # make sure butler doesn't check non-active achievements
        side_effects = {self.achievement.abbrev: False}
        return_value = False

        @au.add_qualifier(self.achievement.abbrev)
        def self_aware_qualifier(person: Person):
            side_effects[self.achievement.abbrev] = True
            return return_value

        self.assertFalse(au.person_has_achievement(self.person1, self.achievement))
        self.assertFalse(side_effects[self.achievement.abbrev])
        butler(self.person1)
        self.assertFalse(side_effects[self.achievement.abbrev])
        self.assertFalse(au.person_has_achievement(self.person1, self.achievement))

    def test_Achievement_active(self):
        # make sure butler does check active achievements
        # also make sure the butler uses the returned value
        side_effects = {self.achievement.abbrev: False}
        return_value = False

        @au.add_qualifier(self.achievement.abbrev)
        def self_aware_qualifier(person: Person):
            side_effects[self.achievement.abbrev] = True
            return return_value

        self.assertFalse(au.person_has_achievement(self.person1, self.achievement))
        self.assertFalse(side_effects[self.achievement.abbrev])

        self.achievement.continuous_checks = True
        self.achievement.save()
        butler(self.person1)

        self.assertTrue(side_effects[self.achievement.abbrev])
        self.assertFalse(au.person_has_achievement(self.person1, self.achievement))

        with self.subTest(
            msg="test the butler uses the returned value from the qualifier"
        ):
            return_value = True
            self.assertFalse(au.person_has_achievement(self.person1, self.achievement))

            butler(self.person1)

            self.assertTrue(au.person_has_achievement(self.person1, self.achievement))

        with self.subTest(msg="test the butler doesn't remove achievements"):
            return_value = False
            butler(self.person1)
            self.assertTrue(au.person_has_achievement(self.person1, self.achievement))

    def tearDown(self):
        au.reset_qualifier_dict(__name__)
        self.achievement.delete()
        self.group.delete()
        self.person1.delete()


class RewardingMedalTestCase(TestCase):
    def setUp(self):
        mu.reset_counter_dict(__name__)
        self.person1, _ = tu.create_users()
        self.medal = tu.new_testmedal("testmedal", Medal.BSGP1234, 1)

    def test_Medal_awarding(self):
        return_value = 0
        mu.add_counter("testmedal")(lambda x: return_value)
        # this adds a counter for the testmedals.
        # lambda cus the name is not important and it shortens the syntax.
        # also the function isn't really complicated.

        for i in range(5):
            return_value = i
            butler(self.person1)
            with self.subTest(
                msg="medal (short type) failed to update properly at", rank=i
            ):
                self.assertEqual(tu.person_medal_count(self.person1, self.medal), i)
                self.assertEqual(
                    tu.person_medal_rank(self.person1, self.medal),
                    self.medal.calculate_rank(i),
                )

        self.medal.type = Medal.BSGP125A
        self.medal.save()

        set_medal_count = mu.get_count_setter(medal=self.medal, allow_reduce=True)
        set_medal_count(self.person1, 0)

        for i in range(11):
            return_value = i
            butler(self.person1)
            with self.subTest(
                msg="medal (long type) failed to update properly at", rank=i
            ):
                self.assertEqual(tu.person_medal_count(self.person1, self.medal), i)
                self.assertEqual(
                    tu.person_medal_rank(self.person1, self.medal),
                    self.medal.calculate_rank(i),
                )

    def tearDown(self):
        mu.reset_counter_dict(__name__)
        self.person1.delete()
        self.medal.delete()
