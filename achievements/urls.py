from django.urls import include, path

from . import views

urlpatterns = [
    path("", views.AchievementListView, name="achievement-listview"),
    path("leden/<int:pk>", views.UserAchievementDetailView, name="userachievements"),
    path(
        "achievement/<str:name>",
        views.AchievementDetailView.as_view(),
        name="achievement-detail",
    ),
    path("medaille/<str:name>", views.MedalDetailView.as_view(), name="medal-detail"),
]
