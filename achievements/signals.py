from django.db.models.signals import post_save
from django.dispatch import receiver

from members.models import Person

from .models import Achieved, Achievement, Medal, Received

# for an explanation of signals, see
# https://docs.djangoproject.com/en/stable/topics/signals/


@receiver(
    post_save, sender=Achievement, weak=False, dispatch_uid="achievement-to-achieved"
)
def achieved_from_achievement(sender, created, instance, **kwargs):
    """
    whenever a new achievement gets created, also create new achieved for every person.
    Except when loading fixtures.
    """
    if created and not kwargs["raw"]:
        for person in Person.objects.all():
            instance.achievers.add(person)


@receiver(post_save, sender=Medal, weak=False, dispatch_uid="medal-to-received")
def received_from_medal(sender, created, instance, **kwargs):
    """
    whenever a new medal gets created, create a new received for every person
    Except when loading fixtures.
    """
    if created == True and not kwargs["raw"] == True:
        for person in Person.objects.all():
            instance.receivers.add(person)


@receiver(post_save, sender=Person, weak=False, dispatch_uid="person-to-received")
def received_and_achieved_from_person(sender, created, instance, **kwargs):
    """
    whenever a new person gets created, create a new received for every medal
        also a new achieved for every achievement.
    """
    if created == True and not kwargs["raw"] == True:
        for medal in Medal.objects.all():
            medal.receivers.add(instance)
        for achievement in Achievement.objects.all():
            achievement.achievers.add(instance)
