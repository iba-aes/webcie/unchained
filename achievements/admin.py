from django.contrib import admin
from guardian.admin import GuardedModelAdmin

from members.models import Person

from .models import Achieved, Achievement, Medal, Received

# Register your models here.


# admin.site.register(Achievement)
# admin.site.register(Achieved)
# admin.site.register(Medal)
# admin.site.register(Received)
@admin.register(Achievement)
class AchievementAdmin(GuardedModelAdmin):
    actions = ["deactivate_automation", "activate_automation", "create_achieveds"]
    list_display = ("abbrev", "hidden", "continuous_checks")
    list_editable = ("hidden", "continuous_checks")
    list_filter = ("hidden", "continuous_checks")
    fieldsets = (
        ("information", {"fields": [("name", "abbrev"), "description"]}),
        (
            "properties",
            {
                "fields": [
                    ("hidden", "continuous_checks"),
                ]
            },
        ),
    )

    @admin.action(description="deactivate automation for these achievements")
    def deactivate_automation(self, request, queryset):
        queryset.update(automated_checks=False)

    @admin.action(description="activate automation for these achievements")
    def activate_automation(self, request, queryset):
        queryset.update(automated_checks=True)

    @admin.action(description="create all achieveds for the selected achievements")
    def create_achieveds(self, request, queryset):
        for achievement in queryset:
            for person in Person.objects.all():
                achievement.achievers.add(person)


@admin.register(Achieved)
class AchievedAdmin(GuardedModelAdmin):
    actions = ["award_achievements", "unaward_achievements"]

    date_hierarchy = "achieved_on"
    fields = ("achievement", "person", "achieved_on", "has_achieved")
    list_display = ("achievement", "person", "achieved_on", "has_achieved")
    list_filter = ("achievement", "person", "achieved_on", "has_achieved")
    list_per_page = 20
    readonly_fields = ("achieved_on",)

    @admin.action(description="setting the has_achieved attribute to `False`")
    def unaward_achievements(self, request, queryset):
        queryset.update(has_achieved=False)

    @admin.action(description="setting the has_achieved attribute to `True`")
    def award_achievements(self, request, queryset):
        queryset.update(has_achieved=True)


@admin.register(Medal)
class MedalAdmin(GuardedModelAdmin):
    actions = ["deactivate_automation", "activate_automation", "create_receiveds"]
    fields = ("name", "abbrev", "description", "type", "value")
    list_display = ("abbrev", "type", "value")
    list_editable = ("type", "value")
    list_filter = ("type",)

    @admin.action(description="deactivate automation for selected medals")
    def deactivate_automation(self, request, queryset):
        queryset.update(continuous_checks=False)

    @admin.action(description="activate automation for selected medals")
    def activate_automation(self, request, queryset):
        queryset.update(continuous_checks=True)

    @admin.action(description="create all receiveds for the selected medals")
    def create_receiveds(self, request, queryset):
        for medal in queryset:
            for person in Person.objects.all():
                medal.receivers.add(person)


@admin.register(Received)
class ReceivedAdmin(GuardedModelAdmin):
    actions = ["recalculate", "soft_recalculate"]
    date_hierarchy = "received_on"
    fields = ("medal", "person", "rank", "received_on")
    list_display = ("medal", "person", "rank", "count", "received_on")
    list_editable = ("rank", "count")
    list_filter = ("medal", "person", "rank", "count", "received_on")
    list_per_page = 20
    readonly_fields = ("received_on",)

    @admin.action(description="recalculate rank")
    def recalculate(self, request, queryset):
        for received in queryset:
            received.rank = received.medal.calculate_rank(received.count)
            received.save()

    @admin.action(description="soft recalculate rank")
    def soft_recalculate(self, request, queryset):
        for received in queryset:
            received.rank = max(
                received.medal.calculate_rank(received.count), received.rank
            )
            received.save()
