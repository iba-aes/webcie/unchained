from django import forms
from django.utils.translation import gettext as _

from .models import Achievement


class DynamicRewardAchievementForm(forms.Form):
    """class for a form for giving people achievements from a dynamic list,
    passed at initialisation"""

    achievements = forms.ModelMultipleChoiceField(
        queryset=Achievement.objects.none(),  # default achievement list is empty
        label=_("select achievements to be rewarded"),
        required=False,
    )

    def __init__(self, *args, queryset=None, **kwargs):
        super().__init__(*args, **kwargs)
        if queryset is None:
            queryset = Achievement.objects.none()
        self.fields["achievements"].queryset = queryset
