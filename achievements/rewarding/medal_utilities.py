import logging
from typing import Callable, Dict, Optional

from django.db.models import F

from members.models.person import Person

from ..models import Medal, Received

logger = logging.getLogger(__name__)

Counter = Callable[[Person], int]

Incrementor = Callable[[Person, int], Received]

# initialising dict with counter functions per medal, will be filled using decorators later on.
medal_counters: Dict[str, Counter] = {}


def deactivated_counter(person: Person) -> int:
    """
    to be used for medals without counter
    DON'T DELETE COUNTERS FOR MEDALS TO DEACTIVATE THEM,
    INSTEAD CHANGE medal.continuouschecks
    """
    return 0


def add_counter(medal_abbrev: str) -> Callable[[Counter], Counter]:
    """
    creates decorator to add counter functions to dictionary.
    medal_abbrev is the (official) (non-editable) abbreviation of the accompanying medal
    """

    def decorate(counter):
        medal_counters[medal_abbrev] = counter
        return counter

    return decorate


def reset_counter_dict(caller_name: str) -> None:
    """
    clears the counter dictionary, and re-adds the default counter.
    DONT USE OUTSIDE OF TESTS UNLESS YOU KNOW WHAT YOU'RE DOING.
    ESPECIALLY IN PRODUCTION CODE
    """
    # NOTE: this function gets called on startup, which is why it *can* be ok to use.

    medal_counters.clear()
    if caller_name != "achievements.rewarding.reward_butler":
        logger.warning(
            "THE COUNTER DICTIONARY HAS BEEN RESET.\n"
            "IF THIS IS IN PRODUCTION, pls do some `grep` + `git blame` magic to find out who's to blame."
            f"this message is brought to you by {caller_name}"
        )
    else:
        logger.info("the counter dictionary has been reset")

    add_counter("")(deactivated_counter)


def get_counter(medal: Medal) -> Counter:
    """accesses the counter dictionary to find the counter associated with this medal"""
    if medal.abbrev in medal_counters:
        return medal_counters[medal.abbrev]
    else:
        logger.warning(
            f"no counting function for {medal.abbrev} exists. 0 will be returned"
        )
        return medal_counters[""]


def get_all_counters() -> Dict[str, Counter]:
    """returns dict of all counters, keyed to the abbreviation of their respective medals"""
    return medal_counters


def get_incrementor(medal_abbrev: str) -> Callable[[Person, int], Received]:
    """
    generates a function which adds an amount to the count of the person passed to that function,
    for this medal.
    """

    def medal_incrementor(person: Person, amount: int = 1) -> Received:
        """adds an amount (default 1) to the count of this person, for the medal passed to the generator"""
        medal = Medal.objects.get(abbrev=medal_abbrev)
        received, created = Received.objects.get_or_create(medal=medal, person=person)

        received.rank = medal.calculate_rank(received.count + amount)
        received.count = F("count") + amount
        # this prevents race conditions: see django Query expression docs
        # it doesnt update received.count, however.

        received.save()
        received.refresh_from_db()
        # The increment also persists, but it is removed when we reload

        return received

    return medal_incrementor


def get_medal_rewarder(
    *, medal_abbrev: Optional[str] = None, medal: Optional[Medal] = None
) -> Incrementor:
    """
    returns a function for this medal which updates the count
    and rank for the person passed in its argument.
    use EXACTLY one of the medal and medal_abbrev kwargs
    """
    if medal_abbrev is not None and medal is not None:
        raise ValueError(
            "get_medal_rewarder requires at most one argument to be specified, but both have been specified"
        )
    if medal is None:
        if medal_abbrev is None:
            raise ValueError(
                "get_medal_rewarder requires at least one argument to be specified, but none have been specified"
            )
        medal = Medal.objects.get(abbrev=medal_abbrev)

    def medal_rewarder(person: Person, count: int) -> Received:
        """
        gives this person the max of this count and the current count
        for the medal passed in the arguments of the constructor.
        updates the rank if required
        """
        rank = medal.calculate_rank(count)
        received, created = Received.objects.get_or_create(
            medal=medal, person=person, defaults={"rank": rank, "count": count}
        )
        if not created:
            received.rank = max(received.rank, rank)
            received.count = max(received.count, count)
            received.save()
        return received

    return medal_rewarder


def get_count_setter(
    *,
    medal_abbrev: Optional[str] = None,
    medal: Optional[Medal] = None,
    allow_reduce: bool = False,
) -> Callable[[Person, int], Received]:
    """constructor of function which changes the count for this medal for the given person"""
    if medal_abbrev is not None and medal is not None:
        raise ValueError(
            "get_medal_setter requires at most one argument to be specified, but both have been specified"
        )
    if medal is None:
        if medal_abbrev is None:
            raise ValueError(
                "get_medal_setter requires at least one argument to be specified, but none have been specified"
            )
        medal = Medal.objects.get(abbrev=medal_abbrev)

    def count_setter(person: Person, count: int) -> Received:
        """
        Sets the count of this person to this count, and sets the rank to the appropriate rank too.
        does this for the medal passed in the kwargs of the constructor
        """
        rank = medal.calculate_rank(count)
        received, created = Received.objects.get_or_create(
            medal=medal, person=person, defaults={"rank": rank, "count": count}
        )
        if not created:
            if allow_reduce:
                received.rank = rank
            else:
                received.rank = max(rank, received.rank)
            received.count = count
            received.save()
        return received

    return count_setter
