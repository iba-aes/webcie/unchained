import logging

from members.models.person import Person

from ..models import Achieved, Achievement, Medal, Received
from .achievement_utilities import (
    add_qualifier,
    create_group_qualifier,
    get_achievement_rewarder,
    get_qualifier,
    reset_qualifier_dict,
)
from .medal_utilities import (
    add_counter,
    get_counter,
    get_medal_rewarder,
    reset_counter_dict,
)

logger = logging.getLogger(__name__)


def setup_qualifiers():
    """
    function which adds all qualifiers to the qualifier dictionary.
    put the automation for your achievements in here by defining a function
    which takes a "person" and returns a boolean, and decorating that function with the following:
    @add_qualifier([achievement.abbrev])
    where [achievement.abbrev] is the (string) abbreviation of the achievement.
    """
    reset_qualifier_dict(__name__)

    @add_qualifier("even")
    def even_qualifier(person):
        return person.id % 2 == 0

    @add_qualifier("oneven")
    def odd_qualifier(person):
        return person.id % 2 == 1

    create_group_qualifier("webcie")


def setup_counters():
    """
    function which adds all counters to the counter dictionary.
    put the automation for your medals in here by defining a function
    which takes a "person" and returns an integer, and decorating that function with the following:
    @add_counter([medal.abbrev])
    where [medal.abbrev] is the (string) abbreviation of the medal.
    """
    reset_counter_dict(__name__)


def setup_automation_dicts():
    """
    function which sets up all the automation for (most of) the rewards
    (the ones with continous_checks==True).
    it gets called ONCE on initialisation of the achievements app
    """
    print("setting up reward qualifier and counter dicts")
    setup_qualifiers()
    setup_counters()
    print("reward dictionary setup complete")


def single_achievement_check(person: Person, achievement: Achievement):
    if get_qualifier(achievement)(person):
        return True
    else:
        return False


def single_medal_check(person: Person, medal: Medal):
    """returns the current count of this person for this medal.
    NOTE: this might be different from the count saved in the received instance.
    butler() has logic to combine them.
    """
    counter = get_counter(medal)
    count = counter(person)
    # get the count of the thing counted through magickall powers
    return count


def butler(person: Person, achievements=None, medals=None):
    # here, achievements and medals are iterables of achievement and medal instances respectively.

    if achievements is None:
        achievements = Achievement.objects.all()

    if medals is None:
        medals = Medal.objects.all()

    for achievement in achievements:
        # if the achievement is automated:
        if achievement.continuous_checks:
            # reward the achievement if the person qualifies
            if single_achievement_check(person, achievement):
                achievement_rewarder = get_achievement_rewarder(achievement=achievement)
                achievement_rewarder(person)
    for medal in medals:
        # if the medal is automated:
        if medal.continuous_checks:
            # update the count and rank.
            # the medal rewarder takes care of any difference between
            # the saved count and rank, and the updated count and rank
            count = single_medal_check(person, medal)
            medal_rewarder = get_medal_rewarder(medal=medal)
            medal_rewarder(person, count)
