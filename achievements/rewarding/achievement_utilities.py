import logging
from typing import Callable, Dict, Optional

from members.models.person import Person

from ..models import Achieved, Achievement

logger = logging.getLogger(__name__)

Qualifier = Callable[[Person], bool]

Rewarder = Callable[[Person], Achieved]

achievement_qualifiers: Dict[str, Qualifier] = {}


def deactivated_qualifier(person: Person) -> bool:
    return False


# creates decorator to add qualifier functions to dictionary. achievement_abbrev is
# the (official) (not-to-be-edited) abbreviation of the accompanying achievement.
def add_qualifier(achievement_abbrev: str) -> Callable[[Qualifier], Qualifier]:
    def decorate(qualifier: Qualifier) -> Qualifier:
        achievement_qualifiers[achievement_abbrev] = qualifier
        return qualifier

    return decorate


def create_group_qualifier(
    group_name: str, achievement_abbrev: Optional[str] = None
) -> Qualifier:
    """creates and adds a qualifier which returns true when person in a
    group with the passed name"""
    if achievement_abbrev is None:
        achievement_abbrev = group_name

    @add_qualifier(achievement_abbrev)
    def group_qualifier(person: Person) -> bool:
        return person.contact.group_set.filter(name__iexact=group_name).exists()

    return group_qualifier


def get_qualifier(achievement: Achievement) -> Qualifier:
    """returns the qualifier associated with the passed achievement, if any.
    if there isn't such a qualifier, returns the constant false qualifier"""

    if achievement.abbrev in achievement_qualifiers:
        return achievement_qualifiers[achievement.abbrev]
    else:
        logger.warning(
            f'there has been an attempt to get a qualifier for the "{achievement}"'
            f"-achievement. as it is not in the list, the deactivated qualifier has been returned."
            "fix by either adding a qualifier for this achievement or by turning off continuous_checks"
            " for this achievement."
        )
        return achievement_qualifiers[""]


def get_all_qualifiers() -> Dict[str, Qualifier]:
    """returns dict of all qualifiers keyed to the abbreviation of their respective achievements"""

    return achievement_qualifiers


def reset_qualifier_dict(caller_name: str) -> None:
    """
    resets the dictionary containing the qualifiers.
    DONT USE OUTSIDE OF TESTS UNLESS YOU KNOW WHAT YOU'RE DOING.
    ESPECIALLY IN PRODUCTION CODE
    """
    # NOTE: this function gets called on startup, which is why it *can* be ok to use.

    achievement_qualifiers.clear()
    if caller_name != "achievements.rewarding.reward_butler":
        logger.warning(
            "THE QUALIFIER DICTIONARY HAS BEEN RESET.\n"
            "IF THIS IS IN PRODUCTION, pls do some `grep` + `git blame` magic to find out who's to blame."
            f"this message is brought to you by {caller_name}"
        )
    else:
        logger.info("the qualifier dictionary has been reset.")
    # adds the constant false qualifier, cus it is required
    add_qualifier("")(deactivated_qualifier)


def get_achievement_rewarder(
    *,
    achievement_abbrev: Optional[str] = None,
    achievement: Optional[Achievement] = None,
) -> Rewarder:
    """
    returns a function which gives the passed person the achievement which was
    specified in the argument to this function
    takes either the abbreviated name of an achievement or the achievement itself. (exclusive)
    """
    if achievement_abbrev is None and achievement is None:
        raise ValueError(
            "get_rewarder requires at least one argument to be specified, but none were specified"
        )
    if achievement_abbrev is not None and achievement is not None:
        raise ValueError(
            "get rewarder requires at most one argument to be specified, but 2 were specified"
        )
    if achievement is None:
        achievement = Achievement.objects.get(abbrev=achievement_abbrev)

    def achievement_rewarder(person: Person) -> Achieved:
        """rewards person the achievement specified in the argument of the surrounding function"""
        achieved, created = Achieved.objects.update_or_create(
            achievement=achievement,
            person=person,
            defaults={
                "has_achieved": True,
            },
        )
        return achieved

    return achievement_rewarder


def person_has_achievement(person: Person, achievement: Achievement) -> bool:
    """returns true iff person has achieved this achievement"""
    achieved, _ = Achieved.objects.get_or_create(achievement=achievement, person=person)
    return achieved.has_achieved
