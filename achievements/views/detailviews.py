from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic

from achievements.rewarding.achievement_utilities import person_has_achievement
from members.models.person import Person

from ..models import Achieved, Achievement, Medal, Received


class AchievementDetailView(LoginRequiredMixin, generic.DetailView):
    """
    this page shows in detail information about a specific achievement

    **authorised user only**

    **CONTEXT:**

        ``user_sees_achievement``
            bool which determines whether or not the current user has the rights to see this achievement

        ``achiever-list``
            a list of all people who have achieved this achievement

        ``num_visits``
            the number of times the current session has visited this specific achievement page in a row
            used for bad achievement lookup surprise

    **SESSION_DATA:**

        ``ACHIEVEMENTS_last_visited_achievement``
            abbrev field of the last visited achievement page,
            used to check if the current page is the same as the last page visited (used for surprise)

        ``ACHIEVEMENTS_num_visits``
            int telling how many times in a row this page has been visited by the current session

    **TEMPLATES:**

        :template:`../achievements/templates/achievements/achievement_detail.html`
    """

    model = Achievement

    slug_field = "abbrev"
    slug_url_kwarg = "name"

    def get_context_data(self, **kwargs):
        if (
            self.request.session.get("ACHIEVEMENTS_last_visited_achievement", "")
            == self.object.abbrev
        ):
            num_visits = self.request.session.get("ACHIEVEMENTS_num_visits", 0) + 1
        else:
            num_visits = 1
        self.request.session[
            "ACHIEVEMENTS_last_visited_achievement"
        ] = self.object.abbrev
        self.request.session["ACHIEVEMENTS_num_visits"] = num_visits
        user = self.request.user
        user_sees_achievement = (
            self.object.hidden
            or user.has_perm("achievements.can_see_all")
            or person_has_achievement(user.person, self.object)
        )

        context = super(AchievementDetailView, self).get_context_data(**kwargs)
        context["user_sees_achievement"] = user_sees_achievement
        context["achiever_list"] = Achieved.objects.filter(
            achievement=self.object
        ).filter(has_achieved=True)
        context["num_visits"] = num_visits
        # NOTE:
        #       type(user_sees_achievement)==Boolean
        #       type(achiever_list)==Queryset
        #       type(num_visits)==Integer
        return context


class MedalDetailView(LoginRequiredMixin, generic.DetailView):
    """
    This page shows detailed information about this :model:`achievements.medal`

    **authorised user only**

    **CONTEXT:**

        ``bronze_receiver_list``
            A list\* of all people who have the bronze rank in this medal

        ``silver_receiver_list``
            A list\* of all people who have the silver rank in this medal

        ``gold_receiver_list``
            A list\* of all people who have the gold rank in this medal

        ``platinum_receiver_list``
            A list\* of all people who have the platinum rank in this medal

        \* actually a queryset, but who cares amirite?
    **TEMPLATES:**
        :template:`../achievements/templates/achievements/medal_detail.html
    """

    model = Medal

    slug_field = "abbrev"
    slug_url_kwarg = "name"

    def get_context_data(self, *args, **kwargs):
        context = super(MedalDetailView, self).get_context_data(*args, **kwargs)
        context["bronze_receiver_list"] = Received.objects.filter(
            medal=self.object
        ).filter(rank__gte=1)
        context["silver_receiver_list"] = Received.objects.filter(
            medal=self.object
        ).filter(rank__gte=2)
        context["gold_receiver_list"] = Received.objects.filter(
            medal=self.object
        ).filter(rank__gte=3)
        context["platinum_receiver_list"] = Received.objects.filter(
            medal=self.object
        ).filter(rank__gte=4)
        return context
