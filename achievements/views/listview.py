from django.contrib.auth.decorators import login_required
from django.db.models import Count, Exists, OuterRef, Q
from django.shortcuts import render

from ..models import Achievement, Medal


@login_required
def AchievementListView(request):
    """
    This page shows a list of all (for the current user visible) :model:`achievements.achievement`
    and :model:`achievements.medal`.

    **authorised user only**

    **CONTEXT:**

        ``achievement_list``
            a list of the achievements visible to the current user,
            along with how many people have achieved them.

        ``medal_list``
            a list of all medals, along with how many people have achieved each rank

    **TEMPLATES:**
        :template:`../achievements/templates/achievements/achievement_list.html`
    """
    current_user = request.user

    current_user_has = Q(
        achieved__person=current_user.person, achieved__has_achieved=True
    )
    not_invisible = Q(hidden=False)
    has_been_achieved = Q(achieved__has_achieved=True)
    current_user_achievements = Achievement.objects.filter(
        current_user_has | not_invisible
    ).filter(pk=OuterRef("pk"))

    achievement_list = Achievement.objects.annotate(
        num_achiever=Count("achieved", filter=has_been_achieved),
        normally_visible=Exists(current_user_achievements),
    )
    if not current_user.has_perm("achievements.can_see_all"):
        achievement_list = achievement_list.filter(normally_visible=True)

    medal_list = Medal.objects.annotate(
        num_bronze=Count("received", filter=Q(received__rank__gte=1)),
        num_silver=Count("received", filter=Q(received__rank__gte=2)),
        num_gold=Count("received", filter=Q(received__rank__gte=3)),
        num_platinum=Count("received", filter=Q(received__rank__gte=4)),
    )

    context = {
        "medal_list": medal_list,
        "achievement_list": achievement_list,
    }
    return render(request, "achievements/achievement_list.html", context=context)
