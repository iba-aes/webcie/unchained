from .detailviews import AchievementDetailView, MedalDetailView
from .listview import AchievementListView
from .userview import UserAchievementDetailView
