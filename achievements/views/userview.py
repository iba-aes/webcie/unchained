from django.contrib.auth.decorators import login_required
from django.db.models import Exists, F, OuterRef, Q, Sum
from django.shortcuts import render
from guardian.shortcuts import get_objects_for_user

from members.models import Person

from ..forms import DynamicRewardAchievementForm
from ..models import Achieved, Achievement, Medal, Received
from ..rewarding import butler, get_achievement_rewarder

# from guardian.utils import get_anonymous_user


@login_required
def UserAchievementDetailView(request, **kwargs):
    """
    This page shows the achievements and medals of a specific person.

    **authorised user only**

    Touches on:
    - :model:`memebers.person`
    - :model:`achievements.achievement`
    - :model:`achievements.achieved`
    - :model:`achievements.medal`
    - :model:`achievements.received`

    **CONTEXT:**
        ``medal_list``
            A list of all medals, with the information about what level the viewed person has received
        ``achievement_list``
            A list of all achievements which the current user can see and the viewed person has achieved

    **TEMPLATES:**
        :template:`../achievements/templates/achievements/user_achievement_list.html`
    """
    current_user = request.user
    viewed_person = Person.objects.get(pk=kwargs["pk"])

    butler(viewed_person)

    current_user_has = Q(
        achieved__person__pk=current_user.person.pk, achieved__has_achieved=True
    )
    not_invisible = Q(hidden=False)

    current_user_achievements = Achievement.objects.filter(
        current_user_has | not_invisible
    ).filter(pk=OuterRef("achievement"))

    achieved_list = (
        Achieved.objects.filter(person=viewed_person)
        .annotate(normally_visible=Exists(current_user_achievements))
        .select_related("achievement")
    )
    if not current_user.has_perm("achievements.can_see_all_achievements"):
        achieved_list = achieved_list.filter(normally_visible=True)

    medal_list = Received.objects.filter(person=viewed_person).select_related("medal")

    rewardable_achievements = get_objects_for_user(
        current_user, "reward_achievement", Achievement
    )
    unachieved_rewardable_achievements = rewardable_achievements.exclude(
        pk__in=rewardable_achievements.filter(
            achieved__person=viewed_person, achieved__has_achieved=True
        )
    )

    current_user_can_reward = unachieved_rewardable_achievements.exists()
    if current_user_can_reward and request.method == "POST":
        form = DynamicRewardAchievementForm(
            request.POST, queryset=unachieved_rewardable_achievements
        )
        if form.is_valid():
            queryset = form.cleaned_data["achievements"]
            for achievement in queryset:
                reward_achievement = get_achievement_rewarder(
                    achievement_abbrev=achievement.abbrev
                )
                reward_achievement(viewed_person)
        else:
            pass
    elif current_user_can_reward:
        form = DynamicRewardAchievementForm(queryset=unachieved_rewardable_achievements)
    else:
        form = None

    context = {
        "medal_list": medal_list,
        "achievement_list": achieved_list,
        "viewed_person": viewed_person,
        "reward_achievement_form": form,
        "current_user_can_reward": current_user_can_reward,
    }

    return render(request, "achievements/user_achievement_list.html", context=context)
