from django.urls import path

from . import views

app_name = "career"
urlpatterns = [
    path("", views.VacancyListView.as_view(), name="vacancy_list"),
    path("<int:pk>/", views.VacancyDetailView.as_view(), name="vacancy_detail"),
]
