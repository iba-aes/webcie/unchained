import datetime

from django.db import models
from django.db.models.fields.files import FieldFile
from tinymce.models import HTMLField

from members.models import StudyProgram
from unchained.types.queryset import QueryType


class Company(models.Model):
    """Some information to identify a company, including a logo
    and some HTML text.
    """

    # The name of the company:
    name: str = models.CharField(max_length=1024)
    # A description of the company, which can include simple HTML
    description: str = HTMLField()
    # The url of the company
    url: str = models.URLField()
    # The logo of the company
    logo: FieldFile = models.ImageField(upload_to="company-logos/")

    def __str__(self) -> str:
        return self.name


class Vacancy(models.Model):
    """Some information to identify a company, including a logo
    and some HTML text.
    """

    # The title of the vacancy:
    title: str = models.CharField(max_length=1024)
    # The company:
    company: Company = models.ForeignKey(Company, on_delete=models.CASCADE)
    # A description of the vacancy, which can include simple HTML
    description: str = HTMLField()

    # The starting date of the vacancy:
    since: datetime = models.DateField()
    # The expiry date of the vacancy:
    until: datetime = models.DateField()

    # The study programs associated with the vacancy.
    study_programs: QueryType[StudyProgram] = models.ManyToManyField(StudyProgram)

    def __str__(self) -> str:
        return self.title


class Advertisement(models.Model):
    """Some advertisement, for a vacancy or for a company."""

    # The company:
    company: Company = models.ForeignKey(Company, on_delete=models.CASCADE)

    # The URL that the advertisement should lead to:
    url: str = models.URLField()

    # The starting date of the advertisement:
    since: datetime = models.DateField()
    # The expiry date of the advertisement:
    until: datetime = models.DateField()

    def __str__(self) -> str:
        return f"{self.company} advertised {self.url}"
