from django.views import generic

from career.models import Vacancy

# Create your views here.


class VacancyListView(generic.ListView):
    paginate_by = 2
    model = Vacancy

    template_name = "vacancy/vacancy_list.html"
    context_object_name = "vacancy_list"


class VacancyDetailView(generic.DetailView):
    model = Vacancy

    template_name = "vacancy/vacancy_detail.html"
    context_object_name = "vacancy"
