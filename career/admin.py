from django.contrib import admin

from .models import Advertisement, Company, Vacancy


@admin.register(Company)
class ContactAdmin(admin.ModelAdmin):
    pass


@admin.register(Vacancy)
class PersonAdmin(admin.ModelAdmin):
    pass


@admin.register(Advertisement)
class MembershipAdmin(admin.ModelAdmin):
    pass
