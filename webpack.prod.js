const { merge } = require('webpack-merge');
const common = require('./webpack.config');
const webpack = require("webpack");
const SentryWebpackPlugin = require("@sentry/webpack-plugin");

// https://stackoverflow.com/a/38401256/11930866
const commitHash = require('child_process')
    .execSync('git rev-parse --short HEAD')
    .toString();

module.exports = env => {
    return merge(common, {
        mode: 'production',
        plugins: [
            new webpack.DefinePlugin({'process.env.NODE_ENV': '"staging"'}, {'process.env.GIT_SHA': '"' + commitHash + '"'}),
            // This should always be the last running plugin
            new SentryWebpackPlugin({
                authToken: env.split("=")[1],
                // Change this once Jason leaves the WebCie. We use a personal account here as Sentry offers free features
                // to students. That's why we don't have a 'webcie' account
                org: 'jasonoro',
                project: 'unchained',

                include: 'assets/webpack_bundles',
                // Django /static maps to the /assets folder, we need to pass this to Sentry otherwise mapping won't work
                urlPrefix: '~/static/webpack_bundles',
                ignore: ["node_modules", "webpack.config.js", "webpack.prod.js", "webpack.staging.js", ".eslintrc.js"],
            })
        ]
    });
}
